var resultCallback = null;

(function($) {
    $.fn.extend({
        processForm: function(callBack, errorCallBack) {
            var $this = $(this)[0];
            var $form = $('#' + $this.id)[0];

            var l = Ladda.create(document.querySelector('#' + $form.id + ' button[type="submit"]'));
            l.start();

            /*$('span.help-block').html('');
             $('.has-error').removeClass('has-error');*/

            /*data: $('#' + $form.id).serialize(),*/

            $.ajax({
                data: new FormData($('#' + $form.id)[0]),
                url: $form.action,
                type: 'post',
                dataType: 'json',
                enctype: 'multipart/form-data',
                async: false,
                contentType: false,
                processData: false,
                cache: false,
                success: function(result) {

                    resultCallback = result;

                    l.stop();

                    if (result.error == true) {
                        /*for(var indice in result.messages){
                         $('#' + indice).addClass('has-error');
                         $('#' + indice + ' .help-block').html(result.messages[indice]);
                         }*/

                        swal("ERROR!", result.message, "warning");

                        if (errorCallBack != undefined)
                            return errorCallBack.call();

                        return false;
                    }

                    if (typeof callBack == 'function')
                        return callBack.call();
                }
            });
        }
    });
})(jQuery);