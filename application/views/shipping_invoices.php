<section class="container contenedorCatalogoProductos">
    <div class="tituloPage">
        <h1 >FINALIZAR PEDIDO</h1>
    </div>
    <img src="<?= base_url('assets/Resources/micarrito/02envioyfact.png') ?>" class="img-responsive imagenCarritoCompras" />

    <div class="containerEnvioYFacturacion row">
        <h3>ENVÍO Y FACTURACIÓN</h3>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 noPaddingLeft ">
            <h3>Envío</h4>
                <div class="row">
                    <div id="validation-alert" class="alert alert-danger" style="display: none;">
                        <a href="javascript:;" class="close" aria-label="close">&times;</a>
                        <strong>Falta completar los siguientes campos:</strong><br />
                        <span id="validation-text"></span>
                    </div>
                    <?php if ($customer->add_id && !$order->add_id): ?>
                        <input type="radio" name="group1" checked="" data-div_info="div_info1" value="" class="col-xs-1">
                        <div class="col-xs-11" id="div_info1">
                            <span class="boldText"><?= $customer->prf_name . ' ' . $customer->prf_surname ?></span>
                            <br/>
                            <?= $customer->add_address ?>,
                            <br/>
                            <?= $customer->add_locality ?>,
                            C.P.:<?= $customer->add_zip_code ?>,
                            <br/>
                            <?= $customer->add_city ?>,
                            <br/>
                            <?= $customer->add_country ?>
                            <?= $customer->prf_phone ? "<br/>" . $customer->prf_phone : '' ?>
                            <br/>
                            <input type="hidden" name="add_address" value="<?= $customer->add_address ?>" >
                            <input type="hidden" name="add_locality" value="<?= $customer->add_locality ?>" >
                            <input type="hidden" name="add_zip_code" value="<?= $customer->add_zip_code ?>" >
                            <input type="hidden" name="add_city" value="<?= $customer->add_city ?>" >
                            <input type="hidden" name="add_country" value="<?= $customer->add_country ?>" >
                        </div>
                        <input type="radio" name="group1" data-div_info="div_info2" value="" class="col-xs-1">
                        <div class="col-xs-10 facturaAExpandido" id="div_info2" style="padding-top: 15px;">
                            <input name="add_address" type="text" disabled="" placeholder="Nueva dirección">
                            <input name="add_locality" type="text" disabled="" placeholder="Localidad">
                            <input name="add_city" type="text" disabled="" placeholder="Provincia">
                            <input name="add_country" type="text" disabled="" placeholder="País">
                            <input name="add_zip_code" type="text" disabled="" placeholder="Código Postal">
                        </div>
                    <?php else: ?>
                        <input type="radio" name="group1" value="" data-div_info="div_info3" checked="" class="col-xs-1">
                        <div class="col-xs-10 facturaAExpandido" id="div_info3" style="padding-top: 15px;">
                            <input name="add_address" type="text" placeholder="Nueva dirección" value="<?= $order->add_address ?>">
                            <input name="add_locality" type="text" placeholder="Localidad" value="<?= $order->add_locality ?>">
                            <input name="add_city" type="text" placeholder="Provincia" value="<?= $order->add_city ?>">
                            <input name="add_country" type="text" placeholder="País" value="<?= $order->add_country ?>">
                            <input name="add_zip_code" type="text" placeholder="Código Postal" value="<?= $order->add_zip_code ?>">
                        </div>
                    <?php endif; ?>
                </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 noPaddingRight">
            <h3>Facturación</h3>
                <div class="row">
                    <input type="radio" name="group2" data-div_info="div_info4" data-toggle="collapse" data-target="#collapseFacturaA" value="A" class="col-xs-1" <?= ($order->inv_invoice == 'a') ? 'checked=""' : '' ?>>
                    <div class=" col-xs-11">
                        <div class="tipoFacturacionRadioButton" > <span>Factura A</span>
                            <a href="javascript:;" data-toggle="collapse" data-target="#collapseFacturaA" class="collapsed">
                                <img src="<?= base_url('assets/Resources/micarrito/abajo.png') ?>">
                            </a>
                        </div>
                    </div>
                    <div id="collapseFacturaA" class="col-xs-12 facturaAExpandido collapse" style="height: 0px;">
                        <div id="div_info4">
                            <input type="hidden" name="inv_invoice" value="a" >
                            <input name="inv_social_reazon_mr" type="text" placeholder="Razón social:" value="<?= ($order->inv_invoice == 'a') ? $order->inv_social_reazon_mr : '' ?>">
                            <input name="inv_address" type="text" placeholder="Domicilio:" value="<?= ($order->inv_invoice == 'a') ? $order->inv_address : '' ?>">
                            <input name="inv_locality" type="text" placeholder="Localidad:" value="<?= ($order->inv_invoice == 'a') ? $order->inv_locality : '' ?>">
                            <input name="inv_province" type="text" placeholder="Provincia:" value="<?= ($order->inv_invoice == 'a') ? $order->inv_province : '' ?>">
                            <span>Responsable inscripto:</span>
                            <div class="inscriptoExpandido">
                                <span>Si</span>
                                <input type="radio" name="inv_registered_manager" <?= ($order->inv_registered_manager == 1) ? 'checked=""' : '' ?> value="1" >
                                <span>No</span>
                                <input <?= ($order->inv_registered_manager == 0) ? 'checked=""' : '' ?> type="radio" name="inv_registered_manager" value="0" >
                            </div>
                            <input name="inv_cuit_cuil" type="text" placeholder="CUIT:" value="<?= ($order->inv_invoice == 'a') ? $order->inv_cuit_cuil : '' ?>">
                        </div>
                    </div>

                    <input type="radio" name="group2" data-div_info="div_info5" data-toggle="collapse" data-target="#collapseFacturaB" value="B" class="col-xs-1" <?= ($order->inv_invoice == 'b') ? 'checked=""' : '' ?>>
                    <div class=" col-xs-11">
                        <div class="tipoFacturacionRadioButton" > <span>Factura B</span>
                            <a href="javascript:;" data-toggle="collapse" data-target="#collapseFacturaB" class="collapsed">
                                <img src="<?= base_url('assets/Resources/micarrito/abajo.png') ?>">
                            </a>
                        </div>
                    </div>

                    <div id="collapseFacturaB" class="col-xs-12 facturaAExpandido collapse" style="height: 0px;" >
                        <div id="div_info5">
                            <input type="hidden" name="inv_invoice" value="b" >
                            <input name="inv_social_reazon_mr" type="text" placeholder="Sr(es)" value="<?= ($order->inv_invoice == 'b') ? $order->inv_social_reazon_mr : '' ?>">
                            <input name="inv_address" type="text" placeholder="Domicilio:" value="<?= ($order->inv_invoice == 'b') ? $order->inv_address : '' ?>">
                            <input name="inv_locality" type="text" placeholder="Localidad:" value="<?= ($order->inv_invoice == 'b') ? $order->inv_locality : '' ?>">
                            <input name="inv_province" type="text" placeholder="Provincia:" value="<?= ($order->inv_invoice == 'b') ? $order->inv_province : '' ?>">
                            <input name="inv_cuit_cuil" type="text" placeholder="CUIT/CUIL:" value="<?= ($order->inv_invoice == 'b') ? $order->inv_cuit_cuil : '' ?>">
                        </div>
                    </div>

                    <input type="radio" name="group2" data-div_info="div_info6" data-toggle="collapse" data-target="#collapseFacturaC" value="C" class="col-xs-1" <?= (!$order->inv_id || $order->inv_invoice == 'c') ? 'checked=""' : '' ?> >
                    <div class=" col-xs-11">
                        <div class="tipoFacturacionRadioButton" > <span>Consumidor final</span>
                            <a href="javascript:;" data-toggle="collapse" data-target="#collapseFacturaC" class="collapsed">
                                <img src="<?= base_url('assets/Resources/micarrito/abajo.png') ?>">
                            </a>
                        </div>
                    </div>
                    <div id="collapseFacturaC" class="col-xs-12 facturaAExpandido collapse in" style="height: 0px;">
                        <div id="div_info6">
                            <input type="hidden" name="inv_invoice" value="c" >
                            <input name="inv_cuit_cuil" type="text" placeholder="DNI:" value="<?= ($order->inv_invoice == 'c') ? $order->inv_cuit_cuil : '' ?>">
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <a href="javascript:void(0);" id="saveShippingInvoices" class="SeguirLink">SEGUIR <span >&#9658;</span></a><span style="width: 30px;" class="SeguirLink">&nbsp;&nbsp;</span><a href="<?= base_url('mi_carrito') ?>" class="SeguirLink"><span>&#9668;</span> VOLVER</a>
</section>

<script>
    $(document).ready(function () {
        $('input[name=group1]').click(function () {
            if ($(this).is(':checked')) {
                if ($(this).attr('data-div_info') == 'div_info2') {
                    $('#div_info2 input').each(function () {
                        $(this).removeAttr('disabled');
                    });
                } else {
                    $('#div_info2 input').each(function () {
                        $(this).val('');
                        $(this).attr('disabled', 'disabled');
                    });
                }
            }
        });
        $('#saveShippingInvoices').click(function () {
            var shippingRadio = $('input[name=group1]:checked').attr('data-div_info');
            var invoiceRadio = $('input[name=group2]:checked').attr('data-div_info');

            var shipping = $('#' + shippingRadio + ' :input').serialize();
            var invoice = $('#' + invoiceRadio + ' :input').serialize();

            var validation = true;
            var text = "";
            if (shippingRadio == 'div_info2') {
                $('#' + shippingRadio + ' :input').each(function () {
                    if (!$.trim($(this).val())) {
                        validation = false;
                        $(this).val('');
                        text += $(this).attr('placeholder') + "<br />";
                    }
                });
            }

            if (invoiceRadio == 'div_info6') {
                if (!$.trim($('#div_info6 input[name="inv_cuit_cuil"]').val())) {
                    validation = false;
                    $(this).val('');
                    text += $('#div_info6 input[name="inv_cuit_cuil"]').attr('placeholder') + "<br />";
                }
            }


            if (validation) {
                $.ajax({
                    type: 'POST',
                    url: 'guardar_envio_y_facturacion/<?= $order->ord_id ?>',
                    data: shipping + '&' + invoice,
                    success: function (response) {
                        if (!response['error']) {
                            window.location.href = "<?= base_url('metodo_de_envio_y_pago') ?>";
                        } else {
                            location.reload();
                        }
                    }
                });
            } else {
                $('#validation-text').html(text);
                $('#validation-alert').fadeIn();
            }
        });
    });
    $('.close').click(function () {
        $(this).parent().fadeOut();
    });
</script>