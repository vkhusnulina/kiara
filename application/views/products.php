<section class="container contenedorCatalogoProductos">
    <div class="tituloPage">
        <h1> PRODUCTOS </h1>
    </div>

    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm 3 col-xs-12 menuCatalogoProducto">

            <div class="TitulomenuCatalogoProducto"> <?= $category->cat_name ?> </div>
            <ul>
                <?php foreach ($products as $product) { ?>
                    <li>
                        <a href="<?= base_url($category->cat_slug . '/' . $product->prod_slug) ?>">
                            <span class="productoMenuCatalogoProducto"><?= $product->prod_name ?></span>
                            <span class="codigoMenuCatalogoProducto">#<?= $product->prod_code ?></span>
                        </a>
                    </li>
                <?php } ?>
            </ul>


            <?php
            foreach ($categories as $other_cat) {
                if ($other_cat->cat_id != $category->cat_id) {
                    ?>
                    <div class="otrasCategoriasCatalogoProductoMenu">
                        <h2><a href="<?= $other_cat->cat_slug ?>"><?= $other_cat->cat_name ?></a></h2>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <div class="col-lg-9 col-md-9 col-sm 9 col-xs-12 colImagenesProductosCatalogoProductos">
            <div>
                <ul class="breadcrumb">
                    <li><a href="productos">Productos</a></li>
                    <li class="active"><?= $category->cat_name ?></li>
                </ul>
            </div>

            <div class="text-center" style="margin-bottom:25px; background: transparent url(<?= base_url('assets/admin/images/categories/' . $category->cat_image) ?>) center center no-repeat; max-height: 200px;">
                <div class="categoriaHomeTexto CatalogoProductoTextoImagen"><?= $category->cat_name ?></div>
            </div>

            <div class="row productoCatalogo">
                <?php $flagx2 = 1; ?>
                <?php foreach ($products as $product) { ?>
                    <?php
                    if ($flagx2 == 1)
                        echo '<div class="row">';
                    ?>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="hoverImagenDelProducto" data-slug="<?= $product->prod_slug ?>">
                            <div class="divImagenProductoCatalogoProducto">
                                <?php if (!empty($product->media)): ?>
                                    <img src="<?= base_url('assets/admin/images/products/' . $product->media[0]->media_name) ?>" class=" imagenProductoCatalogoProducto img-responsive" alt="<?= $product->prod_name ?>"/>
                                <?php else: ?>
                                    <img src="<?= base_url('assets/admin/images/products/default.gif') ?>" class=" imagenProductoCatalogoProducto img-responsive" alt="<?= $product->prod_name ?>"/>
                                <?php endif; ?>
                            </div>
                            <div class="hoverNombreDelProducto">
                                <span class="idProducto">
                                    &nbsp;#<?= $product->prod_code ?>
                                </span>
                                <span class="nombreProducto">
                                    <?= strtoupper($product->prod_name) ?>&nbsp;
                                </span>
                            </div>
                        </div>
                        <?php if ($this->tank_auth->is_logged_in()): ?>
                            </br>
                            <span class="precioProducto">
                                $<?= $product->prod_price ?>
                            </span>
                        <?php endif; ?>
                        <div class="imagenesColoresCatalogoProductos">
                            <?php foreach ($product->colors as $color): ?>
                                <img src="<?= base_url('assets/admin/images/colors/' . $color->clr_image) ?>" class="img-responsive" alt="<?= $color->clr_value ?>" />
                            <?php endforeach; ?>
                        </div>
                        <a href="<?= $category->cat_slug . '/' . $product->prod_slug ?>" class="linkVerMas" > Ver más + </a>
                    </div>
                    <?php
                    if ($flagx2 == 3) {
                        echo '</div>';
                        $flagx2 = 1;
                    } else {
                        $flagx2++;
                    }
                    ?>
                <?php } ?>
            </div>
        </div>
    </div>
</section>

<script>

    $('.hoverImagenDelProducto').click(function() {
        window.location = "<?= base_url($category->cat_slug) ?>/" + $(this).attr('data-slug');
    });
</script>