<section class="container contenedorCatalogoProductos">
    <div class="tituloPage">
        <h1 >FINALIZAR PEDIDO</h1>
    </div>
    <img src="<?= base_url('assets/Resources/micarrito/03metyenvio.png') ?>" class="img-responsive imagenCarritoCompras" />
    <div class="containerEnvioYMetodo row">
        <h3>MÉTODO DE ENVÍO Y PAGO</h3>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 noPaddingLeft ">
            <h3>Envío</h4>
                <div class="envioDiv">
                    <p class="boldText">¡Podés elegir el que vos quieras! Completanos cual transporte es de tu preferencia o el nombre de tu Comisionista.</p>
                    <input type="checkbox" value="" id="ord_transport" <?= ($order->ord_transport == '1') ? 'checked=""' : '' ?>>
                    <input type="hidden" name="ord_transport" value="<?= $order->ord_transport ?>">
                    <span class="boldText">Transporte / Comisionista</span>
                    <br/>
                    <input style="max-width: 100%;" type="text" name="ord_shipping_company" class="inputTransporteEnvio"  <?= ($order->ord_transport == '0') ? 'disabled="" value=""' : 'value="' . $order->ord_shipping_company . '"' ?>>
                    <p class="italic">Recordá que el envío se abona en destino</p><br/>
                    <p class="boldText">Enviamos tu pedido a:</p>
                    <input type="radio" name="ord_ship_to" value="0" <?= ($order->ord_ship_to == '0') ? 'checked=""' : '' ?>>
                    <span> A domicilio</span>
                    <br/><br/>
                    <input type="radio" name="ord_ship_to" value="1" <?= ($order->ord_ship_to == '1') ? 'checked=""' : '' ?>>
                    <span> A terminal</span>
                    <br/><br/><p class="italic">Si no tienes un método de envío habitual, nosotros podemos recomendarte uno cuando nos contactemos para confirmar el pedido</p>
                </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 noPaddingRight">
            <h3>Pago</h4>
                <div >
                    <p class="boldText">CUENTA N°1: <span class="italic">SANTANDER RÍO</span> - Cuenta Corriente</p>

                    <p>- <span class="italic">Titular</span>: Teresa Mabel Damp</p>

                    <p>- <span class="italic">N° de cuenta</span>: 146-365351/5</p>

                    <p>- <span class="italic">CUIT</span>: 27-12080982/2</p>

                    <p>- <span class="italic">CBU</span>: 0720146888000036535154</p>
                </div>
                <div class="cuenta2Metodo">
                    <p class="boldText">CUENTA N°2: <span class="italic">BANCO FRANCÉS</span> - Cuenta Recaudadora</p>

                    <p class="italic">Cod. servicio: 0536 - Pesos (sólo depósito en ventanilla)</p>

                    <p class="italic">Kimberly Clark Arg. SA. -Pañalera UPA S.A.</p>

                    <p><span class="italic">DEPOSITANTE</span> N°: 40091306</p>

                    <p><span class="italic">CUIT</span>: 33710389069</p>

                    <p><span class="italic">CÓDIGO DE BARRAS</span>: 00536400913063</p>

                </div>
                <p><span class="boldText">EFECTIVO:</span><span class="infoRetiroMetodo"> Pueden retirar el pedido por el local y abonarlo directamente acá.</span</p>
        </div>
    </div>
    <div class="btnFinalizarDiv" >
        <a class="botonRojo" href="javascript:void(0);" id="saveShippingPayment">FINALIZAR PEDIDO</a>
    </div>
</section>
<script>
    $(document).ready(function () {

        $('#ord_transport').click(function () {
            if ($(this).is(':checked')) {
                $('input[name="ord_transport"]').val('1');
                $('input[name="ord_shipping_company"]').removeAttr('disabled');
            }else{
                $('input[name="ord_transport"]').val('0');
                $('input[name="ord_shipping_company"]').val('');
                $('input[name="ord_shipping_company"]').attr('disabled', 'disabled');
            }
        });

        $('#saveShippingPayment').click(function () {
            var order = $('.envioDiv :input').serialize();
            $.ajax({
                type: 'POST',
                url: 'guardar_metodo_de_envio_y_pago/<?= $order->ord_id ?>',
                data: order,
                success: function (response) {
                    if (!response['error']) {
                        window.location.href = "<?= base_url('finalizar') ?>/<?= $order->ord_id ?>";
                    } else {
                        location.reload();
                    }
                }
            });
        });
    });
</script>