<section id="contacto" class="container">
    <div class="tituloPage">
        <h1> CONTACTO </h1>
    </div>

    <div  class="container">
        <div class="row">
            <div class="col-lg-6 col-sm-6 col-xs-offset-1 col-sm-offset-0 noPaddingLeft">
                <form action="<?= base_url('contacto/enviar') ?>" method="post">
                    <?php if (isset($_GET['r']) && $_GET['r'] == 1) { ?>
                        <div class="alert alert-success" style="max-width: 528px;">
                            Comentario enviado con &eacute;xito.
                        </div>
                    <?php } ?>

                    <?php if (isset($_GET['r']) && $_GET['r'] == 0) { ?>
                        <div class="alert alert-warning" style="max-width: 528px;">
                            No se pudo enviar el comentario.
                        </div>
                    <?php } ?>

                    <?php if (isset($_GET['r']) && $_GET['r'] == 2) { ?>
                        <div class="alert alert-warning" style="max-width: 528px;">
                            Por favor complete todos los campos.
                        </div>
                    <?php } ?>

                    <div>
                        <input type="text" name="nombre" placeholder="Nombre: " />
                    </div>
                    <div>
                        <input type="text" name="email" placeholder="E-Mail: " />
                    </div>
                    <div>
                        <input type="text" name="telefono" placeholder="Teléfono: " />
                    </div>
                    <div>
                        <input type="text" name="provincia" placeholder="Provincia: " />
                    </div>
                    <div>
                        <textarea name="mensaje" placeholder="Mensaje: "></textarea>
                    </div>
                    <div>
                        <input name="submit" value="Enviar" type="submit" class="btnSuscripcion" />
                    </div>
                </form>
            </div>
            <div class="col-lg-3 col-sm-6 noPaddingRight">
                <div class="divContacto text-center">
                    <span class="glyphicon glyphicon-map-marker colorRed" aria-hidden="true"></span>
                    <span>
                        Aranguren 3071, Ciudad de Bs.As.
                    </span>
                    <br/>
                    <span class="glyphicon glyphicon-earphone colorRed" aria-hidden="true"></span>
                    <span>
                        (011)4613-2831 / 11-5177-3695
                    </span>
                    <br/>
                    <span class="glyphicon glyphicon-envelope colorRed" aria-hidden="true"></span>
                    <span>
                        info@kiaraweb.com.ar
                    </span>
                    <br/>
                    <br/>
                    <div><img src="<?= base_url('assets/Resources/seguinos-en-redes-sociales.png') ?>" alt="" /></div>
                    <div class="m-t-10">
                        <a href="#"><img src="<?= base_url('assets/Resources/contacto-facebook.png') ?>" alt="" /></a>
                        <a href="#"><img src="<?= base_url('assets/Resources/contacto-twitter.png') ?>" alt="" /></a>
                        <a href="#"><img src="<?= base_url('assets/Resources/contacto-instagram.png') ?>" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>