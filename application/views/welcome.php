<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <?php
        $i = 0;
        foreach ($slideshow as $image) {
            ?>
            <li data-target="#carousel-example-generic" data-slide-to="<?= $i; ?>" class="<?php
            if ($i == 0) {
                echo 'active';
            }
            ?>"></li>
                <?php
                $i++;
            }
            ?>
    </ol>

    <div class="carousel-inner" role="listbox">
        <?php
        $i = 0;
        foreach ($slideshow as $image) {
            ?>
            <div class="item <?php
            if ($i == 0) {
                echo 'active';
            }
            ?>">
                <img src="<?= base_url('assets/Resources/slideshow/' . $image->media_name) ?>" alt="...">
            </div>
            <?php
            $i++;
        }
        ?>
    </div>
</div>

<div class="container">
    <div class="row categoriasHome">
        <div style="background: url(<?= base_url('assets/Resources/imagen1.png') ?>) top center ;">
            <a href="anticipo-nueva-temporada">Nueva temporada</a>
        </div>
        <div style="background: url(<?= base_url('assets/Resources/imagen2.png') ?>) top center ;">
            <a href="sweaters-abrigos">Sweaters</a>
        </div>
        <div style="background: url(<?= base_url('assets/Resources/imagen3.png') ?>) top center ;">
            <a href="vestidos-polleras">Vestidos</a>
        </div>
        <div style="background: url(<?= base_url('assets/Resources/imagen4.png') ?>) top center ;">
            <a href="calzas-pantalones">Pantalones</a>
        </div>
        <div style="background: url(<?= base_url('assets/Resources/imagen5.png') ?>) top center ;">
            <a href="rebajas">Rebajas</a>
        </div>
    </div>

    <div class="divSuscripcion">
        <?php if (isset($_GET['news']) && $_GET['news'] == 1) { ?>
            <div class="alert alert-success" style="max-width: 90%; margin: 0 10% 20px;">
                Gracias por suscribirte a nuestro newsletter.
            </div>
        <?php } ?>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right font-cheddar font-24">
            Suscríbite a nuestro correo electrónico!
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <form action="<?= base_url('add_newsletter'); ?>" method="post">
                <input type="text" placeholder="E-Mail" class="emailInput" name="news_email"/>
                <input type="submit" value="SUSCRIBIRME" class="btnSuscripcion"/>
            </form>
        </div>
    </div>
</div>