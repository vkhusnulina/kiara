<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-0 divContacto">
                <span class="glyphicon glyphicon-map-marker colorRed" aria-hidden="true"></span>
                <span>
                    Aranguren 3071, Ciudad de Bs.As.
                </span>
                <br/>
                <span class="glyphicon glyphicon-earphone colorRed" aria-hidden="true"></span>
                <span>
                    (011)4613-2831 / 11-5177-3695
                </span>
                <br/>
                <span class="glyphicon glyphicon-envelope colorRed" aria-hidden="true"></span>
                <span>
                    info@kiaraweb.com.ar
                </span>
                <br/>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 divCopyright">
                <img src="<?= base_url('assets/Resources/logoMini.png') ?>" alt="Kiara" class="logoFooter">
                <br/>
                <span class="FooterVentasMay">
                    VENTAS MAYORISTAS
                </span>
                <br/>
                <span class="FooterCopy">
                    ©copyright 2015 - Todos los derechos reservados
                </span>
                <br/>
                <span class="FooterDesignBy">
                    Design by <a href="http://www.dolmastudio.com" target="_BLANK">Dolma Studio</a>
                </span>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 row divLinkFooter noPaddingRight">
                <ul class="col-xs-6 noPaddingRight">
                    <li><a href="<?= base_url() ?>">Home </a></li>
                    <li><a href="<?= base_url('productos') ?>">Catálogo de productos</a></li>
                    <li><a href="<?= base_url('como-comprar') ?>">Cómo comprar</a></li>
                </ul>
                <ul class="col-xs-6 noPaddingRight">
                    <li><a href="<?= base_url('guia-de-talles') ?>">Guía de talles</a> </li>
                    <li><a href="<?= base_url('donde-encontranos') ?>">Dónde encontrarnos</a></li>
                    <li><a href="<?= base_url('quienes-somos') ?>">Quiénes somos</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
</body>
</html>