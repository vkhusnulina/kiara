<nav class="navbar navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?= base_url() ?>">Kiara</a>
        </div>

        <div class="collapse navbar-collapse ">
            <ul class="nav navbar-nav" role="menu" aria-labelledby="dropdownMenu">
                <li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Configuración <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?= site_url('admin/configuration-emails') ?>">
                                <span>Configuración de e-mails</span>
                                <small>Configure los textos de los correos electrónicos</small>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Slideshow <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?= site_url('admin/slideshow') ?>">
                                <span>Galer&iacute;a de im&aacute;genes</span>
                                <small>Administre las im&aacute;genes del slideshow</small>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Newsletter <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?= site_url('admin/newsletter') ?>">
                                <span>Suscriptos</span>
                                <small>Listado de suscriptos al newsletter</small>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Catálogo <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?= site_url('admin/products') ?>">
                                <span>Productos</span>
                                <small>Administre los productos que se muestran a los visitantes de su tienda</small>
                            </a>
                        </li>
                        <li>
                            <a href="<?= site_url('admin/categories') ?>">
                                <span>Categorías</span>
                                <small>Administre las categorías que se utilizan para agrupar los productos de su tienda.</small>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <!-- Child Menu -->
                        <li class="dropdown-submenu">
                            <a tabindex="-1" href="javascript:void(0)">
                                <span>Atributos de productos</span>
                                <small>Administre los atributos para los productos de su tienda.</small>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<?= site_url('admin/product_attributes/colors') ?>">
                                        <span>Colores y texturas</span>
                                        <small>Administre los colores de los productos de su tienda</small>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= site_url('admin/product_attributes/sizes') ?>">
                                        <span>Talles</span>
                                        <small>Administre los talles de los productos de su tienda</small>
                                    </a>
                                </li>
                                <!--<li>
                                    <a href="<?= site_url('admin/product_attributes/shoes_sizes') ?>">
                                        <span>Talles de calzados</span>
                                        <small>Administre los talles de calzados de su tienda</small>
                                    </a>
                                </li>-->
                            </ul></li>

                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pedidos <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?= site_url('admin/orders') ?>">
                                <span>Listado de pedidos</span>
                                <small>Administre los pedidos realizados por los clientes de su tienda</small>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Clientes <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?= site_url('admin/customers') ?>">
                                <span>Listado de clientes</span>
                                <small>Administre los clientes de su tienda.</small>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $this->session->userdata('username'); ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <!--
                        <li><a href="#">Configuración de la cuenta</a></li>
                        <li class="divider"></li>
                        -->
                        <li><a href="<?php echo site_url('/auth/logout'); ?>">Cerrar sesión</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>