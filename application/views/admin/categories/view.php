<div class="panel panel-default">
    <div class="panel-heading">Categoría " <?= $category->cat_name; ?> ".</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12">
                <form id="submitNew" action="<?php echo site_url('admin/categories/update/' . $category->cat_id); ?>" enctype="multipart/form-data" method="post" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="cat_name" class="col-sm-3 control-label">Nombre de la categoría (*) :</label>
                        <div class="col-sm-9">
                            <input type="text" name="cat_name" class="form-control" value="<?= $category->cat_name; ?>" maxlength="45" required="" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cat_description" class="col-sm-3 control-label">Descripción:</label>
                        <div class="col-sm-9">
                            <textarea name="cat_description" class="form-control" maxlength="255"><?= $category->cat_description; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cat_order" class="col-sm-3 control-label">Orden de lista:</label>
                        <div class="col-sm-9">
                            <input type="number" name="cat_order" class="form-control" value="<?= $category->cat_order; ?>" maxlength="2" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cat_visible" class="col-sm-3 control-label">Estado:</label>
                        <div class="col-sm-9">
                            <select name="cat_visible" class="form-control">
                                <?php if (!$category->cat_visible) { ?>
                                    <option value="0" selected="">OCULTO</option>
                                    <option value="1">VISIBLE</option>
                                <?php } else { ?>
                                    <option value="0">OCULTO</option>
                                    <option value="1" selected="">VISIBLE</option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cat_img" class="col-sm-3 control-label">Imagen:</label>
                        <div class="col-sm-9">
                            <input type="file" name="cat_img" id="archivo" class="form-control" accept="image/*"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-10">
                            <div class="titulo">
                                <span>Vista Previa:</span> 
                                <span id="infoNombre"></span><br/>
                                <span id="infoTamaño"></span>
                            </div>
                            <div id="marcoVistaPrevia">
                                <?php if ($category->cat_image) { ?>
                                    <img id="vistaPrevia" class="previewCategory" src="<?= base_url('assets/admin/images/categories/' . $category->cat_image). '?' . date('his') ?>" alt="" />
                                <?php }else{ ?>
                                    <img id="vistaPrevia" class="previewCategory" src="" alt="" />
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default cancel">Cancelar</button>
                        <button type="submit" class="btn btn-primary ladda-button" data-style="zoom-out"><span class="ladda-label">Modificar</span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- PreviewImage -->
<script src="<?php echo base_url('assets/admin/js/jquery.previewImage.js'); ?>"></script>
<script>
    $(document).ready(function () {
        // Proceso el formulario de creacion
        $('#submitNew button[type="submit"]').bind('click', function (e) {
            e.preventDefault();
            $('#submitNew').processForm(function () {
                swal("Modificada!", "La categoría fue modificada con éxito.", "success");
                setTimeout(function () {
                    location.reload();
                }, 2000);
            });
            return false;
        });

        // Cancelo la operación
        $('.cancel').on('click', function () {
            $('.view-iframe-close').click();
        });
    });
</script>