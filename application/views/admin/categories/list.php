<script>
    $(document).ready(function() {
        var table = $('.table').DataTable({
            order: [[1, 'desc']],
            scrollX: true,
            scrollY: false,
            scrollCollapse: false,
            paging: false,
            ordering: true,
            info: false,
            filter: true,
            autoWidth: false
        });

        $('input[id="search"]').keyup(function() {
            table.search($('input[id="search"]').val()).draw();
        });

        $('#new').on('click', function() {
            $().loadView('categories/new');
        });

        $('.view').on('click', function() {
            $().loadView('categories/view/' + $(this).data('id'));
        });
    });
</script>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6">
                <a id="new" href="javascript:;" class="btn btn-primary"><i class="fa fa-plus"></i> Crear categoría</a>
            </div>
            <div class="col-sm-6">
                <form class="form-inline text-right">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="search" class="sr-only control-label">Buscar:</label>
                            <input type="search" id="search" class="form-control input-sm" placeholder="Buscar ...">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="table-responsive no-filter">
        <table class="table table-condensed table-hover table-striped">
            <thead>
                <tr>
                    <th class="w-20 text-center">&nbsp;</th>
                    <th class="w-20 text-center">Orden</th>
                    <th class="w-350">Nombre</th>
                    <th class="w-350">Slug</th>
                    <th class="w-100 text-center">Estado</th>
                    <th class="w-350 text-center">Última modificación</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach (($categories ? $categories : array()) as $category) { ?>
                    <tr>
                        <td class="w-20 text-center"><a href="javascript:;" class="view" data-id="<?= $category->cat_id ?>"><i class="fa fa-eye"></i></a></td>
                        <td class="w-20 text-center"><?= $category->cat_order ?></td>
                        <td class="w-350"><?= $category->cat_name ?></td>
                        <td class="w-350"><?= $category->cat_slug ?></td>
                        <td class="w-100 text-center">
                            <?php if ($category->cat_visible) { ?>
                                <span class="label label-success">VISIBLE</span>
                            <?php } else { ?>
                                <span class="label label-danger">OCULTO</span>
                            <?php } ?>
                        </td>
                        <td class="w-350 text-center"><?= date("d-m-Y H:i:s", strtotime($category->cat_created_at)) ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>