<div class="panel panel-default">
    <div class="panel-heading">Textura " <?= $color->clr_value; ?> ".</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12">
                <form id="submitNew" action="<?php echo site_url('admin/product_attributes/colors/update/' . $color->clr_id); ?>" enctype="multipart/form-data" method="post" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="clr_value" class="col-sm-3 control-label">Nombre del color / textura (*) :</label>
                        <div class="col-sm-9">
                            <input type="text" name="clr_value" class="form-control" value="<?= $color->clr_value; ?>" maxlength="45" required="" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="clr_description" class="col-sm-3 control-label">Descripción:</label>
                        <div class="col-sm-9">
                            <textarea name="clr_description" class="form-control" maxlength="255"><?= $color->clr_description; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="clr_img" class="col-sm-3 control-label">Color / Textura:</label>
                        <div class="col-sm-9">
                            <input type="file" name="clr_img" id="archivo" class="form-control" accept="image/*"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-9">
                            <div class="titulo">
                                <span>Vista Previa:</span> 
                                <span id="infoNombre"></span><br/>
                                <span id="infoTamaño"></span>
                            </div>
                            <div id="marcoVistaPrevia">
                                <?php if ($color->clr_image) { ?>
                                    <img id="vistaPrevia" class="previewTexture" src="<?= base_url('assets/admin/images/colors/' . $color->clr_image). '?' . date('his') ?>" alt="" />
                                <?php }else{ ?>
                                    <img id="vistaPrevia" class="previewTexture" src="" alt="" />
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default cancel">Cancelar</button>
                        <button type="submit" class="btn btn-primary ladda-button" data-style="zoom-out"><span class="ladda-label">Modificar</span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- PreviewImage -->
<script src="<?php echo base_url('assets/admin/js/jquery.previewImage.js'); ?>"></script>
<script>
    $(document).ready(function () {
        // Proceso el formulario de creacion
        $('#submitNew button[type="submit"]').bind('click', function (e) {
            e.preventDefault();
            $('#submitNew').processForm(function () {
                swal("Modificada!", "La textura fue modificada con éxito.", "success");
                setTimeout(function () {
                    location.reload();
                }, 2000);
            });
            return false;
        });

        // Cancelo la operación
        $('.cancel').on('click', function () {
            $('.view-iframe-close').click();
        });
    });
</script>