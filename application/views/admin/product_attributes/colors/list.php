<script>
    $(document).ready(function() {
        var table = $('.table').DataTable({
            order: [[1, 'desc']],
            scrollX: true,
            scrollY: false,
            scrollCollapse: false,
            paging: false,
            ordering: true,
            info: false,
            filter: true,
            autoWidth: false
        });

        $('input[id="search"]').keyup(function() {
            table.search($('input[id="search"]').val()).draw();
        });

        $('#new').on('click', function() {
            $().loadView('colors/new');
        });

        $('.view').on('click', function() {
            $().loadView('colors/view/' + $(this).data('id'));
        });
    });
</script>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6">
                <a id="new" href="javascript:;" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar color / textura</a>
            </div>
            <div class="col-sm-6">
                <form class="form-inline text-right">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="search" class="sr-only control-label">Buscar:</label>
                            <input type="search" id="search" class="form-control input-sm" placeholder="Buscar ...">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="table-responsive no-filter table-productos">
        <table class="table table-condensed table-hover table-striped">
            <thead>
                <tr>
                    <th class="w-20 text-center">&nbsp;</th>
                    <th class="w-350">Nombre</th>
                    <th class="w-100 text-center">Textura</th>
                    <th class="w-350 text-center">Fecha de creación</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach (($colors ? $colors : array()) as $color) { ?>
                    <tr>
                        <td class="w-20 text-center"><a href="javascript:;" class="view" data-id="<?= $color->clr_id ?>"><i class="fa fa-eye"></i></a></td>
                        <td class="w-350"><?= $color->clr_value ?></td>
                        <td class="w-100 text-center"><img style="width: 25px; height: 25px;" src="<?= base_url('assets/admin/images/colors/' . $color->clr_image). '?' . date('his') ?>" alt="" /></td>
                        <td class="w-350 text-center"><?= date("d-m-Y H:i:s", strtotime($color->clr_created_at)) ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>