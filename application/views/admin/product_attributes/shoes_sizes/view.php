<div class="panel panel-default">
    <div class="panel-heading">Talle de calzado " <?= $shoes_size->siz_value; ?> ".</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12">
                <form id="submitNew" action="<?php echo site_url('admin/product_attributes/shoes_sizes/update/' . $shoes_size->siz_id); ?>" method="post" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="siz_value" class="col-sm-3 control-label">Talle (*) :</label>
                        <div class="col-sm-9">
                            <input type="text" name="siz_value" class="form-control" value="<?= $shoes_size->siz_value; ?>" maxlength="45" required="" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="siz_description" class="col-sm-3 control-label">Descripci&oacute;n:</label>
                        <div class="col-sm-9">
                            <textarea name="siz_description" class="form-control" maxlength="255"><?= $shoes_size->siz_description; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="siz_order" class="col-sm-3 control-label">Orden de lista:</label>
                        <div class="col-sm-9">
                            <input type="number" name="siz_order" class="form-control" value="<?= $shoes_size->siz_order; ?>" maxlength="2" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default cancel">Cancelar</button>
                        <button type="submit" class="btn btn-primary ladda-button" data-style="zoom-out"><span class="ladda-label">Modificar</span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        // Proceso el formulario de creacion
        $('#submitNew button[type="submit"]').bind('click', function (e) {
            e.preventDefault();
            $('#submitNew').processForm(function () {
                swal("Modificado!", "El talle de calzado fue modificado con éxito.", "success");
                setTimeout(function () {
                    location.reload();
                }, 2000);
            });
            return false;
        });

        // Cancelo la operación
        $('.cancel').on('click', function () {
            $('.view-iframe-close').click();
        });
    });
</script>