<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12">
                <?php if (count($images) > 0): ?>
                    <h3>Im&aacute;genes existentes:</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <input id="previous_images" disabled="" type="file" multiple="true" data-show-upload="false" data-show-remove="false" data-show-caption="false">
                    <hr />
                <?php endif; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h3>Agregar im&aacute;genes:</h3>
            </div>
        </div>
        <form id="submitSlideshow" action="<?= base_url('admin/slideshow/upload') ?>" enctype="multipart/form-data" method="post" class="form-horizontal" role="form">
            <div class="row">
                <div class="col-sm-12">
                    <input id="new_images" name="img[]" type="file" multiple="true" data-show-upload="false" data-show-caption="true">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default cancel">Cancelar</button>
                    <button type="submit" class="btn btn-primary ladda-button" data-style="zoom-out"><span class="ladda-label">Guardar</span></button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function () {
    $("#new_images").fileinput({
    language: "es",
            allowedFileExtensions: ["jpg", "gif", "png"]
    });
            //Vista previa de las imagenes existentes
<?php if (count($images) > 0): ?>
        $("#previous_images").fileinput({
        language: "es",
                allowedFileExtensions: ["jpg", "gif", "png"],
                initialPreview: [
    <?php foreach ($images as $image): ?>
            "<img src='<?= base_url('assets/Resources/slideshow/' . $image->media_name) ?>' class='file-preview-image' alt='' title=''>",
    <?php endforeach; ?>
        ],
                // initial preview configuration
                initialPreviewConfig: [
    <?php foreach ($images as $image): ?>
            {
            url: '<?= site_url('admin/products/delete_image/') . '/' . $image->media_id ?>',
                    key: <?= $image->media_id ?>,
                    extra: {
            id: <?= $image->media_id ?>
            }
            },
    <?php endforeach; ?>
        ]});
<?php endif; ?>


    // Proceso el formulario de creacion
    $('#submitSlideshow button[type="submit"]').bind('click', function (e) {
    e.preventDefault();
            $('#submitSlideshow').processForm(function () {
    swal("Guardado!", "La galería slideshow fue actualizada con éxito.", "success");
            setTimeout(function () {
    location.reload();
    }, 2000);
    });
            return false;
    });
            // Cancelo la operación
            $('.cancel').on('click', function () {
    location.reload();
    });
    });
</script>