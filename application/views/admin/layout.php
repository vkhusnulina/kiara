
<?php $this->load->view('admin/header', ['title' => $section]); ?>

<?php $this->load->view('admin/navigation'); ?>

<div class="page-inner">
    <div class="page-title">
        <div class="row">
            <div class="col-md-6">
                <h3><?php echo $section; ?></h3>
                <small><?php echo $description; ?></small>
            </div>
            <div class="col-md-6 text-right">
                &nbsp;
            </div>
        </div>
    </div>

    <div class="page-wrapper">
        <?php $this->load->view($view, $view_data); ?>
    </div>

    <div id="view-iframe">
        <div id="content-iframe"></div>
        <div id="view-iframe-close" class="view-iframe-close">×</div>
    </div>

    <?php $this->load->view('admin/footer'); ?>