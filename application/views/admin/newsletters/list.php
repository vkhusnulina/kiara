<script>
    $(document).ready(function() {
        var table = $('.table').DataTable({
            order: [[1, 'desc']],
            scrollX: true,
            scrollY: false,
            scrollCollapse: false,
            paging: false,
            ordering: true,
            info: false,
            filter: true,
            autoWidth: false
        });

        $('input[id="search"]').keyup(function() {
            table.search($('input[id="search"]').val()).draw();
        });

        $('#new').on('click', function() {
            $().loadView('categories/new');
        });
    });
</script>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12">
                <form class="form-inline text-right">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="search" class="sr-only control-label">Buscar:</label>
                            <input type="search" id="search" class="form-control input-sm" placeholder="Buscar ...">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="table-responsive no-filter">
        <table class="table table-condensed table-hover table-striped">
            <thead>
                <tr>
                    <th class="w-350 text-center">Correo electrónico</th>
                    <th class="w-350 text-center">Fecha de suscripción</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach (($newsletters ? $newsletters : array()) as $newsletter) { ?>
                    <tr>
                        <td class="w-350 text-center"><?= $newsletter->news_email ?></td>
                        <td class="w-350 text-center"><?= date("d-m-Y H:i:s", strtotime($newsletter->news_created_at)) ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>