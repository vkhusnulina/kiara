<script>
    $(document).ready(function () {
        var table = $('.table').DataTable({
            order: [[1, 'desc']],
            scrollX: true,
            scrollY: false,
            scrollCollapse: false,
            paging: false,
            ordering: true,
            info: false,
            filter: true,
            autoWidth: false
        });

        $('input[id="search"]').keyup(function () {
            table.search($('input[id="search"]').val()).draw();
        });

        $('#new').on('click', function () {
            $().loadView('customers/new');
        });

        $('.view').on('click', function () {
            $().loadView('customers/view/' + $(this).data('id'));
        });
    });
</script>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6">
                <a id="new" href="javascript:;" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar cliente</a>
            </div>
            <div class="col-sm-6">
                <form class="form-inline text-right">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="search" class="sr-only control-label">Buscar:</label>
                            <input type="search" id="search" class="form-control input-sm" placeholder="Buscar ...">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="table-responsive no-filter">
        <table class="table table-condensed table-hover table-striped">
            <thead>
                <tr>
                    <th class="w-20 text-center">&nbsp;</th>
                    <th class="w-100">Nombre</th>
                    <th class="w-100">Apellido</th>
                    <th class="w-350">Email</th>
                    <th class="w-100 text-center">Estado</th>
                    <th class="w-200 text-center">Creado</th>
                    <th class="w-200 text-center">&Uacute;ltimo login</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach (($customers ? $customers : array()) as $customer) { ?>
                    <tr>
                        <td class="w-20 text-center"><a href="javascript:;" class="view" data-id="<?= $customer->id ?>"><i class="fa fa-eye"></i></a></td>
                        <td class="w-100"><?= $customer->prf_name ?></td>
                        <td class="w-100"><?= $customer->prf_surname ?></td>
                        <td class="w-350"><?= $customer->email ?></td>
                        <td class="w-100 text-center">
                            <?php if ($customer->activated) { ?>
                                <span class="label label-success">Activado</span>
                            <?php } else { ?>
                                <span class="label label-info">Pendiente de activaci&oacute;n</span>
                            <?php } ?>
                        </td>
                        <td class="w-200 text-center"><?= date("d-m-Y H:i:s", strtotime($customer->created)) ?></td>
                        <td class="w-200 text-center">
                            <?php if ($customer->last_login != '0000-00-00 00:00:00') { ?>
                                <?= date("d-m-Y H:i:s", strtotime($customer->last_login)); ?>
                            <?php } else { ?>
                                Nunca
                            <?php } ?>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>