
<div class="panel panel-default">
    <div class="panel-heading">Cliente " <?= $customer->prf_name . ' ' . $customer->prf_surname; ?> ".</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12 container">
                <ul class="nav nav-tabs">
                    <li class="nav active"><a href="#customer_tab" data-toggle="tab">Cliente</a></li>
                    <li class="nav"><a href="#orders_tab" data-toggle="tab">Historial de pedidos</a></li>
                </ul>
                <br />
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="customer_tab">
                        <form id="submitNew" action="<?php echo site_url('admin/customers/update/' . $customer->id); ?>" method="post" class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Email :</label>
                                <div class="col-sm-9">
                                    <label class="control-label"><?= $customer->email; ?></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="prf_name" class="col-sm-3 control-label">Nombre (*) :</label>
                                <div class="col-sm-9">
                                    <input type="text" name="prf_name" class="form-control" value="<?= $customer->prf_name; ?>" maxlength="45" required="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="prf_surname" class="col-sm-3 control-label">Apellido (*) :</label>
                                <div class="col-sm-9">
                                    <input type="text" name="prf_surname" class="form-control" value="<?= $customer->prf_surname; ?>" maxlength="45" required="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="prf_birth" class="col-sm-3 control-label">Fecha de nacimiento :</label>
                                <div class="col-sm-9">
                                    <input class="form-control" value="<?= date("d/m/Y", strtotime($customer->prf_birth)); ?>" name="prf_birth" type="text" placeholder="DD/MM/AAAA" id="birth">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="prf_phone" class="col-sm-3 control-label">Tel&eacute;fono :</label>
                                <div class="col-sm-9">
                                    <input type="text" name="prf_phone" class="form-control" value="<?= $customer->prf_phone; ?>" maxlength="30" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add_country" class="col-sm-3 control-label">Pa&iacute;s :</label>
                                <div class="col-sm-9">
                                    <input type="text" name="add_country" class="form-control" value="<?= $customer->add_country; ?>" maxlength="60" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add_city" class="col-sm-3 control-label">Ciudad :</label>
                                <div class="col-sm-9">
                                    <input type="text" name="add_city" class="form-control" value="<?= $customer->add_city; ?>" maxlength="60" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add_locality" class="col-sm-3 control-label">Localidad :</label>
                                <div class="col-sm-9">
                                    <input type="text" name="add_locality" class="form-control" value="<?= $customer->add_locality; ?>" maxlength="60" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add_address" class="col-sm-3 control-label">Direcci&oacute;n :</label>
                                <div class="col-sm-9">
                                    <input type="text" name="add_address" class="form-control" value="<?= $customer->add_address; ?>" maxlength="100" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add_zip_code" class="col-sm-3 control-label">C&oacute;digo postal :</label>
                                <div class="col-sm-9">
                                    <input type="text" name="add_zip_code" class="form-control" value="<?= $customer->add_zip_code; ?>" maxlength="20" />
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default cancel">Cancelar</button>
                                <button type="submit" class="btn btn-primary ladda-button" data-style="zoom-out"><span class="ladda-label">Modificar</span></button>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="orders_tab">
                        <div class="table-responsive no-filter">
                            <table class="table table-condensed table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th class="w-100 text-center">C&oacute;digo</th>
                                        <th class="w-100 text-center">Estado</th>
                                        <th class="w-100 text-center">Pago</th>
                                        <th class="w-100 text-center">Env&iacute;o</th>
                                        <th class="w-200 text-center">Fecha de creaci&oacute;n</th>
                                        <th class="w-200 text-center">Última modificaci&oacute;n</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach (($orders ? $orders : array()) as $order) { ?>
                                        <tr>
                                            <td class="w-20 text-center"><?= $order->ord_id ?></td>
                                            <td class="w-100 text-center">
                                                <?php
                                                $general_status_flag = FALSE;
                                                foreach ($general_status as $key => $status) {
                                                    if ($key == $order->ord_general_status) {
                                                        echo '<span class="label label-' . $status[1] . '">' . $status[0] . '</span>';
                                                        $general_status_flag = TRUE;
                                                    }
                                                }
                                                echo ($general_status_flag ? '' : '<span class="label label-warning">Estado indefinido</span>');
                                                ?>
                                            </td>
                                            <td class="w-100 text-center">
                                                <?php
                                                $payment_status_flag = FALSE;
                                                foreach ($payment_status as $key => $status) {
                                                    if ($key == $order->ord_payment_status) {
                                                        echo '<span class="label label-' . $status[1] . '">' . $status[0] . '</span>';
                                                        $payment_status_flag = TRUE;
                                                    }
                                                }
                                                echo ($payment_status_flag ? '' : '<span class="label label-warning">Estado indefinido</span>');
                                                ?>
                                            </td>
                                            <td class="w-100 text-center">
                                                <?php
                                                $shipping_status_flag = FALSE;
                                                foreach ($shipping_status as $key => $status) {
                                                    if ($key == $order->ord_shipping_status) {
                                                        echo '<span class="label label-' . $status[1] . '">' . $status[0] . '</span>';
                                                        $shipping_status_flag = TRUE;
                                                    }
                                                }
                                                echo ($shipping_status_flag ? '' : '<span class="label label-warning">Estado indefinido</span>');
                                                ?>
                                            </td>
                                            <td class="w-200 text-center"><?= date("d-m-Y H:i:s", strtotime($order->ord_created_at)) ?></td>
                                            <td class="w-200 text-center"><?= date("d-m-Y H:i:s", strtotime($order->ord_modified_at)) ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('#birth').datepicker({
                format: "dd/mm/yyyy"
            });
            // Proceso el formulario de creacion
            $('#submitNew button[type="submit"]').bind('click', function (e) {
                e.preventDefault();
                $('#submitNew').processForm(function () {
                    swal("Modificado!", "El cliente fue modificado con éxito.", "success");
                    /*setTimeout(function () {
                        location.reload();
                    }, 2000);*/
                });
                return false;
            });

            // Cancelo la operación
            $('.cancel').on('click', function () {
                $('.view-iframe-close').click();
            });
        });
    </script>