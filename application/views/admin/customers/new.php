
<div class="panel panel-default">
    <div class="panel-heading">Nuevo cliente.</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12">

                <form id="submitNew" action="<?php echo site_url('admin/customers/insert'); ?>" method="post" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Email (*) :</label>
                        <div class="col-sm-9">
                            <input type="text" name="email" class="form-control" value="" maxlength="45" required="" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="prf_name" class="col-sm-3 control-label">Nombre (*) :</label>
                        <div class="col-sm-9">
                            <input type="text" name="prf_name" class="form-control" value="" maxlength="45" required="" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="prf_surname" class="col-sm-3 control-label">Apellido (*) :</label>
                        <div class="col-sm-9">
                            <input type="text" name="prf_surname" class="form-control" value="" maxlength="45" required="" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="prf_birth" class="col-sm-3 control-label">Fecha de nacimiento :</label>
                        <div class="col-sm-9">
                            <input class="form-control" value="" name="prf_birth" type="text" placeholder="DD/MM/AAAA" id="birth">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="prf_phone" class="col-sm-3 control-label">Tel&eacute;fono :</label>
                        <div class="col-sm-9">
                            <input type="text" name="prf_phone" class="form-control" value="" maxlength="30" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add_country" class="col-sm-3 control-label">Pa&iacute;s :</label>
                        <div class="col-sm-9">
                            <input type="text" name="add_country" class="form-control" value="" maxlength="60" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add_city" class="col-sm-3 control-label">Ciudad :</label>
                        <div class="col-sm-9">
                            <input type="text" name="add_city" class="form-control" value="" maxlength="60" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add_locality" class="col-sm-3 control-label">Localidad :</label>
                        <div class="col-sm-9">
                            <input type="text" name="add_locality" class="form-control" value="" maxlength="60" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add_address" class="col-sm-3 control-label">Direcci&oacute;n :</label>
                        <div class="col-sm-9">
                            <input type="text" name="add_address" class="form-control" value="" maxlength="100" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add_zip_code" class="col-sm-3 control-label">C&oacute;digo postal :</label>
                        <div class="col-sm-9">
                            <input type="text" name="add_zip_code" class="form-control" value="" maxlength="20" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default cancel">Cancelar</button>
                        <button type="submit" class="btn btn-primary ladda-button" data-style="zoom-out"><span class="ladda-label">Finalizar alta</span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('#birth').datepicker({
                format: "dd/mm/yyyy"
            });
            // Proceso el formulario de creacion
            $('#submitNew button[type="submit"]').bind('click', function (e) {
                e.preventDefault();
                $('#submitNew').processForm(function () {
                    swal("Agregado!", "El cliente fue agregado con éxito.", "success");
                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                });
                return false;
            });

            // Cancelo la operación
            $('.cancel').on('click', function () {
                $('.view-iframe-close').click();
            });
        });
    </script>