<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <title>Dashboard - <?php echo $title; ?></title>

        <!-- CSS Style -->
        <link href="<?php echo base_url('assets/admin/css/style.css'); ?>" rel="stylesheet" type="text/css" />

        <!-- jQuery -->
        <script src="<?php echo base_url('assets/admin/js/jquery.min.js'); ?>"></script>
        <!-- Bootstrap's JavaScript -->
        <script src="<?php echo base_url('assets/admin/js/bootstrap.min.js'); ?>"></script>      
        <!-- Multi Select Bootstrap -->
        <script src="<?php echo base_url('assets/admin/js/bootstrap-select.js'); ?>"></script>  
        <!-- Datatables -->
        <script src="<?php echo base_url('assets/admin/js/jquery.dataTables.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/admin/js/dataTables.bootstrap.js'); ?>"></script>
        <!-- Ladda -->
        <script src="<?php echo base_url('assets/admin/js/spin.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/admin/js/ladda.min.js'); ?>"></script>
        <!-- Sweet Alert -->
        <script src="<?php echo base_url('assets/admin/js/sweet-alert.min.js'); ?>"></script>
        <!-- ProcessForm -->
        <script src="<?php echo base_url('assets/admin/js/jquery.processForm.js'); ?>"></script>
        <!-- Datapicker Bootstrap -->
        <script src="<?php echo base_url('assets/admin/js/bootstrap-datepicker.js'); ?>"></script>        
        <!-- InputFile Bootstrap -->
        <script src="<?php echo base_url('assets/admin/js/fileinput.min.js'); ?>"></script>        
        <script src="<?php echo base_url('assets/admin/js/fileinput_locale_es.js'); ?>"></script>        

        <script>
            // LOADER DIV **************************************************************
            var loader = '<i class="fa fa-refresh fa-spin"></i>';
            var loader1x = '<span style="margin:auto; display:table;"><i class="fa fa-refresh fa-spin fa-1x"></i></span>';

            $(document).ready(function() {
                $(".dropdown").hover(
                        function() {
                            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true, true).slideDown("fast");
                            $(this).toggleClass('open');
                        },
                        function() {
                            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true, true).slideUp("fast");
                            $(this).toggleClass('open');
                        }
                );

                /**
                 * Mostrar ventana vertical
                 * @param {String} url
                 * @returns {Boolean}
                 */

                $.fn.loadView = function(url) {
                    var iframe = $('#view-iframe');
                    var iframeContent = $('#view-iframe #content-iframe');
                    var iframeClose = $('.view-iframe-close');

                    iframeContent.html(loader).load(url);

                    iframe.addClass('active');

                    iframeClose.on('click', function() {
                        iframeContent.html('');
                        iframe.removeClass('active');
                    });

                    return true;
                };
            });
        </script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>