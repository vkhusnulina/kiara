<div class="panel panel-default">
    <div class="panel-heading">Pedido &numero;<?= $order->ord_id ?>. Cliente <?= $order->prf_name . ' ' . $order->prf_surname ?>.</div>
    <div class="panel-body">
        <div class="row">
            <div class="container col-sm-12">
                <ul class="nav nav-tabs">
                    <li class="nav active"><a href="#order_tab" data-toggle="tab">Pedido</a></li>
                    <li class="nav"><a href="#details_tab" data-toggle="tab">Detalle</a></li>
                    <li class="nav"><a href="#resume_tab" data-toggle="tab">Resumen</a></li>
                    <li class="nav"><a href="#history_tab" data-toggle="tab">Historial</a></li>
                </ul>
                <br />
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="order_tab">
                        <?php $this->load->view('admin/orders/order', ['order' => $order, 'general_status' => $general_status, 'payment_status' => $payment_status, 'shipping_status' => $shipping_status]); ?>
                    </div>
                    <div class="tab-pane fade" id="details_tab">
                        <?php $this->load->view('admin/orders/details', ['order_items' => $order_items]); ?>
                    </div>
                    <div class="tab-pane fade" id="resume_tab">
                        <?php $this->load->view('admin/orders/summary', ['order_items' => $order_items]); ?>
                    </div>
                    <div class="tab-pane fade" id="history_tab">
                        <?php $this->load->view('admin/orders/history', ['order_history' => $order_history]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.selectpicker').selectpicker();

        //Capturo modificaciones en el formulario de direccion
        $('.address_order').on('change keyup paste', function () {
            $('input[name="address_change"]').attr('value', '1');
        });
        //Capturo modificaciones en el formulario de facturacion
        $('.invoice_order').on('change keyup paste', function () {
            $('input[name="invoice_change"]').attr('value', '1');
        });
        $('.shipping_data').on('change keyup paste', function () {
            $('input[name="shipping_change"]').attr('value', '1');
        });
        $('.ship_to').on('change keyup paste', function () {
            $('input[name="ship_to_change"]').attr('value', '1');
        });

        //Capturo modificaciones en el campo de comentario
        $('textarea[name="ord_comments"]').on('change keyup paste', function () {
            $('input[name="comment_change"]').attr('value', '1');
        });

        //Empresa de transporte
        $('select[name="ord_transport"]').on('change', function () {
            if($(this).val() == '0'){
                $('input[name="ord_shipping_company"]').fadeOut();
            }else{
                $('input[name="ord_shipping_company"]').fadeIn();
            }
        });

        // Proceso el formulario de modificacion de orden
        $('#submitNew button[type="submit"]').bind('click', function (e) {
            e.preventDefault();
            $('#submitNew').processForm(function () {
                swal("Modificado!", "El pedido fue modificado con éxito.", "success");
                /*setTimeout(function () {
                    location.reload();
                }, 2000);*/
            });
            return false;
        });

        // Proceso el formulario de modificacion de items
        $('.submitOrderItem').submit(function (e) {
            e.preventDefault();
            $(this).processForm(function () {
                swal("Modificado!", "El item del pedido fue modificado con éxito.", "success");
                /*setTimeout(function () {
                    location.reload();
                }, 2000);*/
            });
            return false;
        });

        // Cancelo la operación
        $('.cancel').on('click', function () {
            $('.view-iframe-close').click();
        });
    });
</script>