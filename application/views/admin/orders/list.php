<script>
    $(document).ready(function () {
        var table = $('.table').DataTable({
            order: [[1, 'desc']],
            scrollX: true,
            scrollY: false,
            scrollCollapse: false,
            paging: false,
            ordering: true,
            info: false,
            filter: true,
            autoWidth: false
        });

        $('input[id="search"]').keyup(function () {
            table.search($('input[id="search"]').val()).draw();
        });

        $('#search_all').on('click', function () {
            table.column([4]).search('', true).draw();
        });

        $('#search_active').on('click', function () {
            table.column([4]).search('PENDIENTE|REVISADO', true).draw();
        });

        $('.view').on('click', function () {
            $().loadView("<?= base_url('admin/orders/view') ?>/" + $(this).data('id'));
        });
    });
</script>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6">
                <a id="search_active" href="javascript:;" class="btn btn-default"><i class="fa fa-list-ul"></i> Listar solo activos</a>
                <a id="search_all" href="javascript:;" class="btn btn-default"><i class="fa fa-list"></i> Listar todos</a>
            </div>
            <div class="col-sm-6">
                <form class="form-inline text-right">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="search" class="sr-only control-label">Buscar:</label>
                            <input type="search" id="search" class="form-control input-sm" placeholder="Buscar ...">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="table-responsive no-filter">
        <table class="table table-condensed table-hover table-striped">
            <thead>
                <tr>
                    <th class="w-20 text-center">&nbsp;</th>
                    <th class="w-100 text-center">C&oacute;digo</th>
                    <th class="w-350">Cliente</th>
                    <th class="w-350">Email</th>
                    <th class="w-100 text-center">Estado</th>
                    <th class="w-100 text-center">Pago</th>
                    <th class="w-100 text-center">Env&iacute;o</th>
                    <th class="w-200 text-center">Fecha de creaci&oacute;n</th>
                    <th class="w-200 text-center">Última modificaci&oacute;n</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach (($orders ? $orders : array()) as $order) { ?>
                    <tr>
                        <td class="w-20 text-center"><a href="javascript:;" class="view" data-id="<?= $order->ord_id ?>"><i class="fa fa-eye"></i></a></td>
                        <td class="w-20 text-center"><?= $order->ord_id ?></td>
                        <td class="w-350"><?= $order->prf_name . ' ' . $order->prf_surname ?></td>
                        <td class="w-350"><?= $order->email ?></td>
                        <td class="w-100 text-center">
                            <?php
                            $general_status_flag = FALSE;
                            foreach ($general_status as $key => $status) {
                                if ($key == $order->ord_general_status) {
                                    echo '<span class="label label-'.$status[1].'">'.$status[0].'</span>';
                                    $general_status_flag = TRUE;
                                }
                            }
                            echo ($general_status_flag ? '' : '<span class="label label-warning">Estado indefinido</span>');
                            ?>
                        </td>
                        <td class="w-100 text-center">
                            <?php
                            $payment_status_flag = FALSE;
                            foreach ($payment_status as $key => $status) {
                                if ($key == $order->ord_payment_status) {
                                    echo '<span class="label label-'.$status[1].'">'.$status[0].'</span>';
                                    $payment_status_flag = TRUE;
                                }
                            }
                            echo ($payment_status_flag ? '' : '<span class="label label-warning">Estado indefinido</span>');
                            ?>
                        </td>
                        <td class="w-100 text-center">
                            <?php
                            $shipping_status_flag = FALSE;
                            foreach ($shipping_status as $key => $status) {
                                if ($key == $order->ord_shipping_status) {
                                    echo '<span class="label label-'.$status[1].'">'.$status[0].'</span>';
                                    $shipping_status_flag = TRUE;
                                }
                            }
                            echo ($shipping_status_flag ? '' : '<span class="label label-warning">Estado indefinido</span>');
                            ?>
                        </td>
                        <td class="w-200 text-center"><?= date("d-m-Y H:i:s", strtotime($order->ord_created_at)) ?></td>
                        <td class="w-200 text-center"><?= date("d-m-Y H:i:s", strtotime($order->ord_modified_at)) ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>