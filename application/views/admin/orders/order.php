<form id="submitNew" action="<?php echo site_url('admin/orders/update/' . $order->ord_id); ?>" method="post" class="form-horizontal" role="form">
    <div class="form-group">
        <label class="col-sm-3 control-label">Cliente :</label>
        <div class="col-sm-9">
            <label class="control-label"><?= $order->prf_name . ' ' . $order->prf_surname ?></label><br/>
            <label class="control-label"><?= $order->email ?></label><br/>
            <label class="control-label">Tel. <?= $order->prf_phone ?></label>
        </div>
    </div>
    <div class="form-group">
        <label for="ord_general_status" class="col-sm-3 control-label">Estado general:</label>
        <div class="col-sm-9">
            <select name="ord_general_status" class="form-control">
                <?php
                $general_status_flag = FALSE;
                foreach ($general_status as $key => $status) {
                    echo '<option value="' . $key . '"';
                    if ($key == $order->ord_general_status) {
                        echo ' selected=""';
                        $general_status_flag = TRUE;
                    }
                    echo '>' . $status[0];
                    echo '</option>';
                }
                echo ($general_status_flag ? '' : '<option selected="" hidden=""></option>');
                ?>
            </select>
        </div>
        <div class="col-sm-9 col-sm-offset-3">
            <label class="control-label">(&Uacute;ltima modificaci&oacute;n: <?= date("d-m-Y H:i:s", strtotime($order->ord_general_status_date)) ?>)</label>
        </div>
    </div>
    <div class="form-group">
        <label for="ord_payment_status" class="col-sm-3 control-label">Estado de pago:</label>
        <div class="col-sm-9">
            <select name="ord_payment_status" class="form-control">
                <?php
                $payment_status_flag = FALSE;
                foreach ($payment_status as $key => $status) {
                    echo '<option value="' . $key . '"';
                    if ($key == $order->ord_payment_status) {
                        echo ' selected=""';
                        $payment_status_flag = TRUE;
                    }
                    echo '>' . $status[0];
                    echo '</option>';
                }
                echo ($payment_status_flag ? '' : '<option selected="" hidden=""></option>');
                ?>
            </select>
        </div>
        <div class="col-sm-9 col-sm-offset-3">
            <label class="control-label">(&Uacute;ltima modificaci&oacute;n: <?= date("d-m-Y H:i:s", strtotime($order->ord_payment_status_date)) ?>)</label>
        </div>
    </div>
    <div class="form-group">
        <label for="ord_shipping_status" class="col-sm-3 control-label">Estado de env&iacute;o:</label>
        <div class="col-sm-9">
            <select name="ord_shipping_status" class="form-control">
                <?php
                $shipping_status_flag = FALSE;
                foreach ($shipping_status as $key => $status) {
                    echo '<option value="' . $key . '"';
                    if ($key == $order->ord_shipping_status) {
                        echo ' selected=""';
                        $shipping_status_flag = TRUE;
                    }
                    echo '>' . $status[0];
                    echo '</option>';
                }
                echo ($shipping_status_flag ? '' : '<option selected="" hidden=""></option>');
                ?>
            </select>
        </div>
        <div class="col-sm-9 col-sm-offset-3">
            <label class="control-label">(&Uacute;ltima modificaci&oacute;n: <?= date("d-m-Y H:i:s", strtotime($order->ord_shipping_status_date)) ?>)</label>
        </div>
    </div>
    <div class="form-group">
        <label for="ord_ship_to" class="col-sm-3 control-label">Enviar pedido a :</label>
        <div class="col-sm-9">
            <select name="ord_ship_to" class="form-control ship_to">
                <?php
                if ($order->ord_ship_to == 1) {
                    echo '<option selected="" value="1">Terminal</option>';
                    echo '<option value="0">Domicilio</option>';
                } else {
                    echo '<option selected="" value="0">Domicilio</option>';
                    echo '<option value="1">Terminal</option>';
                }
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="ord_transport" class="col-sm-3 control-label">Transporte :</label>
        <div class="col-sm-9">
            <select name="ord_transport" class="form-control shipping_data">
                <?php
                if ($order->ord_transport == '1') {
                    echo '<option selected="" value="1">Sí</option>';
                    echo '<option value="0">No</option>';
                } else {
                    echo '<option selected="" value="0">No</option>';
                    echo '<option value="1">Sí</option>';
                }
                ?>
            </select>
        </div>
        <div class="col-sm-9 col-sm-offset-3">
            <input type="text" name="ord_shipping_company" class="form-control shipping_data" 
                   value="<?= $order->ord_shipping_company; ?>" maxlength="60"
                   <?= ($order->ord_transport == '0') ? 'style="display:none;"' : '' ?>
                   placeholder="Empresa de transporte"/>
        </div>
    </div>
    <div class="form-group">
        <label for="ord_comments" class="col-sm-3 control-label">Comentarios:</label>
        <div class="col-sm-9">
            <textarea name="ord_comments" class="form-control" maxlength="255"><?= $order->ord_comments; ?></textarea>
        </div>
    </div>

    <h4>Direcci&oacute;n de env&iacute;o</h4>
    <div class="form-group">
        <label for="add_country" class="col-sm-3 control-label">Pa&iacute;s :</label>
        <div class="col-sm-9">
            <input type="text" name="add_country" class="form-control address_order" value="<?= $order->add_country; ?>" maxlength="60" />
        </div>
    </div>
    <div class="form-group">
        <label for="add_city" class="col-sm-3 control-label">Ciudad :</label>
        <div class="col-sm-9">
            <input type="text" name="add_city" class="form-control address_order" value="<?= $order->add_city; ?>" maxlength="60" />
        </div>
    </div>
    <div class="form-group">
        <label for="add_locality" class="col-sm-3 control-label">Localidad :</label>
        <div class="col-sm-9">
            <input type="text" name="add_locality" class="form-control address_order" value="<?= $order->add_locality; ?>" maxlength="60" />
        </div>
    </div>
    <div class="form-group">
        <label for="add_address" class="col-sm-3 control-label">Direcci&oacute;n :</label>
        <div class="col-sm-9">
            <input type="text" name="add_address" class="form-control address_order" value="<?= $order->add_address; ?>" maxlength="100" />
        </div>
    </div>
    <div class="form-group">
        <label for="add_zip_code" class="col-sm-3 control-label">C&oacute;digo postal :</label>
        <div class="col-sm-9">
            <input type="text" name="add_zip_code" class="form-control address_order" value="<?= $order->add_zip_code; ?>" maxlength="20" />
        </div>
    </div>

    <h4>Datos de facturaci&oacute;n</h4>
    <div class="form-group">
        <label class="col-sm-3 control-label">Factura :</label>
        <div class="col-sm-9">
            <label class="control-label"><?= ($order->inv_invoice == 'c') ? 'Consumidor final' : strtoupper($order->inv_invoice) ?></label>
        </div>
    </div>
    <?php if ($order->inv_invoice != 'c'): ?>
        <div class="form-group">
            <label for="inv_social_reazon_mr" class="col-sm-3 control-label">
                <?= ($order->inv_invoice == 'a') ? 'Razon social' : 'Sr(es)' ?> :
            </label>
            <div class="col-sm-9">
                <input type="text" name="inv_social_reazon_mr" class="form-control invoice_order" value="<?= $order->inv_social_reazon_mr; ?>" maxlength="45" />
            </div>
        </div>
        <?php if ($order->inv_invoice == 'a') { ?>
            <div class="form-group">
                <label for="inv_registered_manager" class="col-sm-3 control-label">Responsable inscripto :</label>
                <div class="col-sm-9">
                    <select name="inv_registered_manager" class="form-control invoice_order">
                        <?php
                        if ($order->inv_registered_manager == 1) {
                            echo '<option selected="" value="1">SI</option>';
                            echo '<option value="0">NO</option>';
                        } else {
                            echo '<option selected="" value="0">NO</option>';
                            echo '<option value="1">SI</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
        <?php } ?>
        <div class="form-group">
            <label for="inv_address" class="col-sm-3 control-label">Domicilio :</label>
            <div class="col-sm-9">
                <input type="text" name="inv_address" class="form-control invoice_order" value="<?= $order->inv_address; ?>" maxlength="45" />
            </div>
        </div>
        <div class="form-group">
            <label for="inv_locality" class="col-sm-3 control-label">Localidad :</label>
            <div class="col-sm-9">
                <input type="text" name="inv_locality" class="form-control invoice_order" value="<?= $order->inv_locality; ?>" maxlength="45" />
            </div>
        </div>
        <div class="form-group">
            <label for="inv_province" class="col-sm-3 control-label">Provincia :</label>
            <div class="col-sm-9">
                <input type="text" name="inv_province" class="form-control invoice_order" value="<?= $order->inv_province; ?>" maxlength="45" />
            </div>
        </div>
        <div class="form-group">
            <label for="inv_cuit_cuil" class="col-sm-3 control-label">
                <?= ($order->inv_invoice == 'a') ? 'CUIT' : 'CUIT/CUIL' ?> :
            </label>
            <div class="col-sm-9">
                <input type="text" name="inv_cuit_cuil" class="form-control invoice_order" value="<?= $order->inv_cuit_cuil; ?>" maxlength="45" />
            </div>
        </div>
    <?php else: ?>
        <div class="form-group">
            <label for="inv_cuit_cuil" class="col-sm-3 control-label">DNI :</label>
            <div class="col-sm-9">
                <input type="text" name="inv_cuit_cuil" class="form-control invoice_order" value="<?= $order->inv_cuit_cuil; ?>" maxlength="45" />
            </div>
        </div>
    <?php endif; ?>
    <!-- Capture changes -->
    <input type="hidden" name="general" value="<?= $order->ord_general_status; ?>"/>
    <input type="hidden" name="payment" value="<?= $order->ord_payment_status; ?>"/>
    <input type="hidden" name="shipping" value="<?= $order->ord_shipping_status; ?>"/>
    <input type="hidden" name="address_change" value="0"/>
    <input type="hidden" name="invoice_change" value="0"/>
    <input type="hidden" name="shipping_change" value="0"/>
    <input type="hidden" name="ship_to_change" value="0"/>
    <input type="hidden" name="comment_change" value="0"/>

    <div class="modal-footer">
        <button type="button" class="btn btn-default cancel">Cancelar</button>
        <button type="submit" class="btn btn-primary ladda-button" data-style="zoom-out"><span class="ladda-label">Guardar cambios</span></button>
    </div>
</form>
