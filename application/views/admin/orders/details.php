<div class="panel-group">
    <?php foreach (($order_items ? $order_items : array()) as $key => $item) { ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="form-group">
                    <?= $item->prod_name . ' código: ' . $item->prod_code ?>
                </div>
            </div>
            <div class="panel-body">
                <form id="updateItem<?= $item->itm_id ?>" action="<?php echo site_url('admin/orders/update_item/' . $item->itm_id); ?>" method="post" class="form-horizontal submitOrderItem itemForm" role="form">

                    <div class="form-group">
                        <label for="itm_unit_price" class="col-sm-3 control-label">Precio unitario :</label>
                        <div class="col-sm-9">
                            <input type="text" name="item[<?= $item->itm_id ?>][itm_unit_price]" class="form-control" value="<?= $item->itm_unit_price; ?>" maxlength="8" />
                        </div>
                        <div class="col-sm-9 col-sm-offset-3">
                            <label class="control-label">(Precio actual del producto $<?= $item->prod_price ?>)</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="itm_quantity" class="col-sm-3 control-label">Cantidad :</label>
                        <div class="col-sm-9">
                            <input type="number" name="item[<?= $item->itm_id ?>][itm_quantity]" class="form-control" value="<?= $item->itm_quantity ?>" maxlength="3" min="0" />
                        </div>
                    </div>
                    <!--<div class="form-group">
                        <label for="itm_color_id" class="col-sm-3 control-label">Color / Textura :</label>
                        <div class="col-sm-9">
                            <select class="selectpicker form-control" name="itm_color_id">
                    <?php
                    /* $color_flag = FALSE;
                      foreach ($item->colors as $color) {
                      echo '<option';
                      echo ' value="' . $color->clr_id . '"';
                      if ($color->clr_id == $item->itm_color_id) {
                      echo ' selected';
                      $color_flag = TRUE;
                      }
                      echo ' data-content="<img src='
                      . "'"
                      . base_url('assets/admin/images/colors/')
                      . '/'
                      . $color->clr_image
                      . "' style='width: 25px; height: 25px;'"
                      . "></span>&nbsp;<span style='display:inline;'>"
                      . $color->clr_value
                      . '"';
                      echo $color->clr_value;
                      echo '</option>';
                      }
                      if (!$color_flag) {
                      echo '<option';
                      echo ' value="' . $item->clr_id . '"';
                      echo ' selected';
                      echo ' data-content="<img src='
                      . "'"
                      . base_url('assets/admin/images/colors/')
                      . '/'
                      . $item->clr_image
                      . "' style='width: 25px; height: 25px;'"
                      . "></span>&nbsp;<span style='display:inline;'>"
                      . $item->clr_value
                      . '"';
                      echo $item->clr_value;
                      echo '</option>';
                      } */
                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="itm_size_id" class="col-sm-3 control-label">Talle :</label>
                        <div class="col-sm-9">
                            <select class="selectpicker form-control" name="itm_size_id">
                    <?php /*
                      $size_flag = FALSE;
                      foreach ($item->sizes as $size) {
                      echo '<option';
                      echo ' value="' . $size->siz_id . '"';
                      if ($size->siz_id == $item->itm_size_id) {
                      echo ' selected';
                      $size_flag = TRUE;
                      }
                      echo ($size->siz_shoes == 1 ? ' data-subtext="Calzado" ' : '');
                      echo '>';
                      echo $size->siz_value;
                      echo '</option>';
                      }
                      if (!$size_flag) {
                      echo '<option';
                      echo ' value="' . $item->siz_id . '"';
                      echo ' selected';
                      echo ($item->siz_shoes == 1 ? ' data-subtext="Calzado" ' : '');
                      echo '>';
                      echo $item->siz_value;
                      echo '</option>';
                      } */
                    ?>
                            </select>
                        </div>
                    </div>
                    -->
                    <div class="form-group">
                        <label for="itm_comments" class="col-sm-3 control-label">Comentarios:</label>
                        <div class="col-sm-9">
                            <textarea name="item[<?= $item->itm_id ?>][itm_comments]" class="form-control" maxlength="255"><?= $item->itm_comments; ?></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="item[<?= $item->itm_id ?>][itm_order_id]" value="<?= $item->itm_order_id; ?>"/>
                    <!--<div class="modal-footer">
                        <button type="submit" class="btn btn-primary ladda-button" data-style="zoom-out"><span class="ladda-label">Modificar</span></button>
                    </div>-->
                </form>
            </div>
        </div>
    <?php } ?>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default cancel">Cancelar</button>
    <button id="btnSubmitAll" class="btn btn-primary ladda-button" data-style="zoom-out"><span class="ladda-label">Modificar</span></button>
</div>
<script>
    $(document).ready(function () {
        $('#btnSubmitAll').click(function () {
            $('#btnSubmitAll').html(loader);
            var serializedData = "";
            $(".itemForm").each(function () {
                serializedData = serializedData + '&' + $(this).serialize();
            });
            $.post("<?= base_url('admin/orders/update_item') ?>", serializedData, function (data) {
                if (data['error'] != false) {
                    swal("Modificado!", "La orden fue modificada con éxito.", "success");
                    setTimeout(function () {
                        location.reload();
                    }, 3000);
                }
            });
        });
    });
</script>