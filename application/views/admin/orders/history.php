<div class="table-responsive no-filter">
    <table class="table table-condensed table-hover table-striped">
        <thead>
            <tr>
                <th>Fecha</th>
                <th>Usuario</th>
                <th>Acci&oacute;n</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($order_history as $key => $item) { ?>
                <tr>
                    <td><?= date("d-m-Y H:i:s", strtotime($item->hst_created_at)) ?></td>
                    <td><?= $item->email ?></td>
                    <td><?= $item->hst_description ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>