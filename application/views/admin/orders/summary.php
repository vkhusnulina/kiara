<div class="table-responsive no-filter">
    <table class="table table-condensed table-hover table-striped">
        <thead>
            <tr>
                <th>C&oacute;digo</th>
                <th>Producto</th>
                <!--
                <th>Color</th>
                <th>Talle</th>
                -->
                <th>Cantidad</th>
                <th>Precio</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            <?php $total = 0; ?>
            <?php foreach (($order_items ? $order_items : array()) as $key => $item) { ?>
                <tr>
                    <td><?= $item->prod_code ?></td>
                    <td><?= $item->prod_name ?></td>
                    <!--
                    <td><?= $item->clr_value ?></td>
                    <td><?= $item->siz_value ?></td>
                    -->
                    <td><?= $item->itm_quantity ?></td>
                    <td>$<?= $item->itm_unit_price ?></td>
                    <td>$<?= $item->itm_unit_price * $item->itm_quantity ?></td>
                    <?php $total = $total + ($item->itm_unit_price * $item->itm_quantity); ?>
                </tr>
            <?php } ?>
        </tbody>
        <tfoot>
            <tr>
                <td></td>
                <td></td>
                <!--
                <td></td>
                <td></td>
                -->
                <td></td>
                <td>Total:</td>
                <td>$<?= $total ?></td>
            </tr>
        </tfoot>
    </table>
</div>