
<div class="panel panel-default">
    <div class="panel-heading">Editar correo electrónico " <?= $correo->config_name; ?> ".</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12 container">
                <form id="submitEdit" action="<?php echo site_url('admin/configuration-emails/save/' . $correo->config_id); ?>" method="post" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="config_value" class="col-sm-3 control-label">Mensaje del correo :</label>
                        <div class="col-sm-9">
                            <textarea name="config_value" class="form-control" style="height: 400px"><?= $correo->config_value; ?></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default cancel">Cancelar</button>
                        <button type="submit" class="btn btn-primary ladda-button" data-style="zoom-out"><span class="ladda-label">Modificar</span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            // Proceso el formulario de creacion
            $('#submitEdit button[type="submit"]').bind('click', function(e) {
                e.preventDefault();
                $('#submitEdit').processForm(function() {
                    swal("Modificado!", "El mensaje fue modificado con éxito.", "success");
                });
                return false;
            });

            // Cancelo la operación
            $('.cancel').on('click', function() {
                $('.view-iframe-close').click();
            });
        });
    </script>