<script>
    $(document).ready(function() {
        var table = $('.table').DataTable({
            order: [[1, 'desc']],
            scrollX: true,
            scrollY: false,
            scrollCollapse: false,
            paging: false,
            ordering: true,
            info: false,
            filter: true,
            autoWidth: false
        });

        $('input[id="search"]').keyup(function() {
            table.search($('input[id="search"]').val()).draw();
        });

        $('.view').on('click', function() {
            $().loadView('configuration-emails/view/' + $(this).data('id'));
        });
    });
</script>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12">
                <form class="form-inline text-right">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="search" class="sr-only control-label">Buscar:</label>
                            <input type="search" id="search" class="form-control input-sm" placeholder="Buscar ...">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="table-responsive no-filter">
        <table class="table table-condensed table-hover table-striped">
            <thead>
                <tr>
                    <th class="w-20 text-center">&nbsp;</th>
                    <th class="w-350">Asunto</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach (($correos ? $correos : array()) as $correo) { ?>
                    <tr>
                        <td class="w-20 text-center"><a href="javascript:;" class="view" data-id="<?= $correo->config_id ?>"><i class="fa fa-eye"></i></a></td>
                        <td class="w-350"><?= $correo->config_name ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>