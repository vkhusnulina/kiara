<div class="panel panel-default">
    <div class="panel-heading">Producto " <?= $product->prod_name; ?> ".</div>
    <div class="panel-body">
        <div class="row">
            <div class="container col-sm-12">
                <form id="submitNew" action="<?php echo site_url('admin/products/update/' . $product->prod_id); ?>" enctype="multipart/form-data" method="post" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="prod_name" class="col-sm-3 control-label">Nombre del producto (*) :</label>
                        <div class="col-sm-9">
                            <input type="text" name="prod_name" class="form-control" value="<?= $product->prod_name; ?>" maxlength="45" required="" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="prod_code" class="col-sm-3 control-label">Código de identificación:</label>
                        <div class="col-sm-9">
                            <input type="text" name="prod_code" class="form-control" value="<?= $product->prod_code; ?>" maxlength="10" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="prod_description" class="col-sm-3 control-label">Descripción:</label>
                        <div class="col-sm-9">
                            <textarea name="prod_description" class="form-control" maxlength="255"><?= $product->prod_description; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="prod_price" class="col-sm-3 control-label">Precio (ARS):</label>
                        <div class="col-sm-9">
                            <input type="text" name="prod_price" class="form-control" value="<?= $product->prod_price; ?>" maxlength="8" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="prod_visible" class="col-sm-3 control-label">Estado:</label>
                        <div class="col-sm-9">
                            <select name="prod_visible" class="form-control">
                                <?php if (!$product->prod_visible): ?>
                                    <option value="0" selected="">OCULTO</option>
                                    <option value="1">VISIBLE</option>
                                <?php else: ?>
                                    <option value="0">OCULTO</option>
                                    <option value="1" selected="">VISIBLE</option>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="categories[]" class="col-sm-3 control-label">Categor&iacute;as :</label>
                        <div class="col-sm-9">
                            <select class="selectpicker form-control" name="categories[]" multiple>
                                <?php
                                foreach ($categories as $category) {
                                    echo '<option';
                                    echo ' value="' . $category->cat_id . '"';
                                    echo ($category->exist ? ' selected' : '');
                                    echo '>';
                                    echo $category->cat_name;
                                    echo '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="colors[]" class="col-sm-3 control-label">Colores / Texturas :</label>
                        <div class="col-sm-9">
                            <select class="selectpicker form-control" name="colors[]" multiple>
                                <?php
                                foreach ($colors as $color) {
                                    echo '<option';
                                    echo ' value="' . $color->clr_id . '"';
                                    echo ($color->exist ? ' selected' : '');
                                    echo ' data-content="<img src='
                                    . "'"
                                    . base_url('assets/admin/images/colors/')
                                    . '/'
                                    . $color->clr_image
                                    . "' style='width: 25px; height: 25px;'"
                                    . "></span>&nbsp;<span style='display:inline;'>"
                                    . $color->clr_value
                                    . '"';
                                    echo $color->clr_value;
                                    echo '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sizes[]" class="col-sm-3 control-label">Talles :</label>
                        <div class="col-sm-9">
                            <select class="selectpicker form-control" name="sizes[]" multiple>
                                <optgroup label="Talles">
                                    <?php
                                    foreach ($sizes as $size) {
                                        echo '<option';
                                        echo ' value="' . $size->siz_id . '"';
                                        echo ($size->exist ? ' selected' : '');
                                        echo '>';
                                        echo $size->siz_value;
                                        echo '</option>';
                                    }
                                    ?>
                                </optgroup>
                                <!--<optgroup label="Talles de Calzado">
                                    <?php
                                    foreach ($shoes_sizes as $size) {
                                        echo '<option';
                                        echo ($size->exist ? ' selected' : '');
                                        echo '>';
                                        echo $size->siz_value;
                                        echo '</option>';
                                    }
                                    ?>
                                </optgroup>-->
                            </select>
                        </div>
                    </div>
                    <?php if (count($images) > 0): ?>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Im&aacute;genes existentes:</label>
                            <div class="col-sm-9">
                                <input id="previous_images" disabled="" type="file" multiple="true" data-show-upload="false" data-show-remove="false" data-show-caption="false">
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="form-group">
                        <label for="prod_img[]" class="col-sm-3 control-label">Agregar im&aacute;genes:</label>
                        <div class="col-sm-9">
                            <input id="new_images" name="prod_img[]" type="file" multiple="true" data-show-upload="false" data-show-caption="true">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default cancel">Cancelar</button>
                        <button type="submit" class="btn btn-primary ladda-button" data-style="zoom-out"><span class="ladda-label">Modificar</span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- PreviewImage
<script src="<?php echo base_url('assets/admin/js/jquery.previewImage.js'); ?>"></script>-->
<script>
    $(document).ready(function () {
        $('.selectpicker').selectpicker();
        // initialize with defaults
        $("#new_images").fileinput({
            language: "es",
            allowedFileExtensions: ["jpg", "gif", "png"]
        });
        //Vista previa de las imagenes existentes
<?php if (count($images) > 0): ?>
            $("#previous_images").fileinput({
                language: "es",
                allowedFileExtensions: ["jpg", "gif", "png"],
                initialPreview: [
    <?php foreach ($images as $image): ?>
                        "<img src='<?= base_url('assets/admin/images/products/' . $image->media_name) ?>' class='file-preview-image' alt='' title=''>",
    <?php endforeach; ?>
                ],
                // initial preview configuration
                initialPreviewConfig: [
    <?php foreach ($images as $image): ?>
                        {
                            url: '<?= site_url('admin/products/delete_image/') . '/' . $image->media_id ?>',
                                    key: <?= $image->media_id ?>,
                            extra: {
                                id: <?= $image->media_id ?>
                            }
                        },
    <?php endforeach; ?>
                ]});
<?php endif; ?>

        // Proceso el formulario de creacion
        $('#submitNew button[type="submit"]').bind('click', function (e) {
            e.preventDefault();
            $('#submitNew').processForm(function () {
                swal("Modificado!", "El producto fue actualizado con éxito.", "success");
                /*setTimeout(function () {
                    location.reload();
                }, 2000);*/
            });
            return false;
        });
        // Cancelo la operación
        $('.cancel').on('click', function () {
            $('.view-iframe-close').click();
        });
    });
</script>