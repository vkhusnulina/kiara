<div class="panel panel-default">
    <div class="panel-heading">Crear producto.</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12">
                <form id="submitNew" action="<?php echo site_url('admin/products/insert'); ?>" method="post" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="prod_name" class="col-sm-3 control-label">Nombre del producto (*) :</label>
                        <div class="col-sm-9">
                            <input type="text" name="prod_name" class="form-control" value="" maxlength="45" required="" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="prod_code" class="col-sm-3 control-label">Código de identificación:</label>
                        <div class="col-sm-9">
                            <input type="text" name="prod_code" class="form-control" value="" maxlength="10" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="prod_description" class="col-sm-3 control-label">Descripción:</label>
                        <div class="col-sm-9">
                            <textarea name="prod_description" class="form-control" maxlength="255"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="prod_price" class="col-sm-3 control-label">Precio (ARS):</label>
                        <div class="col-sm-9">
                            <input type="text" name="prod_price" class="form-control" value="" maxlength="8" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="prod_visible" class="col-sm-3 control-label">Estado:</label>
                        <div class="col-sm-9">
                            <select name="prod_visible" class="form-control">
                                <option value="0" selected="">OCULTO</option>
                                <option value="1">VISIBLE</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="categories[]" class="col-sm-3 control-label">Categor&iacute;as :</label>
                        <div class="col-sm-9">
                            <select class="selectpicker form-control" name="categories[]" multiple>
                                <?php
                                foreach ($categories as $category) {
                                    echo '<option';
                                    echo ' value="' . $category->cat_id . '"';
                                    echo '>';
                                    echo $category->cat_name;
                                    echo '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="colors[]" class="col-sm-3 control-label">Colores / Texturas :</label>
                        <div class="col-sm-9">
                            <select class="selectpicker form-control" name="colors[]" multiple>
                                <?php
                                foreach ($colors as $color) {
                                    echo '<option';
                                    echo ' value="' . $color->clr_id . '"';
                                    echo ' data-content="<img src='
                                    . "'"
                                    . base_url('assets/admin/images/colors/')
                                    . '/'
                                    . $color->clr_image
                                    . "' style='width: 25px; height: 25px;'"
                                    . "></span>&nbsp;<span style='display:inline;'>"
                                    . $color->clr_value
                                    . '"';
                                    echo $color->clr_value;
                                    echo '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sizes[]" class="col-sm-3 control-label">Talles :</label>
                        <div class="col-sm-9">
                            <select class="selectpicker form-control" name="sizes[]" multiple>
                                <optgroup label="Talles">
                                    <?php
                                    foreach ($sizes as $size) {
                                        echo '<option';
                                        echo ' value="' . $size->siz_id . '"';
                                        echo '>';
                                        echo $size->siz_value;
                                        echo '</option>';
                                    }
                                    ?>
                                </optgroup>
                                <!--<optgroup label="Talles de Calzado">
                                    <?php
                                    foreach ($shoes_sizes as $size) {
                                        echo '<option';
                                        echo '>';
                                        echo $size->siz_value;
                                        echo '</option>';
                                    }
                                    ?>
                                </optgroup>-->
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="prod_img[]" class="col-sm-3 control-label">Imagenes:</label>
                        <div class="col-sm-9">
                            <input id="input-2" name="prod_img[]" type="file" multiple="true" data-show-upload="false" data-show-caption="true">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default cancel">Cancelar</button>
                        <button type="submit" class="btn btn-primary ladda-button" data-style="zoom-out"><span class="ladda-label">Finalizar alta</span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- PreviewImage -->
<script src="<?php echo base_url('assets/admin/js/jquery.previewImage.js'); ?>"></script>
<script>
    $(document).ready(function () {
        $('.selectpicker').selectpicker();
        
        // initialize with defaults
        $("#input-2").fileinput({
            language: "es",
            allowedFileExtensions: ["jpg", "gif", "png"]
        });

        // Proceso el formulario de creacion
        $('#submitNew button[type="submit"]').bind('click', function (e) {
            e.preventDefault();

            $('#submitNew').processForm(function () {
                swal("Ingresada!", "El producto fue creado con éxito.", "success");

                setTimeout(function () {
                    location.reload();
                }, 2000);
            });

            return false;
        });

        // Cancelo la operación
        $('.cancel').on('click', function () {
            $('.view-iframe-close').click();
        });
    });
</script>