<section id="guia-talles" class="container">
    <div class="tituloPage">
        <h1> GUIA DE TALLES </h1>
    </div>
    <div class="row content">
        <div class="col-lg-12 text-center m-b-30">
            <img src="<?= base_url('assets/Resources/guiatalles.jpg') ?>" alt="" class="img-responsive full-width" />
        </div>
        <div class="container-min">
            <div class="col-lg-12 text-left">
                <p class="colorRed text-uppercase border-b-red m-b-30">Buscá tus medidas en nuestra tabla de talles:</p>

                <p class="colorRed">TOPS: Remeras, Musculosas, Camisas, Vestidos, Blazers</p>

                <img src="<?= base_url('assets/Resources/guiatalles_1.png') ?>" alt="" class="img-responsive full-width m-b-30" />

                <p class="bold italic subrayado">¿Cómo medir tu Busto?</p>

                <p class="font-normal m-b-50">Con los brazos relajados alrededor de tu cuerpo, medí con un centímetro la vuelta entera de busto y espalda.</p>

                <p class="colorRed">BOTTOMS: Polleras, Shorts, Monos, Jeans, Calzas</p>

                <img src="<?= base_url('assets/Resources/guiatalles_2.png') ?>" alt="" class="img-responsive full-width m-b-30" />

                <p class="bold italic subrayado">Cómo medir tu Cintura?</p>

                <p class="font-normal">Paráte de forma cómoda y colocá el centímetro alrededor de la parte más chica de tu cintura. Para asegurarte un calce cómodo, no lo ajustes demasiado.</p>

                <p class="bold italic subrayado">¿Cómo medir mi Cadera?</p>

                <p class="font-normal m-b-50">Paráte de forma cómoda con las piernas apenas separadas y colocá el centímetro en la parte más ancha de tus caderas. Esta zona generalmente se corresponde con la parte media del glúteo.</p>


                <div class="info m-b-50">
                    <p class="colorRed text-uppercase">Prendas Talle Único</p>

                    <p class="font-normal ">Todas las prendas que sean Talle Único están cortadas de fábrica en Talle M / 2 o L / 3 y generalmente son bases o de materiales que pueden adaptarse a todos los cuerpos sin ver afectado el diseño o la calidad de calce de la misma.</p>
                </div>
            </div>
        </div>
    </div>
</section>