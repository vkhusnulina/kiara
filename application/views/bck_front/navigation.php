<nav>
    <div class="divContainerLoginData">
        <div class="container">
            <div class="row">
                <div class="col-xs-12" >
                    <?php if ($this->tank_auth->is_logged_in()): ?>
                        <a href="<?= base_url('mi_cuenta') ?>"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <?= $this->session->userdata('username') ?></a>
                        <span> / </span>
                        <a href="<?= base_url('auth/logout') ?>">Logout</a>
                    <?php else: ?>
                        <a href="<?= base_url('auth/login') ?>">
                            <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Iniciar Sesión  </a>
                        <span> / </span>
                        <a href="<?= base_url('auth/login') ?>">Crear cuenta</a>
                    <?php endif; ?>
                    <div class="divCarrito">
                        <a id="drop2" href="#resumen-carrito" data-toggle="collapse">
                            <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Mi carrito (<?= ($this->session->userdata("shopping_cart") > 0) ? $this->session->userdata("shopping_cart") : '0' ?>)
                            <span class="caret"></span>
                        </a>
                        <!--
                        <div class="dropdown">
                            <a id="drop2" href="<?= base_url('mi_carrito') ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Mi carrito (<?= ($this->session->userdata("shopping_cart") > 0) ? $this->session->userdata("shopping_cart") : '0' ?>)
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="drop2">
                                <li><a href="<?= base_url('mi_carrito') ?>">Ver productos</a></li>
                            </ul>
                        </div>
                        -->
                    </div>

                    <div id="resumen-carrito" class="collapse">
                        <h3 class="colorRed text-uppercase">Carrito de compras</h3>
                        <div class="row">
                            <?php foreach (($this->session->userdata("shopping_items") ? $this->session->userdata("shopping_items") : array()) as $key => $item): ?>
                                <div class="col-sm-6 <?= ( $key & 1 ) ? '' : 'border-right' ?>">
                                    <div class="row <?= ( $key > 1 ) ? 'margin-top-30' : '' ?>">
                                        <div class="col-sm-4" style="padding-right: 0 !important;">
                                            <img src="<?= base_url('assets/admin/images/products/' . $item['image']) ?>"/>
                                        </div>
                                        <div class="col-sm-8">
                                            <b><?= strtoupper($item['prod_name']) ?></b>
                                            <span class="shopping-data">C&oacute;digo: <?= $item['prod_code'] ?></span>
                                            <span class="shopping-data">Cantidad: <?= $item['itm_quantity'] ?></span>
                                            <span class="price">$<?= $item['itm_price'] ?></span>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <?php if ($this->session->userdata("shopping_cart") > 4): ?>
                            <div class="row alignCenter" class="margin-top: 15px;">
                                <a class="verDetalleLink" style="color: #af0118; text-decoration: underline;" href="<?= base_url('mi_carrito') ?>">VER TODAS LAS PRENDAS &gt;</a>
                            </div>
                        <?php endif; ?>
                        <div class="row tipoFacturacionRadioButton alignRight shopping-total">
                            Total: $<?= $this->session->userdata("shopping_total") ?>
                        </div>
                        <div class="row">
                            <div class="col-sm-6"><a href="<?= base_url('productos') ?>" class="volverLink">Volver a productos &gt; </a></div>
                            <div class="col-sm-6 alignRight"><button class="btnSuscripcion" onclick="location.href = '<?= base_url('mi_carrito') ?>';" >IR AL CARRITO</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row divNavegacion">
            <div class="col-xs-5  linksLeftNav">
                <a href="<?= base_url() ?>" >
                    HOME
                </a>
                <span class="separador">|</span>
                <a href="<?= base_url('productos') ?>" >
                    CATÁLOGO DE PRODUCTOS
                </a>
                <span class="separador">|</span>
                <a href="<?= base_url('como-comprar') ?>" >
                    CÓMO COMPRAR
                </a>
            </div>
            <div class="col-xs-2 divLogoPrincipal">
                <a href="<?= base_url() ?>" >
                    <img src="<?= base_url('assets/Resources/logo.png') ?>" alt="Kiara" class="img-responsive" >
                </a>
            </div>
            <div class="col-xs-5 containerRedesLinksRight ">
                <div class="divRedes">
                    <a href="#" class="facebook"></a>
                    <a href="#" class="twitter"></a>
                    <a href="#" class="instagram"></a>
                </div>
                <div class="linksRightNav ">
                    <a href="<?= base_url('guia-de-talles') ?>" >
                        GUIA DE TALLES
                    </a>
                    <span class="separador">|</span>
                    <a href="<?= base_url('donde-encontrarnos') ?>" >
                        DONDE ENCONTRARNOS
                    </a>
                    <span class="separador">|</span>
                    <a href="<?= base_url('contacto') ?>" >
                        CONTACTO
                    </a>
                </div>
            </div>
        </div>
    </div>
</nav>
<script>
                        $('body,html').on('click', function(e) {

                            if (!$(e.target).is($('#resumen-carrito, #resumen-carrito *'))) {
                                $('#resumen-carrito').collapse('hide');
                            }

                        });
</script>