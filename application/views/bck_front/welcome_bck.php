<section class="divSlideshow">
    <div class="mi_galeria ">
        <div class="cycle-slideshow"
             data-cycle-fx=scrollHorz
             data-cycle-timeout=3000
             data-cycle-pager-event="mouseover"
             data-cycle-caption-plugin="caption2"
             data-cycle-carousel-visible=5
             data-cycle-carousel-fluid=true
             >
            <div class="cycle-pager"></div>
            <?php foreach ($slideshow as $image) { ?>
                <img src="<?= base_url('assets/Resources/slideshow/' . $image->media_name) ?>"/>
            <?php } ?>
        </div>
    </div>
</section>

<section class="container divContainerCategoriasHome">
    <div class="row">
        <div class="col-xs-12 noPaddingLeft noPaddingRight">
            <div class="col-xs-2 col-xs-15">
                <a href="anticipo-nueva-temporada" class="grisearImagen">
                    <img src="<?= base_url('assets/Resources/imagen1.png') ?>" alt="Nueva temporada" class="img-responsive" />
                    <span class="categoriaHomeTexto top-18">Nueva temporada</span>
                </a>
            </div>
            <div class="col-xs-2 col-xs-15">
                <a href="sweaters-abrigos" class="grisearImagen">
                    <img src="<?= base_url('assets/Resources/imagen2.png') ?>" alt="Sweaters" class="img-responsive" />
                    <span class="categoriaHomeTexto">Sweaters</span>
                </a>
            </div>
            <div class="col-xs-2 col-xs-15">
                <a href="vestidos-polleras" class="grisearImagen">
                    <img src="<?= base_url('assets/Resources/imagen3.png') ?>" alt="Vestidos" class="img-responsive" />
                    <span class="categoriaHomeTexto">Vestidos</span>
                </a>
            </div>
            <div class="col-xs-2 col-xs-15">
                <a href="calzas-pantalones" class="grisearImagen">
                    <img src="<?= base_url('assets/Resources/imagen4.png') ?>" alt="Pantalones" class="img-responsive" />
                    <span class="categoriaHomeTexto">Pantalones</span>
                </a>
            </div>
            <div class="col-xs-2 col-xs-15">
                <a href="rebajas" class="grisearImagen">
                    <img src="<?= base_url('assets/Resources/imagen5.png') ?>" alt="Rebajas" class="img-responsive" />
                    <span class="categoriaHomeTexto">Rebajas</span>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="container">
    <div class="divSuscripcion">

        <?php if (isset($_GET['news']) && $_GET['news'] == 1) { ?>
            <div class="alert alert-success">
                Gracias por suscribirte a nuestro newsletter.
            </div>
        <?php } ?>

        <span class="spanSuscribite">
            Suscríbite a nuestro correo electrónico!
        </span>
        <div class="divContainerSuscripcionInputs">
            <form action="<?= base_url('add_newsletter'); ?>" method="post">
                <input type="text" placeholder="E-Mail" class="emailInput" name="news_email"/>
                <input type="submit" value="SUSCRIBIRME" class="btnSuscripcion"/>
            </form>
        </div>
    </div>
</section>