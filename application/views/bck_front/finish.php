<section class="container contenedorCatalogoProductos">
    <div class="tituloPage">
        <h1 >FINALIZAR PEDIDO</h1>
    </div>
    <img src="<?= base_url('assets/Resources/micarrito/04finalizarpedido.png') ?>" class="img-responsive imagenCarritoCompras" />
    <div class="finalizarPedidoTextoDiv">
        <img src="<?= base_url('assets/Resources/micarrito/finalizarpedido.png') ?>" />
        <p class="boldText">Tu pedido se ha enviado correctamente.</p>
        <span>El número de tu pedido es:</span>
        <span class="boldText"><?= $order->ord_id ?></span>
        <div class="finalizarEnvio">
            <p>En el transcurso de 48hr recibirás un correo electrónico de confirmación con los detalles del mismo
                y un enlace para seguir todo el proceeso.</p>

            <p>Podés hacer el seguimiento de tu compra ingresando a tu cuenta.</p>
        </div>

    </div>
    <div class="btnFinalizarDiv">
        <a class="botonRojo" href="<?= base_url('productos') ?>">VOLVER A CATÁLOGO</a>
        <a class="botonNegro" href="<?= base_url('mi_cuenta') ?>">IR A MI CUENTA</a>
    </div>
</section>