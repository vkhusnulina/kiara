<section class="container contenedorCatalogoProductos">
    <div class="tituloPage">
        <h1 > PRODUCTOS </h1>
    </div>
    <div class="row">
        <div class="col-xs-3 menuCatalogoProducto">

            <div class="TitulomenuCatalogoProducto"> <?= $category->cat_name ?> </div>
            <ul>
                <?php foreach ($products as $product_item) { ?>
                    <li>
                        <a href="<?= base_url($category->cat_slug . '/' . $product_item->prod_slug) ?>"
                           class="<?= ($product_item->prod_id == $product->prod_id) ? 'colorRed' : '' ?>">
                            <span class="productoMenuCatalogoProducto"><?= $product_item->prod_name ?></span>
                            <span class="codigoMenuCatalogoProducto">#<?= $product_item->prod_code ?></span>
                        </a>
                    </li>
                <?php } ?>
            </ul>


            <?php
            foreach ($categories as $other_cat) {
                if ($other_cat->cat_id != $category->cat_id) {
                    ?>
                    <div class="otrasCategoriasCatalogoProductoMenu">
                        <h2><a href="<?= $other_cat->cat_slug ?>"><?= $other_cat->cat_name ?></a></h2>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <div class="col-xs-9 colImagenesProductosCatalogoProductos">
            <div>
                <ul class="breadcrumb">
                    <li><a href="productos">Productos</a></li>
                    <li><a href="<?= base_url() . $category->cat_slug ?>"><?= $category->cat_name ?></a></li>
                    <li class="active"><?= $product->prod_name ?></li>
                </ul>
            </div>

            <div class="row ">
                <div class="col-xs-6">
                    <div style="width:350px;">
                        <
                        <div id="slider1_container" style="position: relative; width: 350px; height: 400px; overflow: hidden;">
                            <div u="loading" style="position: absolute; top: 0px; left: 0px;">
                                <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                                     background-color: #000; top: 0px; left: 0px;width: 100%;height:100%;">
                                </div>
                                <div style="position: absolute; display: block; background: url(<?= base_url('assets/Resources/detalleProducto/loading.gif') ?>) no-repeat center center;
                                     top: 0px; left: 0px;width: 100%;height:100%;">
                                </div>
                            </div>

                            <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 350px; height: 300px;
                                 overflow: hidden;">
                                 <?php foreach ($images as $image): ?>
                                    <div>
                                        <img u="image" class="imageSliderProducto" src="<?= base_url('assets/admin/images/products/' . $image->media_name) ?>" />
                                        <img u="thumb" src="<?= base_url('assets/admin/images/products/' . $image->media_name) ?>" />
                                    </div>
                                <?php endforeach; ?>
                            </div>

                            <div u="thumbnavigator" class="jssort07" style="width: 350px; height: 100px; left: 0px; bottom: 0px;">
                                <div u="slides" style="cursor: default;">
                                    <div u="prototype" class="p">
                                        <div u="thumbnailtemplate" class="i"></div>
                                        <div class="o"></div>
                                    </div>
                                </div>

                                <span u="arrowleft" class="jssora11l" style="top: 123px; left: 8px; width: 37px; height:37px;">
                                </span>

                                <span u="arrowright" class="jssora11r" style="top: 123px; right: 8px; width: 37px; height:37px;">
                                </span>
                            </div>
                        </div>
                        <?php if ($product->prod_description): ?>
                            <div class="descripcionProductoDetalle">
                                <h5>DESCRIPCI&Oacute;N</h5>
                                <p><?= $product->prod_description ?></p>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-xs-6">
                    <?php if ($this->tank_auth->is_logged_in()): ?>
                        <span class="precioProductoDetalle">
                            $<?= $product->prod_price ?>
                        </span>
                    <?php else: ?>
                        <div class="contornoBlanco">
                            <a href="<?= base_url('auth/login') ?>">
                                <div class="botonRojo">
                                    <img src="<?= base_url('assets/Resources/header_iconocarrito1.png') ?>" />
                                    <span>CONSULTAR PRECIO</span>
                                </div>
                            </a>
                        </div>
                        &nbsp;
                    <?php endif; ?>
                    <div class="contornoBlanco">
                        <a  href="<?= base_url('mi_carrito') ?>">
                            <div class="botonRojo">
                                <img src="<?= base_url('assets/Resources/header_iconocarrito1.png') ?>" />
                                <span>VER MI PEDIDO</span>
                            </div>
                        </a>
                    </div>
                    <div class="containerNombreProductoDetalle">
                        <span class="nombreProductoDetalle">
                            <?= strtoupper($product->prod_name) ?>
                        </span>
                    </div>

                    <p class="idProductoDetalle">
                        CÓDIGO
                        <br/>
                        <?= $product->prod_code ?>
                    </p>
                    <div class="estampasProductoDetalle">
                        <?php if (!empty($colors)): ?>
                            <span >
                                ESTAMPAS
                            </span>
                            <br/>
                        <?php endif; ?>
                        <?php foreach ($colors as $color): ?>
                            <img src="<?= base_url('assets/admin/images/colors/' . $color->clr_image) ?>" data-toggle="tooltip" data-placement="top" title="<?= $color->clr_value ?>" />
                        <?php endforeach; ?>
                    </div>

                    <div class="estampasProductoDetalle">
                        <?php if (!empty($sizes)): ?>
                            <span >
                                TALLE
                            </span>
                            <br/>
                        <?php endif; ?>
                        <?php foreach ($sizes as $size): ?>
                            <div class="talleProductoDetalle">
                                <span><?= $size->siz_value ?></span>
                            </div>
                        <?php endforeach; ?>
                        <?php if (!empty($sizes)): ?>
                            <!--<div class="botonNegro">
                                <span><a href="<?= base_url('guia-de-talles') ?>" class="font-white">GUÍA DE TALLES</a></span>
                            </div>-->
                        <?php endif; ?>
                    </div>
                    <div class="cantidadProductoDetalle">
                        <span>CANTIDAD: </span>
                        <select id="quantity">
                            <?php for ($i = 1; $i <= 50; $i++): ?>
                                <option value="<?= $i ?>"><?= $i ?></option>
                            <?php endfor; ?>
                        </select>
                    </div>
                    <div class="detalleProductoDetalle">
                        <h5>DETALLE DEL PEDIDO: </h5>
                        <span>No obligatorio. Especifique talle y color </span>

                        <textarea class="vert" id="description"></textarea>
                    </div>
                    <div class="containerBotonAgregarCarrito">
                        <div class="contornoBlanco">
                            <a <?= ($this->tank_auth->is_logged_in()) ? 'id="agregarAlCarrito" href="javascript:void(0);"' : 'href="' . base_url('auth/login') . '"' ?> >
                                <div class="botonRojo">
                                    <span>AGREGAR AL CARRITO</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div id="addItemOk" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Carrito de compra</h4>
            </div>
            <div class="modal-body">
                <p>Producto agregado con &eacute;xito.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>

    </div>
</div>
<div id="addItemFiled" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Carrito de compra</h4>
            </div>
            <div class="modal-body">
                <p>Error al agregar el producto al carrito.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript" src="<?= base_url('assets/js/jssor.js') ?>" ></script>
<script type="text/javascript" src="<?= base_url('assets/js/jssor.slider.js') ?>" ></script>
<script type="text/javascript" src="<?= base_url('assets/js/jssorConfigurable.js') ?>" ></script>

<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();

        $('.imageSliderProducto').css('width', 'auto');
        $('.imageSliderProducto').css('height', 'auto');

        $('#agregarAlCarrito').click(function () {
            $.ajax({
                type: 'POST',
                url: '<?= base_url('agregar_al_carrito') ?>',
                data: {
                    product: '<?= $product->prod_id ?>',
                    quantity: $('#quantity').val(),
                    description: $('#description').val()
                },
                success: function (response) {
                    if (!response['error']) {
                        $("#addItemOk").modal({backdrop: true});
                        $("#addItemOk").on('hidden.bs.modal', function () {
                            location.reload();
                        });
                    } else {
                        $("#addItemFiled").modal({backdrop: true});
                    }
                }
            });
        });
    });
</script>