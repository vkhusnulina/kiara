<section class="container contenedorCatalogoProductos">
    <div class="tituloPage">
        <h1 >INICIAR SESIÓN / CREAR CUENTA</h1>
    </div>
    <div class="row containerIniciarSesion">
        <div class="col-xs-5 iniciarSesionDiv">
            <h3>Iniciar Sesión</h3>
            <form id="login_form" action="<?= base_url('auth/login') ?>" method="post" accept-charset="utf-8">
                <input type="text" placeholder="E-mail:" name="login" value="" id="login" maxlength="80">
                <input placeholder="Contraseña" type="password" name="password" value="" id="password" size="30">
                <div class="recuperarPassLink">
                    <a href="<?= base_url('auth/forgot_password') ?>" >¿olvidó la contraseña?</a>
                </div>
                <a href="javascript:;" id="login_submit" ><img src="<?= base_url('assets/Resources/iniciarSesion/sesion_boton.png') ?>" onmouseover="this.src = '<?= base_url('assets/Resources/iniciarSesion/sesion_botonhover.png') ?>'" onmouseout="this.src = '<?= base_url('assets/Resources/iniciarSesion/sesion_boton.png') ?>'" /></a>
            </form>
            <?php if (!empty($validation_login) || !empty($login['errors'])): ?>
                <div class="alert alert-danger" role="alert">
                    <?php foreach (($login['errors'] ? $login['errors'] : array()) as $error): ?>
                        <?= '<p>' . $error . '</p>' ?>
                    <?php endforeach; ?>
                    <?= $validation_login ?>
                </div>
            <?php endif; ?>
        </div>
        <script>
                    $('#login_submit').click(function() {
                        $('#login_form').submit();
                    });
        </script>
        <div class="col-xs-5 col-xs-offset-2 crearCuentaDiv">
            <h3>Crear cuenta</h3>
            <?= $this->session->flashdata('message') ?>
            <form id="register_form" action="<?= base_url('auth/register') ?> " method="post" accept-charset="utf-8">
                <input type="text" name="prf_name" placeholder="Nombre:" class="mitadWidthInput">
                <input type="text" name="prf_surname" placeholder="Apellido:" class="mitadWidthInputDerecha">
                <input type="text" name="email" placeholder="Email:">
                <input type="text" name="add_locality" placeholder="Localidad:">
                <input type="text" name="add_country" placeholder="Pais:" class="mitadWidthInput">
                <input type="text" name="add_city" placeholder="Ciudad:" class="mitadWidthInputDerecha">
                <input type="text" name="add_address" placeholder="Domicilio:">
                <input type="text" name="add_zip_code" placeholder="Código Postal:" class="mitadWidthInput">
                <input type="text" name="prf_phone" placeholder="Teléfono:" class="mitadWidthInputDerecha">
                <input type="hidden" name="prf_birth" value="<?= date('Y-m-d') ?>">
                <p class="fechaNacimientoP">Fecha de nacimiento: </p>
                <div class="row divFechaNacimiento">
                    <div class="col-xs-4 noPaddingLeft">
                        <span>DD</span>
                        <input maxlength="2" id="birth_day" type="text">
                    </div>
                    <div class="col-xs-4">
                        <span>MM</span>
                        <input maxlength="2" id="birth_month" type="text">
                    </div>
                    <div class="col-xs-4">
                        <span>AAAA</span>
                        <input maxlength="4" id="birth_year" type="text">
                    </div>
                </div>
                <input type="password" name="password" placeholder="Contraseña" class="mitadWidthInput">
                <input type="password" name="confirm_password" placeholder="Repetir contraseña" class="mitadWidthInputDerecha">
                <!--
                <input type="checkbox" id="divTermCond" class="divTermCond"> <span class="divTermCondTexto"> Acepto <u>Términos y condiciones de uso</u> y registrarme al newsletter.</span>
                -->
                <div class="crearCuentaBoton">
                    <a href="javascript:;" id="register_submit" ><img src="<?= base_url('assets/Resources/iniciarSesion/cuenta_boton.png') ?>" onmouseover="this.src = '<?= base_url('assets/Resources/iniciarSesion/cuenta_botonhover.png') ?>'" onmouseout="this.src = '<?= base_url('assets/Resources/iniciarSesion/cuenta_boton.png') ?>'" /></a>
                </div>
            </form>
            <?php if (!empty($validation_register) || !empty($register['errors'])): ?>
                <div class="alert alert-danger" role="alert">
                    <?php foreach (($register['errors'] ? $register['errors'] : array()) as $error): ?>
                        <?= '<p>' . $error . '</p>' ?>
                    <?php endforeach; ?>
                    <?= $validation_register ?>
                </div>
            <?php endif; ?>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="modalDanger" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="border-gray">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Error</h4>

                            <div class="alert alert-danger" role="alert">
                                <p style="font-size: 20px;" id="messageErr"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="modalOk" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="border-gray" style="padding-bottom: 42px; padding-top: 42px;">
                            <h2 class="modal-title colorRed">¡ FELICIDADES !</h2>
                            <div>
                                <p style="font-size: 20px;" id="messageOK"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
                    $(document).ready(function() {
                        var flashdata = "<?= $this->session->flashdata('message') ?>";
                        if (flashdata.length > 0) {
                            $('#messageOK').text(flashdata);
                            $('#modalOk').modal();
                        }

                        $('#register_submit').click(function() {
                            /*if ($('#divTermCond').is(':checked')) {*/
                            var re = new RegExp(/^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/);
                            var datastr = $('#birth_day').val() + '/' + $('#birth_month').val() + '/' + $('#birth_year').val();
                            var datamysql = $('#birth_year').val() + '-' + $('#birth_month').val() + '-' + $('#birth_day').val();
                            if (datastr.match(re)) {
                                $('[name="prf_birth"]').val(datamysql);
                            }
                            $('#register_form').submit();
                            /*} else {
                             $('#messageErr').text('Debe aceptar "Términos y Condiciones de uso".');
                             $('#modalDanger').modal();
                             }*/
                        });
                    });
        </script>
    </div>


</section>