
<?php $this->load->view('header', ['title' => $section]); ?>

<?php $this->load->view('navigation'); ?>

<?php $this->load->view($view, $view_data); ?>

<?php $this->load->view('footer'); ?>

