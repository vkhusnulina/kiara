<section class="container contenedorCatalogoProductos">
    <div class="tituloPage">
        <h1 > PRODUCTOS </h1>
    </div>
    <div class="row">
        <div class="col-xs-3 menuCatalogoProducto">

            <div class="TitulomenuCatalogoProducto"> <?= $category->cat_name ?> </div>
            <ul>
                <?php foreach ($products as $product_item) { ?>
                    <li>
                        <a href="<?= base_url($category->cat_slug . '/' . $product_item->prod_slug) ?>"
                           class="<?= ($product_item->prod_id == $product->prod_id) ? 'colorRed' : '' ?>">
                            <span class="productoMenuCatalogoProducto"><?= $product_item->prod_name ?></span>
                            <span class="codigoMenuCatalogoProducto">#<?= $product_item->prod_code ?></span>
                        </a>
                    </li>
                <?php } ?>
            </ul>


            <?php
            foreach ($categories as $other_cat) {
                if ($other_cat->cat_id != $category->cat_id) {
                    ?>
                    <div class="otrasCategoriasCatalogoProductoMenu">
                        <h2><a href="<?= base_url($other_cat->cat_slug) ?>"><?= $other_cat->cat_name ?></a></h2>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <div class="col-xs-9 colImagenesProductosCatalogoProductos">
            <div>
                <ul class="breadcrumb">
                    <li><a href="productos">Productos</a></li>
                    <li><a href="<?= base_url() . $category->cat_slug ?>"><?= $category->cat_name ?></a></li>
                    <li class="active"><?= $product->prod_name ?></li>
                </ul>
            </div>

            <div class="row ">
                <div class="col-xs-6">
                    <div style="">
                        <div id="slider1_container">
                            <div style="min-height: 350px">
                                <img id="img_01" style="max-width: 350px; max-height: 350px;" src="<?= base_url('assets/admin/images/products/' . $images[0]->media_name) ?>" />
                            </div>
                            <div id="gal1" style="clear: both;">
                                <?php foreach ($images as $image): ?>
                                    <a href="#" data-image="<?= base_url('assets/admin/images/products/' . $image->media_name) ?>"><img style="max-width: 80px;" id="img_01" src="<?= base_url('assets/admin/images/products/' . $image->media_name) ?>" /></a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <?php if ($product->prod_description): ?>
                            <div class="descripcionProductoDetalle">
                                <h5>DESCRIPCI&Oacute;N</h5>
                                <p><?= $product->prod_description ?></p>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-xs-6">
                    <?php if ($this->tank_auth->is_logged_in()): ?>
                        <span class="precioProductoDetalle">
                            $<?= $product->prod_price ?>
                        </span>
                    <?php else: ?>
                        <div class="contornoBlanco">
                            <a href="<?= base_url('auth/login') ?>">
                                <div class="botonRojo">
                                    <img src="<?= base_url('assets/Resources/header_iconocarrito1.png') ?>" />
                                    <span>CONSULTAR PRECIO</span>
                                </div>
                            </a>
                        </div>
                        &nbsp;
                    <?php endif; ?>
                    <div class="contornoBlanco">
                        <a  href="<?= base_url('mi_carrito') ?>">
                            <div class="botonRojo">
                                <img src="<?= base_url('assets/Resources/header_iconocarrito1.png') ?>" />
                                <span>VER MI PEDIDO</span>
                            </div>
                        </a>
                    </div>
                    <div class="containerNombreProductoDetalle">
                        <span class="nombreProductoDetalle">
                            <?= strtoupper($product->prod_name) ?>
                        </span>
                    </div>

                    <p class="idProductoDetalle">
                        CÓDIGO
                        <br/>
                        <?= $product->prod_code ?>
                    </p>
                    <div class="estampasProductoDetalle">
                        <?php if (!empty($colors)): ?>
                            <span >
                                ESTAMPAS
                            </span>
                            <br/>
                        <?php endif; ?>
                        <?php foreach ($colors as $color): ?>
                            <img src="<?= base_url('assets/admin/images/colors/' . $color->clr_image) ?>" data-toggle="tooltip" data-placement="top" title="<?= $color->clr_value ?>" />
                        <?php endforeach; ?>
                    </div>

                    <div class="estampasProductoDetalle">
                        <?php if (!empty($sizes)): ?>
                            <span >
                                TALLE
                            </span>
                            <br/>
                        <?php endif; ?>
                        <?php foreach ($sizes as $size): ?>
                            <div class="talleProductoDetalle">
                                <span><?= $size->siz_value ?></span>
                            </div>
                        <?php endforeach; ?>
                        <?php if (!empty($sizes)): ?>
                            <!--<div class="botonNegro">
                                <span><a href="<?= base_url('guia-de-talles') ?>" class="font-white">GUÍA DE TALLES</a></span>
                            </div>-->
                        <?php endif; ?>
                    </div>
                    <div class="cantidadProductoDetalle">
                        <span>CANTIDAD: </span>
                        <select id="quantity">
                            <?php for ($i = 1; $i <= 50; $i++): ?>
                                <option value="<?= $i ?>"><?= $i ?></option>
                            <?php endfor; ?>
                        </select>
                    </div>
                    <div class="detalleProductoDetalle">
                        <h5>DETALLE DEL PEDIDO: </h5>
                        <span>No obligatorio. Especifique talle y color </span>

                        <textarea class="vert" id="description"></textarea>
                    </div>
                    <div class="containerBotonAgregarCarrito">
                        <div class="contornoBlanco">
                            <a <?= ($this->tank_auth->is_logged_in()) ? 'id="agregarAlCarrito" href="javascript:void(0);"' : 'href="' . base_url('auth/login') . '"' ?> >
                                <div class="botonRojo">
                                    <span>AGREGAR AL CARRITO</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div id="addItemOk" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="border-gray">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-8">
                                    <img src="<?= base_url('assets/admin/images/products/' . $images[0]->media_name) ?>" style="width: 100%; height: auto;">
                                </div>
                                <div class="col-sm-4 p-top-25">
                                    <img class="" src="<?= base_url('assets/Resources/carrito.jpg') ?>" style="width: 100%; height: auto;">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 p-top-5 text-left">
                            <span class="volverLink"><?= strtoupper($product->prod_name) ?></span><br />
                            <div class="detalleProductoDetalle">
                                <span>Fue agregado</span>
                                <span>a su carrito.</span>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix" style="height: 20px;"></div>
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="javascript:;" onclick="location.reload();" class="botonRojo" style="line-height: 18px; display: block;">SEGUIR COMPRANDO</a>
                        </div>
                        <div class="col-sm-6">
                            <a href="<?= base_url('mi_carrito') ?>" class="botonNegro" style="display: block; line-height: 24px; padding: 0; margin: 0;">IR AL CARRITO</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div id="addItemFiled" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Carrito de compra</h4>
            </div>
            <div class="modal-body">
                <p>Error al agregar el producto al carrito.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>

    </div>
</div>

<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();

        $("#img_01").elevateZoom({gallery: 'gal1', cursor: 'pointer', galleryActiveClass: 'active', imageCrossfade: true, loadingIcon: 'http://www.elevateweb.co.uk/spinner.gif'});

        $('#agregarAlCarrito').click(function () {
            $.ajax({
                type: 'POST',
                url: '<?= base_url('agregar_al_carrito') ?>',
                data: {
                    product: '<?= $product->prod_id ?>',
                    quantity: $('#quantity').val(),
                    description: $('#description').val()
                },
                success: function (response) {
                    if (!response['error']) {
                        $("#addItemOk").modal({backdrop: true});
                        $("#addItemOk").on('hidden.bs.modal', function () {
                            location.reload();
                        });
                    } else {
                        $("#addItemFiled").modal({backdrop: true});
                    }
                }
            });
        });
    });
</script>