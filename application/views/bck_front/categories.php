<section class="container">
    <div class="tituloPage">
        <h1>PRODUCTOS</h1>
    </div>
    <div class="row">
        <div class="col-xs-3">
            <ul class="listaCategorias">
                <?php foreach ($categories as $category): ?>
                    <li class="<?= ($category === end($categories))?'noBorderBottom':'' ?>">
                        <h2><a href="<?= $category->cat_slug ?>"><?= $category->cat_name ?></a></h2>
                    </li>
                <?php endforeach; ?>
                <!--
                <li class="">
                    <h2><a href="nueva-temporada">Nueva temporada</a></h2>
                </li>
                <li class="">
                    <h2><a href="sweaters">Sweaters</a></h2>
                </li>
                <li class="">
                    <h2><a href="vestidos">Vestidos</a></h2>
                </li>
                <li class="">
                    <h2><a href="pantalones">Pantalones</a></h2>
                </li>
                <li class="noBorderBottom">
                    <h2><a href="rebajas">Rebajas</a></h2>
                </li>
                -->
            </ul>
        </div>
        <div class="col-xs-9">
            <img src="<?= base_url('assets/Resources/CategoriaInicio/1M.png') ?>" class="img-responsive" />
        </div>
    </div>
</section>

<section class="container">
    <div class="divSuscripcion">
        <span class="spanSuscribite">
            Suscríbite a nuestro correo electrónico!
        </span>
        <div class="divContainerSuscripcionInputs">
            <form action="<?= base_url('add_newsletter'); ?>" method="post">
            <input type="text" placeholder="E-Mail" class="emailInput" name="news_email"/>
            <input type="submit" value="SUSCRIBIRME" class="btnSuscripcion"/>
            </form>
        </div>
    </div>
</section>