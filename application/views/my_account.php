<section class="container contenedorCatalogoProductos">
    <div class="tituloPage">
        <h1 >HISTORIAL PEDIDOS / CONFIGURACIÓN CUENTA</h1>
    </div>

    <div class="containerPedidosRecientes row">
        <h3>PEDIDOS RECIENTES: Detalle de la orden</h3>

        <?php if (!empty($orders)): ?>
            <?php $first_order = array_shift($orders); ?>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 noPaddingLeft ">
                    <p>FECHA: <span><?= date("d/m/Y", strtotime($first_order->ord_created_at)) ?></span></p>
                    <p>PEDIDO N°: <span><?= $first_order->ord_id ?></span></p>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 noPaddingLeft ">
                    <p>ESTADO DEL PEDIDO</p>
                    <p class="textoMasChico">Estado de la orden:
                        <span>
                            <?php
                            $general_status_flag = FALSE;
                            foreach ($general_status as $key => $status) {
                                if ($key == $first_order->ord_general_status) {
                                    echo $status[0];
                                    $general_status_flag = TRUE;
                                }
                            }
                            echo ($general_status_flag ? '' : 'Estado indefinido');
                            ?>
                        </span>
                    </p>
                    <p class="textoMasChico">Estado del pago:
                        <span>
                            <?php
                            $payment_status_flag = FALSE;
                            foreach ($payment_status as $key => $status) {
                                if ($key == $first_order->ord_payment_status) {
                                    echo $status[0];
                                    $payment_status_flag = TRUE;
                                }
                            }
                            echo ($payment_status_flag ? '' : 'Estado indefinido');
                            ?>
                        </span>
                    </p>
                    <p class="textoMasChico">Estado del envío:
                        <span>
                            <?php
                            $shipping_status_flag = FALSE;
                            foreach ($shipping_status as $key => $status) {
                                if ($key == $first_order->ord_shipping_status) {
                                    echo $status[0];
                                    $shipping_status_flag = TRUE;
                                }
                            }
                            echo ($shipping_status_flag ? '' : 'Estado indefinido');
                            ?>
                        </span>
                    </p>

                </div>
                <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12 noPaddingLeft totalDelPedidoHistorial">
                    <p>TOTAL DEL PEDIDO</p>
                    <p class="textoMasGrande">$<?= $first_order->total ?></p>
                </div >
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPaddingLeft" >
                    <p class="verDetalleLink">
                        <a href="javascript:;" data-toggle="collapse" data-target="#collapse<?= $first_order->ord_id ?>" class="collapsed">> Ver detalles</a>
                    </p>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPaddingLeft collapse" id="collapse<?= $first_order->ord_id ?>" >
                    <table class="table table-bordered tableMiCarrito">
                        <thead>
                            <tr>
                                <th class="col-xs-3 text-center">PRODUCTO</th>
                                <th class="col-xs-3 hidden-xs text-center">DETALLE</th>
                                <th class="col-xs-1 text-center">CANTIDAD</th>
                                <th class="col-xs-2 text-center">PRECIO UNITARIO </th>
                                <th class="col-xs-1 hidden-xs text-center">SUBTOTAL</th>
                                <?php if ($first_order->edit): ?>
                                    <th class="col-xs-1 hidden-xs text-center">EDITAR</th>
                                    <th class="col-xs-1 hidden-xs text-center">ELIMINAR</th>
                                <?php endif; ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($first_order->items as $item): ?>
                                <tr>
                                    <td class="row text-left">
                                        <div class="row-height">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-height hidden-xs">
                                                <div class="inside">
                                                    <div class="content"><img src="<?= base_url('assets/admin/images/products/' . $item->image) ?>" class="img-responsive"/></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-height col-middle">
                                                <div class="inside">
                                                    <div class="content">
                                                        <p class="nombreProductoMiCarrito" id="prod_name_<?= $item->itm_id ?>"><?= $item->prod_name ?></p>
                                                        <p class="idProductoMiCarrito">Código #<?= $item->prod_code ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="hidden-xs text-left"><p id="itm_comments_<?= $item->itm_id ?>"><?= $item->itm_comments ?></p></td>
                                    <td><p id="itm_quantity_<?= $item->itm_id ?>"><?= $item->itm_quantity ?></p></td>
                                    <td><p>$<?= $item->itm_unit_price ?></p></td>
                                    <td class="hidden-xs"><p>$<?= $item->itm_quantity * $item->itm_unit_price ?></p></td>
                                    <?php if ($first_order->edit): ?>
                                        <td class="hidden-xs"><a data-itm_id="<?= $item->itm_id ?>" class="edit_item" href="javascript:void(0);"><img src="<?= base_url('assets/Resources/micarrito/modificar.png') ?>"></img></a></td>
                                        <td class="hidden-xs"><a data-itm_id="<?= $item->itm_id ?>" class="remove_item" href="javascript:void(0);" style="color: #555;"><i class="glyphicon glyphicon-remove"></i></a></td>
                                    <?php endif; ?>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <br>
            <?php if (count($orders) > 0): ?>
                <p class="botonNegro">
                    <a data-toggle="collapse" href="#moreOrders" aria-expanded="false" aria-controls="moreOrders">
                        Ver Historial de Pedidos
                    </a>
                </p>
                <div class="collapse" id="moreOrders">
                    <br />
                    <?php foreach ($orders as $order): ?>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12  noPaddingLeft ">
                                <p>FECHA: <span><?= date("d/m/Y", strtotime($order->ord_created_at)) ?></span></p>
                                <p>PEDIDO N°: <span><?= $order->ord_id ?></span></p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 noPaddingLeft ">
                                <p>ESTADO DEL PEDIDO</p>
                                <p class="textoMasChico">Estado de la orden:
                                    <span>
                                        <?php
                                        $general_status_flag = FALSE;
                                        foreach ($general_status as $key => $status) {
                                            if ($key == $order->ord_general_status) {
                                                echo $status[0];
                                                $general_status_flag = TRUE;
                                            }
                                        }
                                        echo ($general_status_flag ? '' : 'Estado indefinido');
                                        ?>
                                    </span>
                                </p>
                                <p class="textoMasChico">Estado del pago:
                                    <span>
                                        <?php
                                        $payment_status_flag = FALSE;
                                        foreach ($payment_status as $key => $status) {
                                            if ($key == $order->ord_payment_status) {
                                                echo $status[0];
                                                $payment_status_flag = TRUE;
                                            }
                                        }
                                        echo ($payment_status_flag ? '' : 'Estado indefinido');
                                        ?>
                                    </span>
                                </p>
                                <p class="textoMasChico">Estado del envío:
                                    <span>
                                        <?php
                                        $shipping_status_flag = FALSE;
                                        foreach ($shipping_status as $key => $status) {
                                            if ($key == $order->ord_shipping_status) {
                                                echo $status[0];
                                                $shipping_status_flag = TRUE;
                                            }
                                        }
                                        echo ($shipping_status_flag ? '' : 'Estado indefinido');
                                        ?>
                                    </span>
                                </p>

                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12 noPaddingLeft totalDelPedidoHistorial">
                                <p>TOTAL DEL PEDIDO</p>
                                <p class="textoMasGrande">$<?= $order->total ?></p>
                            </div >
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPaddingLeft" >
                                <p class="verDetalleLink">
                                    <a href="javascript:;" data-toggle="collapse" data-target="#collapse<?= $order->ord_id ?>" class="collapsed">> Ver detalles</a>
                                </p>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPaddingLeft collapse" id="collapse<?= $order->ord_id ?>" >
                                <table class="table table-bordered tableMiCarrito">
                                    <thead>
                                        <tr>
                                            <th class="col-xs-3 text-center">PRODUCTO</th>
                                            <th class="col-xs-3 hidden-xs text-center">DETALLE</th>
                                            <th class="col-xs-1 text-center">CANTIDAD</th>
                                            <th class="col-xs-2 text-center">PRECIO UNITARIO </th>
                                            <th class="col-xs-1 hidden-xs text-center">SUBTOTAL</th>
                                            <?php if ($order->edit): ?>
                                                <th class="col-xs-1 hidden-xs text-center">EDITAR</th>
                                                <th class="col-xs-1 hidden-xs text-center">ELIMINAR</th>
                                            <?php endif; ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($order->items as $item): ?>
                                            <tr>
                                                <td class="row text-left">
                                                    <div class="row-height">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-height hidden-xs">
                                                            <div class="inside">
                                                                <div class="content"><img src="<?= base_url('assets/admin/images/products/' . $item->image) ?>" class="img-responsive"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-height col-middle">
                                                            <div class="inside">
                                                                <div class="content">
                                                                    <p class="nombreProductoMiCarrito" id="prod_name_<?= $item->itm_id ?>"><?= $item->prod_name ?></p>
                                                                    <p class="idProductoMiCarrito">Código #<?= $item->prod_code ?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="hidden-xs text-left"><p id="itm_comments_<?= $item->itm_id ?>"><?= $item->itm_comments ?></p></td>
                                                <td><p id="itm_quantity_<?= $item->itm_id ?>"><?= $item->itm_quantity ?></p></td>
                                                <td><p>$<?= $item->itm_unit_price ?></p></td>
                                                <td class="hidden-xs"><p>$<?= $item->itm_quantity * $item->itm_unit_price ?></p></td>
                                                <?php if ($order->edit): ?>
                                                    <td class="hidden-xs"><a data-itm_id="<?= $item->itm_id ?>" class="edit_item" href="javascript:void(0);"><img src="<?= base_url('assets/Resources/micarrito/modificar.png') ?>"></img></a></td>
                                                    <td class="hidden-xs"><a data-itm_id="<?= $item->itm_id ?>" class="remove_item" href="javascript:void(0);" style="color: #555;"><i class="glyphicon glyphicon-remove"></i></a></td>
                                                <?php endif; ?>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <br>
                    <?php endforeach; ?>
                </div>
                <?php
            endif;
        endif;
        ?>



        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPaddingLeft" style="margin-top:35px;">
            <h3>INFORMACIÓN DE LA CUENTA:</h3>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 noPaddingLeft infoDeLaCuenta">
            <h5>Libreta de direcciones</h5>
            <p class="titulo">Dirección de envío por defecto</p>
            <p><?= $customer->prf_name ?> <?= $customer->prf_surname ?></p>
            <p><?= ($customer->add_address ? $customer->add_address : "[DOMICILIO]") ?></p>
            <p><?= ($customer->add_locality ? $customer->add_locality : "[LOCALIDAD]") ?>, <?= ($customer->add_city ? $customer->add_city : "[CIUDAD]") ?>, <?= ($customer->add_zip_code ? $customer->add_zip_code : "[CÓDIGO POSTAL]") ?></p>
            <p><?= ($customer->add_country ? $customer->add_country : "[PAÍS]") ?></p>
            <br/>
            <p class="italic colorRed" data-toggle="modal" data-target="#editAddressModal" style="cursor:pointer;">editar la dirección</p>
            <br/>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 noPaddingLeft infoDeLaCuenta">
            <h5> &nbsp;<span><!--gestionar direcciones--></span></h5>
            <p class="titulo">Dirección de facturación por defecto</p>
            <p><?= $customer->prf_name ?> <?= $customer->prf_surname ?></p>
            <p><?= ($customer->add_address ? $customer->add_address : "[DOMICILIO]") ?></p>
            <p><?= ($customer->add_locality ? $customer->add_locality : "[LOCALIDAD]") ?>, <?= ($customer->add_city ? $customer->add_city : "[CIUDAD]") ?>, <?= ($customer->add_zip_code ? $customer->add_zip_code : "[CÓDIGO POSTAL]") ?></p>
            <p><?= ($customer->add_country ? $customer->add_country : "[PAÍS]") ?></p>
            <br/>
            <p class="italic">&nbsp; <!--editar la dirección--></p>

            <br/>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 noPaddingLeft infoDeLaCuenta">
            <h5>Información de contacto<span class="colorRed" data-toggle="modal" data-target="#editAccountModal" style="cursor:pointer;">editar</span></h5>
            <p><?= $customer->prf_name ?> <?= $customer->prf_surname ?></p>
            <p><?= $customer->email ?></p>
            <p>Tel&eacute;fono: <?= $customer->prf_phone ? $customer->prf_phone : '[TELÉFONO]' ?></p>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 noPaddingLeft infoDeLaCuenta">
            <h5>Newsletters<span class="colorRed" style="cursor:pointer;">editar</span></h5>
            <p>Actualmente no está suscrito a ningún newsletter</p>
        </div>


    </div>
    <div class="btnFinalizarDiv">
        <a class="botonRojo">VOLVER A CATÁLOGO</a>
    </div>
</section>




<!-- Modal -->
<div id="editItem" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <span class="nombreProductoDetalle modal-title" id="prod_name"></span>
            </div>
            <div class="modal-body">
                <div class="cantidadProductoDetalle" style="margin-top:-5px;">
                    <span>CANTIDAD: </span>
                    <select id="itm_quantity">
                        <?php for ($i = 1; $i <= 50; $i++): ?>
                            <option value="<?= $i ?>"><?= $i ?></option>
                        <?php endfor; ?>
                    </select>
                </div>
                <div class="detalleProductoDetalle">
                    <h5>DETALLE DEL PEDIDO: </h5>
                    <span>No obligatorio. Especifique talle y color </span>

                    <textarea class="vert" id="itm_comments"></textarea>
                </div>
            </div>
            <input type="hidden" id="itm_id" value="" />
            <div class="modal-footer">
                <button type="button" class="botonRojo" id="editItemSave">MODIFICAR</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>
<div id="removeItem" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <span class="nombreProductoDetalle modal-title">&iquest;Seguro que quiere remover "<span id="prod_name_rmv"></span>" del carrito?</span>
            </div>
            <div class="modal-body">
                <div class="cantidadProductoDetalle" style="margin-top:-5px;">
                    <span>CANTIDAD: </span>
                    <span id="itm_quantity_rmv"></span>
                </div>
                <div class="detalleProductoDetalle">
                    <h5>DETALLE DEL PEDIDO: </h5>
                    <div id="itm_comments_rmv" style="font-weight: normal; font-size: 20px;"></div>
                </div>
            </div>
            <input type="hidden" id="itm_id_rmv" value="" />
            <div class="modal-footer">
                <button type="button" class="botonNegro" id="itemRemove">ELIMINAR</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>

<div id="editAccountModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <span class="nombreProductoDetalle modal-title">Editar datos de contacto</span>
            </div>
            <div class="modal-body form-horizontal font-normal" style="font-size: 16px;" id="editAccount">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Email :</label>
                    <div class="col-sm-9">
                        <label class="form-control"><?= $customer->email; ?></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="prf_name" class="col-sm-3 control-label">Nombre (*) :</label>
                    <div class="col-sm-9">
                        <input type="text" name="prf_name" class="form-control" value="<?= $customer->prf_name; ?>" maxlength="45" required="" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="prf_surname" class="col-sm-3 control-label">Apellido (*) :</label>
                    <div class="col-sm-9">
                        <input type="text" name="prf_surname" class="form-control" value="<?= $customer->prf_surname; ?>" maxlength="45" required="" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="prf_birth" class="col-sm-3 control-label">Fecha de nacimiento :</label>
                    <div class="col-sm-9">
                        <input class="form-control" value="<?= date("d/m/Y", strtotime($customer->prf_birth)); ?>" name="prf_birth" type="text" placeholder="DD/MM/AAAA" id="birth">
                    </div>
                </div>
                <div class="form-group">
                    <label for="prf_phone" class="col-sm-3 control-label">Tel&eacute;fono :</label>
                    <div class="col-sm-9">
                        <input type="text" name="prf_phone" class="form-control" value="<?= $customer->prf_phone; ?>" maxlength="30" />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="botonRojo" id="editAccountSave">MODIFICAR</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>

<div id="editAddressModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <span class="nombreProductoDetalle modal-title">Editar direcci&oacute;n</span>
            </div>
            <div class="modal-body form-horizontal font-normal" style="font-size: 16px;" id="editAddress">
                <div class="form-group">
                    <label for="add_country" class="col-sm-3 control-label">Pa&iacute;s :</label>
                    <div class="col-sm-9">
                        <input type="text" name="add_country" class="form-control" value="<?= $customer->add_country; ?>" maxlength="60" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="add_city" class="col-sm-3 control-label">Ciudad :</label>
                    <div class="col-sm-9">
                        <input type="text" name="add_city" class="form-control" value="<?= $customer->add_city; ?>" maxlength="60" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="add_locality" class="col-sm-3 control-label">Localidad :</label>
                    <div class="col-sm-9">
                        <input type="text" name="add_locality" class="form-control" value="<?= $customer->add_locality; ?>" maxlength="60" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="add_address" class="col-sm-3 control-label">Direcci&oacute;n :</label>
                    <div class="col-sm-9">
                        <input type="text" name="add_address" class="form-control" value="<?= $customer->add_address; ?>" maxlength="100" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="add_zip_code" class="col-sm-3 control-label">C&oacute;digo postal :</label>
                    <div class="col-sm-9">
                        <input type="text" name="add_zip_code" class="form-control" value="<?= $customer->add_zip_code; ?>" maxlength="20" />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="botonRojo" id="editAddressSave">MODIFICAR</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        // textarea
        $("#itm_comments").change(function () {
            $(this).text($(this).val());
        });
        $('#itemRemove').click(function () {
            $.ajax({
                type: 'POST',
                url: '<?= base_url('remover_item_del_pedido') ?>/' + $('#itm_id_rmv').val(),
                data: {},
                success: function (response) {
                    if (!response['error']) {
                        location.reload();
                    }
                }
            });
        });
        $('#editItemSave').click(function () {
            $.ajax({
                type: 'POST',
                url: '<?= base_url('modificar_item_del_pedido') ?>/' + $('#itm_id').val(),
                data: {
                    itm_quantity: $('#itm_quantity').val(),
                    itm_comments: $('#itm_comments').text()
                },
                success: function (response) {
                    if (!response['error']) {
                        location.reload();
                    }
                }
            });
        });
        $('#editAccountSave').click(function () {
            $.ajax({
                type: 'POST',
                url: '<?= site_url('editar_contacto/' . $customer->id) ?>',
                data: $('#editAccount :input').serialize(),
                success: function (response) {
                    if (!response['error']) {
                        location.reload();
                    }
                }
            });
        });
        $('#editAddressSave').click(function () {
            $.ajax({
                type: 'POST',
                url: '<?= site_url('editar_direccion/' . $customer->id) ?>',
                data: $('#editAddress :input').serialize(),
                success: function (response) {
                    if (!response['error']) {
                        location.reload();
                    }
                }
            });
        });
        $('.edit_item').click(function () {
            itm_id = $(this).attr('data-itm_id');
            $('#itm_id').attr('value', itm_id);
            $('#prod_name').html($('#prod_name_' + itm_id).text().toUpperCase());
            $("#itm_quantity option:selected").removeAttr("selected");
            $('#itm_quantity option:eq(' + ($('#itm_quantity_' + itm_id).text() - 1) + ')').attr('selected', 'selected');
            $('#itm_comments').html($('#itm_comments_' + itm_id).text());
            $('#editItem').modal({backdrop: true});
        });
        $('.remove_item').click(function () {
            itm_id = $(this).attr('data-itm_id');
            $('#itm_id_rmv').attr('value', itm_id);
            $('#prod_name_rmv').html($('#prod_name_' + itm_id).text().toUpperCase());
            $('#itm_quantity_rmv').html($('#itm_quantity_' + itm_id).text());
            $('#itm_comments_rmv').html('"' + $('#itm_comments_' + itm_id).text().replace(/\n/g, "<br />") + '"');
            $('#removeItem').modal({backdrop: true});
        });
    });
</script>