<section class="container">
    <div class="tituloPage">
        <h1>PRODUCTOS</h1>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-5 col-xs-12">
            <ul class="listaCategorias">
                <?php foreach ($categories as $category): ?>
                    <li class="<?= ($category === end($categories))?'noBorderBottom':'' ?>">
                        <h2><a href="<?= $category->cat_slug ?>"><?= $category->cat_name ?></a></h2>
                    </li>
                <?php endforeach; ?>
                <!--
                <li class="">
                    <h2><a href="nueva-temporada">Nueva temporada</a></h2>
                </li>
                <li class="">
                    <h2><a href="sweaters">Sweaters</a></h2>
                </li>
                <li class="">
                    <h2><a href="vestidos">Vestidos</a></h2>
                </li>
                <li class="">
                    <h2><a href="pantalones">Pantalones</a></h2>
                </li>
                <li class="noBorderBottom">
                    <h2><a href="rebajas">Rebajas</a></h2>
                </li>
                -->
            </ul>
        </div>
        <div class="col-lg-9 col-md-8 col-sm-7 col-xs-12">
            <img src="<?= base_url('assets/Resources/CategoriaInicio/1M.png') ?>" class="img-responsive" />
        </div>
    </div>
</section>

<section class="container">
    <div class="divSuscripcion">
        <?php if (isset($_GET['news']) && $_GET['news'] == 1) { ?>
            <div class="alert alert-success" style="max-width: 90%; margin: 0 10% 20px;">
                Gracias por suscribirte a nuestro newsletter.
            </div>
        <?php } ?>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right font-cheddar font-24">
            Suscríbite a nuestro correo electrónico!
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <form action="<?= base_url('add_newsletter'); ?>" method="post">
                <input type="text" placeholder="E-Mail" class="emailInput" name="news_email"/>
                <input type="submit" value="SUSCRIBIRME" class="btnSuscripcion"/>
            </form>
        </div>
    </div>
</section>