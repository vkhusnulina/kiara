<section class="container contenedorCatalogoProductos">
    <div class="tituloPage">
        <h1 >MI CARRITO</h1>
    </div>
    <img src="<?= base_url('assets/Resources/micarrito/01pedido.png') ?>" alt="Lista del pedido" class="img-responsive imagenCarritoCompras" />

    <?php $subtotal = 0; ?>
    <?php if (empty($items)): ?>
        <div class="alert alert-warning" role="alert" style="font-weight: normal; font-size: 20px;">A&uacute;n no tiene productos en el carrito de compras.</div>
    <?php else: ?>
        <table class="table table-bordered tableMiCarrito table-responsive">
            <thead>
                <tr>
                    <th class="col-lg-3 col-md-3 col-sm-3 col-xs-4 text-center">PRODUCTO</th>
                    <th class="col-lg-3 col-md-3 col-sm-3 hidden-xs text-center">DETALLE</th>
                    <th class="col-lg-1 col-md-1 col-sm-1 col-xs-4 text-center">CANTIDAD</th>
                    <th class="col-lg-2 col-md-2 col-sm-2 col-xs-4 text-center">PRECIO UNITARIO </th>
                    <th class="col-lg-1 col-md-1 col-sm-1 hidden-xs text-center">SUBTOTAL</th>
                    <th class="col-lg-1 col-md-1 col-sm-1 hidden-xs text-center">EDITAR</th>
                    <th class="col-lg-1 col-md-1 col-sm-1 hidden-xs text-center">ELIMINAR</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($items as $item): ?>
                    <?php $subtotal = $subtotal + ($item->itm_unit_price * $item->itm_quantity); ?>
                    <tr>
                        <td class="row text-left">
                            <div class="row-height">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-height hidden-xs">
                                    <div class="inside">
                                        <div class="content"><img src="<?= base_url('assets/admin/images/products/' . $item->image) ?>" class="img-responsive"/></div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-height col-middle">
                                    <div class="inside">
                                        <div class="content">
                                            <p class="nombreProductoMiCarrito" id="prod_name_<?= $item->itm_id ?>"><?= $item->prod_name ?></p>
                                            <p class="idProductoMiCarrito">Código #<?= $item->prod_code ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="hidden-xs text-left"><p id="itm_comments_<?= $item->itm_id ?>"><?= $item->itm_comments ?></p></td>
                        <td><p id="itm_quantity_<?= $item->itm_id ?>"><?= $item->itm_quantity ?></p></td>
                        <td><p>$<?= $item->itm_unit_price ?></p></td>
                        <td class="hidden-xs"><p>$<?= $item->itm_quantity * $item->itm_unit_price ?></p></td>
                        <td class="hidden-xs"><a data-itm_id="<?= $item->itm_id ?>" class="edit_item" href="javascript:void(0);"><img src="<?= base_url('assets/Resources/micarrito/modificar.png') ?>"></img></a></td>
                        <td class="hidden-xs"><a data-itm_id="<?= $item->itm_id ?>" class="remove_item" href="javascript:void(0);" style="color: #555;"><i class="glyphicon glyphicon-remove"></i></a></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
    <div>
        <a href="<?= base_url('productos') ?>" class="volverLink">Volver a productos > </a>
        <div class="finalizarPedidoMiCarrito">
            <span class="colorRed">Monto mínimo: $2000</span>
            <span class="totalMicarrito">Total: $<?= $subtotal ?></span>
            <?php if ($order): ?>
                <span>Número de pedido: <?= $order->ord_id ?></span>
            <?php endif; ?>
            <?php if ($subtotal >= 2000): ?>
                <br/>
                <a class="botonNegro" href="<?= base_url('envio_y_facturacion') ?>">FINALIZAR PEDIDO</a>
            <?php endif; ?>
        </div>
    </div>
</section>

<!-- Modal -->
<div id="editItem" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <span class="nombreProductoDetalle modal-title" id="prod_name"></span>
            </div>
            <div class="modal-body">
                <div class="cantidadProductoDetalle" style="margin-top:-5px;">
                    <span>CANTIDAD: </span>
                    <select id="itm_quantity">
                        <?php for ($i = 1; $i <= 50; $i++): ?>
                            <option value="<?= $i ?>"><?= $i ?></option>
                        <?php endfor; ?>
                    </select>
                </div>
                <div class="detalleProductoDetalle">
                    <h5>DETALLE DEL PEDIDO: </h5>
                    <span>No obligatorio. Especifique talle y color </span>

                    <textarea class="vert" id="itm_comments"></textarea>
                </div>
            </div>
            <input type="hidden" id="itm_id" value="" />
            <div class="modal-footer">
                <button type="button" class="botonRojo" id="editItemSave">MODIFICAR</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>
<div id="removeItem" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <span class="nombreProductoDetalle modal-title">&iquest;Seguro que quiere remover "<span id="prod_name_rmv"></span>" del carrito?</span>
            </div>
            <div class="modal-body">
                <div class="cantidadProductoDetalle" style="margin-top:-5px;">
                    <span>CANTIDAD: </span>
                    <span id="itm_quantity_rmv"></span>
                </div>
                <div class="detalleProductoDetalle">
                    <h5>DETALLE DEL PEDIDO: </h5>
                    <div id="itm_comments_rmv" style="font-weight: normal; font-size: 20px;"></div>
                </div>
            </div>
            <input type="hidden" id="itm_id_rmv" value="" />
            <div class="modal-footer">
                <button type="button" class="botonNegro" id="itemRemove">ELIMINAR</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        // textarea
        $("#itm_comments").change(function () {
            $(this).text($(this).val());
        });
        $('#itemRemove').click(function () {
            $.ajax({
                type: 'POST',
                url: '<?= base_url('remover_item_del_pedido') ?>/' + $('#itm_id_rmv').val(),
                data: {},
                success: function (response) {
                    if (!response['error']) {
                        location.reload();
                    }
                }
            });
        });
        $('#editItemSave').click(function () {
            $.ajax({
                type: 'POST',
                url: '<?= base_url('modificar_item_del_pedido') ?>/' + $('#itm_id').val(),
                data: {
                    itm_quantity: $('#itm_quantity').val(),
                    itm_comments: $('#itm_comments').text()
                },
                success: function (response) {
                    if (!response['error']) {
                        location.reload();
                    }
                }
            });
        });
        $('.edit_item').click(function () {
            itm_id = $(this).attr('data-itm_id');
            $('#itm_id').attr('value', itm_id);
            $('#prod_name').html($('#prod_name_' + itm_id).text().toUpperCase());
            $("#itm_quantity option:selected").removeAttr("selected");
            $('#itm_quantity option:eq(' + ($('#itm_quantity_' + itm_id).text() - 1) + ')').attr('selected', 'selected');
            $('#itm_comments').html($('#itm_comments_' + itm_id).text());
            $('#editItem').modal({backdrop: true});
        });
        $('.remove_item').click(function () {
            itm_id = $(this).attr('data-itm_id');
            $('#itm_id_rmv').attr('value', itm_id);
            $('#prod_name_rmv').html($('#prod_name_' + itm_id).text().toUpperCase());
            $('#itm_quantity_rmv').html($('#itm_quantity_' + itm_id).text());
            $('#itm_comments_rmv').html('"' + $('#itm_comments_' + itm_id).text().replace(/\n/g, "<br />") + '"');
            $('#removeItem').modal({backdrop: true});
        });
    });
</script>