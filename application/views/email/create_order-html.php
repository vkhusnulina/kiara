<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>Nuevo pedido creado en <?php echo $site_name; ?></title></head>
<body>
<div style="max-width: 800px; margin: 0; padding: 30px 0;">
<table width="80%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="5%"></td>
<td align="left" width="95%" style="font: 13px/18px Arial, Helvetica, sans-serif;">
    <h1><img src="<?= base_url('assets/Resources/logo.png') ?>" alt="Kiara" /></h1>
<br />
<h2 style="font: normal 20px/23px Arial, Helvetica, sans-serif; margin: 0; padding: 0 0 18px; color: black;">Nuevo pedido creado en <?php echo $site_name; ?></h2>
El pedido fue dado de alta exitosamente el d&iacute;a <?= date('d/m/Y') ?> a las <?= date('H:i') ?>hs. Para las pr&oacute;ximas referencias, el n&uacute;mero de pedido es #<?= $order_id ?><br />
Los productos del pedido son:
<br />
<ul>
<?php foreach ($item_list as $item) {
     echo '<li>'.$item.'</li>';
} ?>
</ul>
Total del pedido: $<?= $total ?>
<br />
<br />
Atte. <br />
el quipo de  <?php echo $site_name; ?>
</td>
</tr>
</table>
</div>
</body>
</html>