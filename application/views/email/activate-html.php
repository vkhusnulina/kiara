<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>Bienvenido a Kiara!</title></head>
<body>
<div style="max-width: 800px; margin: 0; padding: 30px 0;">
<table width="80%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="5%"></td>
<td align="left" width="95%" style="font: 13px/18px Arial, Helvetica, sans-serif;">
<h1><img src="<?= base_url('assets/Resources/logo.png') ?>" alt="Kiara" /></h1>
<br />
<h2 style="font: normal 20px/23px Arial, Helvetica, sans-serif; margin: 0; padding: 0 0 18px; color: black;">Bienvenido a Kiara!</h2>
Gracias por registrarte en nuestra web.<br />
Para verificar su dirección de correo electrónico, por favor, haga click aqui y complete sus datos:<br />
<br />
<big style="font: 16px/18px Arial, Helvetica, sans-serif;"><b><a href="<?php echo site_url('/auth/activate/'.$user_id.'/'.$new_email_key); ?>" style="color: #3366cc;">Termine su registro ...</a></b></big><br />
<br />
El enlace no funciona? Copie el siguiente enlace a la barra de direcciones del navegador:<br />
<nobr><a href="<?php echo site_url('/auth/activate/'.$user_id.'/'.$new_email_key); ?>" style="color: #3366cc;"><?php echo site_url('/auth/activate/'.$user_id.'/'.$new_email_key); ?></a></nobr><br />
<br />
Verifique su correo electrónico en <?php echo $activation_period; ?> horas, de lo contrario su registro se convertirá en nulo y que tendrá que registrarse nuevamente.<br />
<br />
<br />
<?php if (strlen($username) > 0) { ?>Your username: <?php echo $username; ?><br /><?php } ?>
Su dirección de correo electrónico: <?php echo $email; ?><br />
<?php if (isset($password)) { /* ?>Your password: <?php echo $password; ?><br /><?php */ } ?>
</td>
</tr>
</table>
</div>
</body>
</html>