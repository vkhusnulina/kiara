<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>Modificaciones en el pedido de <?php echo $site_name; ?></title></head>
<body>
<div style="max-width: 800px; margin: 0; padding: 30px 0;">
<table width="80%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="5%"></td>
<td align="left" width="95%" style="font: 13px/18px Arial, Helvetica, sans-serif;">
    <h1><img src="<?= base_url('assets/Resources/logo.png') ?>" alt="Kiara" /></h1>
<br />
<h2 style="font: normal 20px/23px Arial, Helvetica, sans-serif; margin: 0; padding: 0 0 18px; color: black;">Modificaciones en el pedido de <?php echo $site_name; ?></h2>
El d&iacute;a <?= date('d/m/Y') ?> a las <?= date('H:i') ?>hs, se realizaron las siguientes modificaciones en su pedido n&uacute;mero <?= $order_id ?>:<br />
<br />
<ul>
<?php foreach ($history as $item) {
     echo '<li>'.$item.'</li>';
} ?>
</ul>
<br />
<br />
Atte. <br />
el quipo de  <?php echo $site_name; ?>
</td>
</tr>
</table>
</div>
</body>
</html>