<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>&iexcl;Bienvenido a <?php echo $site_name; ?>!</title></head>
<body>
<div style="max-width: 800px; margin: 0; padding: 30px 0;">
<table width="80%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="5%"></td>
<td align="left" width="95%" style="font: 13px/18px Arial, Helvetica, sans-serif;">
    <h1><img src="<?= base_url('assets/Resources/logo.png') ?>" alt="Kiara" /></h1>
<br />
<h2 style="font: normal 20px/23px Arial, Helvetica, sans-serif; margin: 0; padding: 0 0 18px; color: black;">&iexcl;Bienvenido a <?php echo $site_name; ?>!</h2>
Gracias por formar parte de <?php echo $site_name; ?>. A continuaci&oacute;n le pasamos sus datos de LogIn, asegurese de que est&eacute;n bien.<br />
Para verificar su correo electr&oacute;nico siga este enlace:<br />
<br />
<big style="font: 16px/18px Arial, Helvetica, sans-serif;"><b><a href="<?php echo site_url('/auth/activate/'.$user_id.'/'.$new_email_key); ?>" style="color: #3366cc;">Finalizar el registro...</a></b></big><br />
<br />
&iquest;El enlace no funciona? Copie el siguiente enlace en la barra de direcci&oacute;n de su navegador:<br />
<nobr><a href="<?php echo site_url('/auth/activate/'.$user_id.'/'.$new_email_key); ?>" style="color: #3366cc;"><?php echo site_url('/auth/activate/'.$user_id.'/'.$new_email_key); ?></a></nobr><br />
<br />
Por favor, verifique su email en el plazo de <?php echo $activation_period; ?> horas, de lo contrario el registro ser&aacute; invalidado y tendr&aacute; que registrarse de vuelta.<br />
<br />
<br />
<?php if(strlen($username)>0) { ?>Su nombre de usuario: <?php echo $username; ?><br /><?php } ?>
Su correo electr&oacute;nico: <?php echo $email; ?><br />
<?php if (isset($password)) { ?>Su contrase&ntilde;a: <?php echo $password; ?><br /><?php } ?>
<br />
<br />
Atte. <br />
el quipo de  <?php echo $site_name; ?>
</td>
</tr>
</table>
</div>
</body>
</html>