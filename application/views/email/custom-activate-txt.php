Bienvenido a <?php echo $site_name; ?>,

Gracias por formar parte de <?php echo $site_name; ?>. A continuación le pasamos sus datos de LogIn, asegurese de que están bien. 
Para verificar su correo electrónico siga este enlace:

<?php echo site_url('/auth/activate/' . $user_id . '/' . $new_email_key); ?>


Por favor, verifique su email en el plazo de <?php echo $activation_period; ?> horas, de lo contrario el registro será invalidado y tendrá que registrarse de vuelta.
<?php if (strlen($username) > 0) { ?>

    Su usuario: <?php echo $username; ?>
<?php } ?>

Su correo electrónico: <?php echo $email; ?>
<?php if (isset($password)) { ?>

    Su contraseña: <?php echo $password; ?>
<?php } ?>



Atte.
el quipo de  <?php echo $site_name; ?>