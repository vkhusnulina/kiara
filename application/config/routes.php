<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There area two reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router what URI segments to use if those provided
  | in the URL cannot be matched to a valid route.
  |
 */


/* * *****************
 * ****** CATALOGUE
 */

$route['admin'] = 'orders/admin_list';

/* products */
$route['admin/products'] = 'products/admin_list';
$route['admin/products/new'] = 'products/admin_new';
$route['admin/products/insert'] = 'products/admin_insert';
$route['admin/products/view/(:num)'] = 'products/admin_view/$1';
$route['admin/products/update/(:num)'] = 'products/admin_update/$1';
$route['admin/products/delete_image/(:num)'] = 'products/admin_delete_image/$1';
$route['admin/products/change_details/(:num)'] = 'products/admin_change_details/$1';

/* categories */
$route['admin/categories'] = 'categories/admin_list';
$route['admin/categories/new'] = 'categories/admin_new';
$route['admin/categories/insert'] = 'categories/admin_insert';
$route['admin/categories/view/(:num)'] = 'categories/admin_view/$1';
$route['admin/categories/update/(:num)'] = 'categories/admin_update/$1';

/* product attributes */

/* colors */
$route['admin/product_attributes/colors'] = 'colors/admin_list';
$route['admin/product_attributes/colors/new'] = 'colors/admin_new';
$route['admin/product_attributes/colors/insert'] = 'colors/admin_insert';
$route['admin/product_attributes/colors/view/(:num)'] = 'colors/admin_view/$1';
$route['admin/product_attributes/colors/update/(:num)'] = 'colors/admin_update/$1';

/* sizes */
$route['admin/product_attributes/sizes'] = 'sizes/admin_list';
$route['admin/product_attributes/sizes/new'] = 'sizes/admin_new';
$route['admin/product_attributes/sizes/insert'] = 'sizes/admin_insert';
$route['admin/product_attributes/sizes/view/(:num)'] = 'sizes/admin_view/$1';
$route['admin/product_attributes/sizes/update/(:num)'] = 'sizes/admin_update/$1';

/* shoes sizes */
$route['admin/product_attributes/shoes_sizes'] = 'shoes_sizes/admin_list';
$route['admin/product_attributes/shoes_sizes/new'] = 'shoes_sizes/admin_new';
$route['admin/product_attributes/shoes_sizes/insert'] = 'shoes_sizes/admin_insert';
$route['admin/product_attributes/shoes_sizes/view/(:num)'] = 'shoes_sizes/admin_view/$1';
$route['admin/product_attributes/shoes_sizes/update/(:num)'] = 'shoes_sizes/admin_update/$1';

/* * *****************
 * ****** CUSTOMERS
 */

/* users */

$route['admin/customers'] = 'customers/admin_list';
$route['admin/customers/new'] = 'customers/admin_new';
$route['admin/customers/insert'] = 'customers/admin_insert';
$route['admin/customers/view/(:num)'] = 'customers/admin_view/$1';
$route['admin/customers/update/(:num)'] = 'customers/admin_update/$1';


/* * *****************
 * ****** ORDERS
 */

/* orders */

$route['admin/orders'] = 'orders/admin_list';
$route['admin/orders/insert'] = 'orders/admin_insert';
$route['admin/orders/view/(:num)'] = 'orders/admin_view/$1';
$route['admin/orders/update/(:num)'] = 'orders/admin_update/$1';
$route['admin/orders/update_item'] = 'orders/admin_update_all_items';
$route['admin/orders/update_item/(:num)'] = 'orders/admin_update_item/$1';


/* * *****************
 * ****** SLIDESHOW
 */

$route['admin/slideshow'] = 'slideshow/manage';
$route['admin/slideshow/upload'] = 'slideshow/upload';


/* * *****************
 * ****** NEWSLETTERS
 */

$route['admin/newsletter'] = 'newsletters/admin_list';


/* * *****************
 * ****** CONFIGURATION
 */

$route['admin/configuration-emails'] = 'configuration/email_list';
$route['admin/configuration-emails/view/(:num)'] = 'configuration/email_view/$1';
$route['admin/configuration-emails/save/(:num)'] = 'configuration/email_save/$1';


/* * *****************
 * ****** FRONT
 */
$route['default_controller'] = "front/home";

$route['add_newsletter'] = "newsletters/add";

// AGREGADOS POR GASTI (PARA LAS SECCIONES ESTATICAS)
$route['contacto'] = "front/contact";
$route['contacto/enviar'] = "front/contact_submit";
$route['donde-encontrarnos'] = "front/where_we_are";
$route['guia-de-talles'] = "front/size_guide";
$route['quienes-somos'] = "front/who_are_we";
$route['como-comprar'] = "front/how_buy";
$route['mi_cuenta'] = "front/my_account";
$route['editar_contacto/(:num)'] = "front/update_account/$1";
$route['editar_direccion/(:num)'] = "front/update_address/$1";

$route['productos'] = "front/categories";
$route['agregar_al_carrito'] = "front/add_to_order";

$route['mi_carrito'] = "front/actual_order";
$route['envio_y_facturacion'] = "front/shipping_invoices";
$route['guardar_envio_y_facturacion/(:num)'] = "orders/update/$1";
$route['metodo_de_envio_y_pago'] = "front/shipping_payment";
$route['guardar_metodo_de_envio_y_pago/(:num)'] = "orders/update/$1";
$route['finalizar/(:num)'] = "front/finish/$1";

$route['modificar_item_del_pedido/(:num)'] = "front/update_item/$1";
$route['remover_item_del_pedido/(:num)'] = "front/delete_item/$1";

$route['404_override'] = "";

$route['((?!auth).*?)/(.*)'] = "front/product/$1/$2";
$route['((?!auth).*$)'] = "front/category/$1";

/* End of file routes.php */
/* Location: ./application/config/routes.php */