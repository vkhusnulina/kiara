<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Images {
    /*     * *
     * Recibe $_FILES['image']
     * y tamanio maximo en KB
     */

    function control($image, $max_size_kb) {
        $uploadedfileload = true;
        $uploadedfile_size = $image['size'];
        $uploadedfile_type = $image['type'];
        $msg = "";
        //Control de tamanio
        if ($uploadedfile_size > (1000 * $max_size_kb)) {
            $msg = $msg . "El archivo es mayor a " . $max_size_kb . "KB, debe reducirlo antes de subirlo.";
            $uploadedfileload = false;
        }
        //Control formato
        if ($uploadedfile_type !== 'image/jpeg' && $uploadedfile_type !== 'image/png' && $uploadedfile_type !== 'image/gif') {
            $msg = $msg . " El archivo puede ser JPG, PNG o GIF. Otros archivos no son permitidos.";
            $uploadedfileload = false;
        }
        return array('result' => $uploadedfileload, 'message' => $msg);
    }

    /*     * *
     * Recibe $_FILES['image']
     * nuevo nombre del archivo
     * y el directorio donde se guardara
     */

    function upload($image, $new_name, $directory) {
        try {
            $path = $image['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            //Sube archivo y designa nombre
            $server_path = 'assets/admin/images';
            //$server_path = str_replace("/", DIRECTORY_SEPARATOR, $_SERVER["DOCUMENT_ROOT"]);
            $add = join(DIRECTORY_SEPARATOR, array($server_path,
                'kiara',
                'assets',
                'admin',
                'images',
                $directory,
                $new_name . '.' . $ext));
            //if (move_uploaded_file($image['tmp_name'], $add)) {
            if (move_uploaded_file($image['tmp_name'], "$server_path/$directory/$new_name.$ext")) {
                return $new_name . '.' . $ext;
            } else {
                return FALSE;
            }
        } catch (Exception $ex) {
            return FALSE;
        }
    }

}

/* End of file images.php */
/* Location: ./application/libraries/images.php */

