<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Catalog {

    function __construct() {
        $this->ci = & get_instance();

        $this->ci->load->library('Tank_auth', 'tank_auth');
        $this->ci->load->model('products_model', 'products');
        $this->ci->load->model('categories_model', 'categories');
        $this->ci->load->model('colors_model', 'colors');
        $this->ci->load->model('sizes_model', 'sizes');
        $this->ci->load->model('tank_auth/users', 'users');
        $this->ci->load->model('customers_model', 'customers');
        $this->ci->load->model('orders_model', 'orders');
        $this->ci->load->model('media_model', 'media');
    }

    /*     * *
     * SLIDESHOW
     */

    function slideshow_all() {
        return $this->ci->media->get_by_visible(3);
    }

    /*     * *
     * CATEGORIES
     */

    function categories_by_id($cat_id) {
        return $this->ci->categories->get_by_id($cat_id);
    }

    function categories_by_slug($cat_slug) {
        return $this->ci->categories->get_by_slug($cat_slug);
    }

    function categories_all($visibleOnly = False) {
        return $this->ci->categories->get_all($visibleOnly);
    }

    function categories_products_by_id($cat_id, $visible = 0) {
        return $this->ci->categories->get_products_by_id($cat_id, $visible);
    }

    function categories_new($data) {
        $validation = $this->name_validation('cat_name', 'Nombre de la categoría', 'El nombre de la categoría es obligatorio.');
        if ($validation['result']) {
            $data['cat_name'] = $validation['message'];
            $data['cat_slug'] = url_title($data['cat_name'], 'dash', true);
            $new_category = $this->ci->categories->insert($data);
            if ($new_category) {
                return ['error' => FALSE, 'message' => $new_category];
            } else {
                return ['error' => TRUE, 'message' => 'No se pudo ingresar correctamente la categoría.'];
            }
        } else {
            return ['error' => TRUE, 'message' => $validation['message']];
        }
        die();
    }

    function categories_update($cat_id, $data) {
        $validation = $this->name_validation('cat_name', 'Nombre de la categoría', 'El nombre de la categoría es obligatorio.');
        if ($validation['result']) {
            $data['cat_name'] = $validation['message'];
            $data['cat_slug'] = url_title($data['cat_name'], 'dash', true);
            if ($this->ci->categories->update($cat_id, $data)) {
                return ['error' => FALSE, 'message' => 'Categoría modificada con éxito.'];
            } else {
                return ['error' => TRUE, 'message' => 'No se pudo modificar correctamente la categoría.'];
            }
        } else {
            return ['error' => TRUE, 'message' => $validation['message']];
        }
        die();
    }

    /*     * *
     * PRODUCTS
     */

    function products_all() {
        return $this->ci->products->get_all();
    }

    function products_by_id($prod_id) {
        return $this->ci->products->get_by_id($prod_id);
    }

    function products_categories_by_id($prod_id) {
        return $this->ci->products->get_categories($prod_id);
    }

    function products_categories_update($prod_id, $categories) {
        $categories_added = $this->ci->products->get_categories($prod_id);
        $ids = array();
        foreach (($categories_added ? $categories_added : array()) as $category_added) {
            $ids[] = $category_added->categories_cat_id;
        }
        return $this->ci->products->update_categories($prod_id, $categories, $ids);
    }

    function products_colors_update($prod_id, $colors) {
        $colors_added = $this->ci->products->get_colors($prod_id);
        $ids = array();
        foreach (($colors_added ? $colors_added : array()) as $color_added) {
            $ids[] = $color_added->colors_clr_id;
        }
        return $this->ci->products->update_colors($prod_id, $colors, $ids);
    }

    function products_sizes_update($prod_id, $sizes) {
        $sizes_added = $this->ci->products->get_sizes($prod_id);
        $ids = array();
        foreach (($sizes_added ? $sizes_added : array()) as $size_added) {
            $ids[] = $size_added->sizes_siz_id;
        }
        return $this->ci->products->update_sizes($prod_id, $sizes, $ids);
    }

    function products_colors_by_id($prod_id) {
        return $this->ci->products->get_colors($prod_id);
    }

    function products_sizes_by_id($prod_id) {
        return $this->ci->products->get_sizes($prod_id);
    }

    function products_images_by_id($prod_id) {
        return $this->ci->products->get_images($prod_id);
    }

    function product_image($prod_id) {
        $media = $this->ci->products->get_images($prod_id);
        if (empty($media)) {
            return 'default.gif';
        } else {
            return $media[0]->media_name;
        }
    }

    function products_media_delete($media_id) {
        return $this->ci->media->delete($media_id);
    }

    function products_new($data) {
        $validation = $this->name_validation('prod_name', 'Nombre del producto', 'El nombre del producto es obligatorio.');
        if ($validation['result']) {
            $data['prod_name'] = $validation['message'];
            $data['prod_slug'] = url_title($data['prod_name'], 'dash', true);
            $new_product = $this->ci->products->insert($data);
            if ($new_product) {
                return ['error' => FALSE, 'message' => $new_product];
            } else {
                return ['error' => TRUE, 'message' => 'No se pudo ingresar correctamente el producto.'];
            }
        } else {
            return ['error' => TRUE, 'message' => $validation['message']];
        }

        die();
    }

    function media_new($data, $visible = 1) {
        $save['media_name'] = $data;
        $save['media_visible'] = $visible;
        $new_media = $this->ci->media->insert($save);
        if ($new_media) {
            return ['error' => FALSE, 'message' => $new_media];
        } else {
            return ['error' => TRUE, 'message' => 'No se pudo gurdar en la base la imagen.'];
        }
    }

    function products_media_save($prod_id, $media_id) {
        $new_media = $this->ci->products->insert_media($prod_id, $media_id);
        if ($new_media) {
            return ['error' => FALSE, 'message' => $new_media];
        } else {
            return ['error' => TRUE, 'message' => 'No se pudo relacionar la imagen con el producto.'];
        }
    }

    function products_update($prod_id, $data) {
        $validation = $this->name_validation('prod_name', 'Nombre del producto', 'El nombre del producto es obligatorio.');
        if ($validation['result']) {
            $data['prod_name'] = $validation['message'];
            $data['prod_slug'] = url_title($data['prod_name'], 'dash', true);
            if ($this->ci->products->update($prod_id, $data)) {
                return ['error' => FALSE, 'message' => 'Producto modificad con éxito.'];
            } else {
                return ['error' => TRUE, 'message' => 'No se pudo modificar correctamente el producto.'];
            }
        } else {
            return ['error' => TRUE, 'message' => $validation['message']];
        }
        die();
    }

    /*     * *
     * COLORS (PRODUCT ATTRIBUTES)
     */

    function colors_all() {
        return $this->ci->colors->get_all();
    }

    function colors_by_id($clr_id) {
        return $this->ci->colors->get_by_id($clr_id);
    }

    function colors_new($data) {
        $validation = $this->name_validation('clr_value', 'Nombre de la textura', 'El nombre de la textura es obligatorio.');
        //Agregar validacion de imagen
        if ($validation['result']) {
            $data['clr_value'] = $validation['message'];
            $new_color = $this->ci->colors->insert($data);
            if ($new_color) {
                return ['error' => FALSE, 'message' => $new_color];
            } else {
                return ['error' => TRUE, 'message' => 'No se pudo ingresar correctamente el color.'];
            }
        } else {
            return ['error' => TRUE, 'message' => $validation['message']];
        }

        die();
    }

    function colors_update($clr_id, $data) {
        $validation = $this->name_validation('clr_value', 'Nombre de la textura', 'El nombre de la textura es obligatorio.');
        if ($validation['result']) {
            $data['clr_value'] = $validation['message'];
            //$data['prod_slug'] = url_title($data['prod_name'], 'dash', true);
            if ($this->ci->colors->update($clr_id, $data)) {
                return ['error' => FALSE, 'message' => 'Textura modificada con éxito.'];
            } else {
                return ['error' => TRUE, 'message' => 'No se pudo modificar correctamente la textura.'];
            }
        } else {
            return ['error' => TRUE, 'message' => $validation['message']];
        }
        die();
    }

    /*     * *
     * SIZES (PRODUCT ATTRIBUTES)
     */

    function sizes_all($is_shoes = FALSE) {
        if ($is_shoes) {
            return $this->ci->sizes->get_all_shoes_sizes();
        } else {
            return $this->ci->sizes->get_all_sizes();
        }
    }

    function sizes_by_id($siz_id) {
        return $this->ci->sizes->get_by_id($siz_id);
    }

    function sizes_new($data) {
        $validation = $this->name_validation('siz_value', 'Talle', 'El valor del talle es obligatorio.');
        if ($validation['result']) {
            $data['siz_value'] = $validation['message'];
            $new_size = $this->ci->sizes->insert($data);
            if ($new_size) {
                return ['error' => FALSE, 'message' => $new_size];
            } else {
                return ['error' => TRUE, 'message' => 'No se pudo ingresar correctamente el talle.'];
            }
        } else {
            return ['error' => TRUE, 'message' => $validation['message']];
        }

        die();
    }

    function sizes_update($siz_id, $data) {
        $validation = $this->name_validation('siz_value', 'Talle', 'El valor del talle es obligatorio.');
        if ($validation['result']) {
            $data['siz_value'] = $validation['message'];
            if ($this->ci->sizes->update($siz_id, $data)) {
                return ['error' => FALSE, 'message' => 'Talle modificado con éxito.'];
            } else {
                return ['error' => TRUE, 'message' => 'No se pudo modificar correctamente el talle.'];
            }
        } else {
            return ['error' => TRUE, 'message' => $validation['message']];
        }
        die();
    }

    /*     * *
     * CUSTOMERS
     */

    function customers_all() {
        return $this->ci->users->get_users_by_role(3);
    }

    function customer_by_user_id($user_id) {
        return $this->ci->customers->get_by_user_id($user_id);
    }

    function customers_update($user_id, $data_profile, $data_address) {
        if ($data_profile) {
            if ($data_profile['prf_birth']) {
                $data_profile['prf_birth'] = date('Y-m-d', strtotime($data_profile['prf_birth']));
            }
        } else {
            $data_profile = [];
        }

        if ($data_address) {
            $address_id = $this->ci->customers->get_address_id_by_user_id($user_id);
            if ($address_id) {
                if (!$this->ci->customers->update_address($address_id, $data_address)) {
                    return ['error' => TRUE, 'message' => 'Problemas al actualizar domicilio del cliente!'];
                }
            } else {
                $address_id = $this->ci->customers->insert_address($data_address);
                if (!$address_id) {
                    return ['error' => TRUE, 'message' => 'Problemas al insertar domicilio del cliente!'];
                }
            }
            $data_profile['prf_address_id'] = $address_id;
        }

        if ($this->ci->customers->update_profile($user_id, $data_profile)) {
            return ['error' => FALSE, 'message' => 'Datos actualizados satisfactoriamente!'];
        } else {
            return ['error' => TRUE, 'message' => 'Problemas al actualizar datos del cliente!'];
        }
    }

    function customers_new($data, $data_profile, $data_address) {
        $validationEmail = $this->name_validation('email', 'Email', 'El email es obligatorio.');
        $validationName = $this->name_validation('prf_name', 'Nombre', 'El nombre es obligatorio.');
        $validationSurname = $this->name_validation('prf_surname', 'Apellido', 'El apellido es obligatorio.');
        if ($validationEmail['result'] && $validationName['result'] && $validationSurname['result']) {
            $data['email'] = $validationEmail['message'];
            $data_profile['prf_name'] = $validationName['message'];
            $data_profile['prf_surname'] = $validationSurname['message'];
            if ($data_profile['prf_birth']) {
                $data_profile['prf_birth'] = date('Y-m-d', strtotime($data_profile['prf_birth']));
            }
            //generate 8 char password and store it
            $data['password'] = substr(uniqid(), 2, 8);

            //Alta de usuario mediante TANK_AUTH
            $user = $this->ci->tank_auth->create_user('', $data['email'], $data['password'], TRUE);
            if (!is_null($user)) {
                $user['activation_period'] = $this->ci->config->item('email_activation_expire', 'tank_auth') / 3600;
                $address_id = $this->ci->customers->insert_address($data_address);
                if ($address_id) {
                    //return ['error' => TRUE, 'message' => 'Problemas al agregar domicilio del cliente!'];
                    $data_profile['prf_address_id'] = $address_id;
                }
                $data_profile['prf_user_id'] = $user['user_id'];
                if ($this->ci->customers->insert_profile($data_profile)) {
                    //Envío mail de activaión de cuenta + nueva password
                    $this->_send_email($user['email'], $user, sprintf($this->ci->lang->line('auth_subject_activate'), $this->ci->config->item('website_name', 'tank_auth')), 'custom-activate');
                    return ['error' => FALSE, 'message' => 'Cliente creado satisfactoriamente!'];
                } else {
                    $this->ci->users->delete_user($user['user_id']);
                    return ['error' => TRUE, 'message' => 'Problemas al agregar un nuevo cliente!'];
                }
            } else {
                return ['error' => TRUE, 'message' => 'Problemas al agregar un nuevo usuario!'];
            }
        } else {
            if (!$validationEmail['result']) {
                return ['error' => TRUE, 'message' => $validationEmail['message']];
            }
            if (!$validationName['result']) {
                return ['error' => TRUE, 'message' => $validationName['message']];
            }
            if (!$validationSurname['result']) {
                return ['error' => TRUE, 'message' => $validationSurname['message']];
            }
        }
        die();
    }

    function insert_user_profile_address($user_id, $data_profile, $data_address) {
        if ($data_profile['prf_birth']) {
            $data_profile['prf_birth'] = date('Y-m-d', strtotime($data_profile['prf_birth']));
        }
        $address_id = $this->ci->customers->insert_address($data_address);
        if ($address_id) {
            //return ['error' => TRUE, 'message' => 'Problemas al agregar domicilio del cliente!'];
            $data_profile['prf_address_id'] = $address_id;
        }
        $data_profile['prf_user_id'] = $user_id;
        if ($this->ci->customers->insert_profile($data_profile)) {
            return ['error' => FALSE, 'message' => ''];
        } else {
            $this->ci->users->delete_user($user_id);
            return ['error' => TRUE, 'message' => ''];
        }
    }

    /*     * *
     * ORDERS
     */

    function orders_all() {
        return $this->ci->orders->get_all();
    }

    function orders_by_id($ord_id) {
        return $this->ci->orders->get_by_id($ord_id);
    }

    function orders_by_user_id($user_id) {
        return $this->ci->orders->get_by_user_id($user_id);
    }

    function order_history_by_order($ord_id) {
        return $this->ci->orders->get_history_by_order_id($ord_id);
    }

    function order_items_by_order($ord_id) {
        return $this->ci->orders->get_items_by_id($ord_id);
    }

    function order_by_item_id($itm_id) {
        return $this->ci->orders->get_order_id_by_item_id($itm_id);
    }

    function order_item_by_id($itm_id) {
        return $this->ci->orders->get_item_by_id($itm_id);
    }

    function orders_update($ord_id, $data_order, $data_address, $data_invoice, $status_data, $data_changes) {
        $address_id = $this->ci->orders->get_address_id_by_order_id($ord_id);
        if ($address_id) {
            if (!$this->ci->orders->update_address($address_id, $data_address)) {
                return ['error' => TRUE, 'message' => 'Problemas al actualizar dirección de envío!'];
            }
        } else {
            $address_id = $this->ci->orders->insert_address($data_address);
            if (!$address_id) {
                return ['error' => TRUE, 'message' => 'Problemas al insertar dirección de envío!'];
            }
        }
        $invoice_id = $this->ci->orders->get_invoice_id_by_order_id($ord_id);
        if ($invoice_id) {
            if (!$this->ci->orders->update_invoice($invoice_id, $data_invoice)) {
                return ['error' => TRUE, 'message' => 'Problemas al actualizar datos de facturación!'];
            }
        } else {
            $data_invoice['inv_order_id'] = $ord_id;
            $invoice_id = $this->ci->orders->insert_invoice($data_invoice);
            if (!$invoice_id) {
                return ['error' => TRUE, 'message' => 'Problemas al insertar datos de facturación!'];
            }
        }

        $data_order['ord_address_id'] = $address_id;
        if ($data_order['ord_transport']) {
            $data_order['ord_transport'] = ($data_order['ord_transport'] ? 1 : 0);
        }
        // Captura si algun estado ha cambiado
        $history_text = array();
        foreach ($status_data as $key => $value) {
            if ($value != $data_order['ord_' . $key . '_status']) {
                //Actualizo fecha de modificacion
                $data_order['ord_' . $key . '_status_date'] = date('Y-m-d H:i:s');

                //Guardo registro para el historial
                $status = $this->ci->orders->status($key);
                $history_text[] = "Cambio de estado " . $status[0] . " de " . $status[1][$value][0] . ' a ' . $status[1][$data_order['ord_' . $key . '_status']][0];
            }
        }

        // Si el pago y el envio estan completados
        // a menos que el pedido este anulado, su estado cambia a "TERMINADO - 3"
        if ($data_order['ord_payment_status'] == 2 && $data_order['ord_shipping_status'] == 2 && $data_order['ord_general_status'] != 4) {
            //Guardo registro para el historial
            $status = $this->ci->orders->status('general');
            $history_text[] = "Cambio automático de estado General de " . $status[1][$data_order['ord_general_status']][0] . " a " . $status[1][3][0];

            $data_order['ord_general_status'] = 3;
            $data_order['ord_general_status_date'] = date('Y-m-d H:i:s');
        }

        //Controlo si hubieron cambios en el formulario de direccion
        if ($data_changes['address_change'] == '1') {
            $history_text[] = "Actualización de dirección de envío.";
        }
        //Controlo si hubieron cambios en el formulario de facturación
        if ($data_changes['invoice_change'] == '1') {
            $history_text[] = "Actualización en los datos de facturación.";
        }
        //Controlo si hubieron cambios en el formulario del pedido
        if ($data_changes['ship_to_change'] == '1') {
            $history_text[] = "Actualizado. Enviar a " . (($data_order['ord_ship_to'] == '1') ? 'Terminal' : 'Domicilio');
        }
        if ($data_changes['shipping_change'] == '1') {
            if ($data_order['ord_transport'] == '0') {
                $history_text[] = "Actualizado. Sin transporte.";
                $data_order['ord_shipping_company'] = '';
            } else {
                $history_text[] = "Actualizado. Con transporte (" . $data_order['ord_shipping_company'] . ")";
            }
        }
        if ($data_changes['comment_change'] == '1') {
            $history_text[] = "Actualización en el comentario general del pedido.";
        }

        if ($this->ci->orders->update($ord_id, $data_order)) {
            foreach ($history_text as $value) {
                $this->insert_history($ord_id, $value);
            }
            //Send email
            $email = $this->ci->orders->get_user_email_by_order_id($ord_id);
            $email_data = array();
            $email_data['order_id'] = $ord_id;
            $email_data['history'] = $history_text;
            $email_data['site_name'] = $this->ci->config->item('website_name', 'tank_auth');
            $this->_send_email($email, $email_data, 'Cambios en el pedido de ' . $email_data['site_name'], 'change_order');
            return ['error' => FALSE, 'message' => 'Datos actualizados satisfactoriamente!'];
        } else {
            return ['error' => TRUE, 'message' => 'Problemas al actualizar datos del pedido!'];
        }
    }

    function orders_update_without_comments($ord_id, $data_order = array(), $data_address = NULL, $data_invoice = NULL) {
        if ($data_address) {
            $address_id = $this->ci->orders->get_address_id_by_order_id($ord_id);
            if ($address_id) {
                if (!$this->ci->orders->update_address($address_id, $data_address)) {
                    return ['error' => TRUE, 'message' => 'Problemas al actualizar dirección de envío!'];
                }
            } else {
                $address_id = $this->ci->orders->insert_address($data_address);
                if (!$address_id) {
                    return ['error' => TRUE, 'message' => 'Problemas al insertar dirección de envío!'];
                }
            }
            $data_order['ord_address_id'] = $address_id;
        }
        if ($data_invoice) {
            $invoice_id = $this->ci->orders->get_invoice_id_by_order_id($ord_id);
            if ($invoice_id) {
                if (!$this->ci->orders->update_invoice($invoice_id, $data_invoice)) {
                    return ['error' => TRUE, 'message' => 'Problemas al actualizar datos de facturación!'];
                }
            } else {
                $data_invoice['inv_order_id'] = $ord_id;
                $invoice_id = $this->ci->orders->insert_invoice($data_invoice);
                if (!$invoice_id) {
                    return ['error' => TRUE, 'message' => 'Problemas al insertar datos de facturación!'];
                }
            }
        }
        if ($this->ci->orders->update($ord_id, $data_order)) {
            return ['error' => FALSE, 'message' => 'Datos actualizados satisfactoriamente!'];
        } else {
            return ['error' => TRUE, 'message' => 'Problemas al actualizar datos del pedido!'];
        }
    }

    function unfinished_orders_update_item($itm_id, $data) {
        return $this->ci->orders->update_item($itm_id, $data);
    }

    function orders_delete_item($itm_id) {
        return $this->ci->orders->delete_item($itm_id);
    }

    function orders_update_item($itm_id, $data, $previous) {
        $text_history = array();
        if ($previous->itm_unit_price != $data['itm_unit_price']) {
            $text_history[] = "Se modificó el precio del producto " . $previous->prod_code . " de $" . $previous->itm_unit_price . " a $" . $data['itm_unit_price'];
        }
        if ($previous->itm_quantity != $data['itm_quantity']) {
            $text_history[] = "Se modificó la cantidad del producto " . $previous->prod_code . " de " . $previous->itm_quantity . " a " . $data['itm_quantity'];
        }
        if ($previous->itm_comments != $data['itm_comments']) {
            $text_history[] = "Se modificó el comentario del producto " . $previous->prod_code;
        }

        $data['itm_comments'] = '<p style="font-weight:bold;">' . $data['itm_comments'] . '<p>';

        if ($this->ci->orders->update_item($itm_id, $data)) {
            $order['ord_modified_at'] = date('Y-m-d H:i:s');
            if ($this->ci->orders->update($data['itm_order_id'], $order)) {
                foreach ($text_history as $value) {
                    $this->insert_history($previous->itm_order_id, $value);
                }
                return [
                    'error' => FALSE,
                    'message' => 'Datos actualizados satisfactoriamente!',
                    'history' => $text_history,
                    'order_id' => $data['itm_order_id'],
                    'last_item' => $itm_id
                ];
            } else {
                return ['error' => TRUE, 'message' => 'Problemas al actualizar el pedido!'];
            }
        } else {
            return ['error' => TRUE, 'message' => 'Problemas al actualizar datos del item!'];
        }
    }

    function send_mail_items_update($itm_id, $order_id, $history) {
        //Send email
        if (count($history) > 0) {
            $email = $this->ci->orders->get_user_email_by_item_id($itm_id);
            $email_data = array();
            $email_data['order_id'] = $order_id;
            $email_data['history'] = $history;
            $email_data['site_name'] = $this->ci->config->item('website_name', 'tank_auth');
            $this->_send_email($email, $email_data, 'Cambios en el pedido de ' . $email_data['site_name'], 'change_order');
        }
    }

    function orders_new() {
        return $this->ci->orders->insert(['ord_customer_id' => $this->ci->session->userdata('user_id')]);
    }

    function get_unfinished_order() {
        return $order = $this->ci->orders->get_unfinished_order_by_user($this->ci->session->userdata('user_id'));
    }

    function add_item_to_order($order_id, $prod_id, $quantity, $comments) {
        $product = $this->ci->products->get_by_id($prod_id);
        $item_id = $this->ci->orders->insert_item([
            'itm_order_id' => $order_id,
            'itm_product_id' => $prod_id,
            'itm_unit_price' => $product->prod_price,
            'itm_quantity' => $quantity,
            'itm_comments' => $comments
        ]);
        if ($item_id) {
            return ['error' => FALSE, 'message' => 'Agregado al carrito de compra satisfactoriamente!'];
        } else {
            return ['error' => TRUE, 'message' => 'Problemas al agregar el item al carrito de compra!'];
        }
        die();
    }

    function send_finished_order_email($item_list, $order_id, $total) {
        $email_data = array();
        $email_data['order_id'] = $order_id;
        $email_data['item_list'] = $item_list;
        $email_data['total'] = $total;
        $email_data['site_name'] = $this->ci->config->item('website_name', 'tank_auth');
        $this->_send_email($this->ci->session->userdata('username'), $email_data, 'Pedido creado en Kiara', 'create_order');
    }

    /*     * *
     * ACTUALIZE SESSION DATA
     */

    function actualize_shopping_data($order_id = NULL, $item_id = NULL) {
        if (!$order_id && !$item_id) {
            $this->ci->session->set_userdata('shopping_cart', 0);
            $this->ci->session->set_userdata('shopping_total', 0);
            $this->ci->session->set_userdata('shopping_items', array());
        } else {
            if (!$order_id) {
                $order_id = $this->order_by_item_id($item_id);
                if ($order_id) {
                    $order_id = $order_id->itm_order_id;
                }
            }
            if ($order_id) {
                $items = $this->order_items_by_order($order_id);
                $total = 0;
                $items_in_session = 0;
                $items_session = array();
                foreach ($items as &$item) {
                    if ($items_in_session < 4) {
                        $item->image = $this->product_image($item->itm_product_id);
                        $items_session[] = array(
                            'prod_name' => $item->prod_name,
                            'prod_code' => $item->prod_code,
                            'itm_price' => $item->itm_unit_price * $item->itm_quantity,
                            'itm_quantity' => $item->itm_quantity,
                            'image' => $item->image,
                        );
                    }
                    $items_in_session += 1;
                    $total += $item->itm_unit_price * $item->itm_quantity;
                }
                $this->ci->session->set_userdata('shopping_cart', count($items));
                $this->ci->session->set_userdata('shopping_total', $total);
                $this->ci->session->set_userdata('shopping_items', $items_session);
            } else {
                $this->ci->session->set_userdata('shopping_cart', 0);
                $this->ci->session->set_userdata('shopping_total', 0);
                $this->ci->session->set_userdata('shopping_items', array());
            }
        }
    }

    /*     * *
     * PRIVATE FUNCTIONS
     */

    private
            function name_validation($field, $name, $error_message) {
        $this->ci->form_validation->set_rules($field, $name, 'trim|required|xss_clean');
        if ($this->ci->form_validation->run()) {
            return ['result' => TRUE, 'message' => $this->ci->form_validation->set_value($field)];
        } else {
            return ['result' => FALSE, 'message' => $error_message];
        }
    }

    private function insert_history($order_id, $text) {
        $data = array();
        $data['hst_user_id'] = $this->ci->session->userdata('user_id');
        $data['hst_order_id'] = $order_id;
        $data['hst_description'] = $text;
        $this->ci->orders->insert_history($data);
    }

    /**
     * Send email
     *
     * @param	string
     * @param	string
     * @param	array
     * @return	void
     */
    private function _send_email($email, &$data, $subject, $view) {
        $this->ci->load->library('email');
        $this->ci->email->from($this->ci->config->item('webmaster_email', 'tank_auth'), $this->ci->config->item('website_name', 'tank_auth'));
        $this->ci->email->reply_to($this->ci->config->item('webmaster_email', 'tank_auth'), $this->ci->config->item('website_name', 'tank_auth'));
        $this->ci->email->to($email);
        $this->ci->email->subject($subject);
        $this->ci->email->message($this->ci->load->view('email/' . $view . '-html', $data, TRUE));
        $this->ci->email->set_alt_message($this->ci->load->view('email/' . $view . '-txt', $data, TRUE));
        $this->ci->email->send();
    }

}

/* End of file catalog.php */
/* Location: ./application/libraries/catalog.php */