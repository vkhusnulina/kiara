<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customers extends CI_Controller {

    //Duplicated in orders.php
    private function general_status() {
        return ['1' => ['PENDIENTE', 'info'], '2' => ['REVISADO', 'info'], '3' => ['TERMINADO', 'success'], '4' => ['ANULADO', 'danger']];
    }

    //Duplicated in orders.php
    private function payment_status() {
        return ['1' => ['PENDIENTE', 'info'], '2' => ['CONFIRMADO', 'success']];
    }

    //Duplicated in orders.php
    private function shipping_status() {
        return ['1' => ['EN PROCESO', 'info'], '2' => ['ENVIADO', 'success']];
    }

    function __construct() {
        parent::__construct();

        $this->load->library('catalog');
    }

    function admin_list() {
        $this->control_auth_and_permission([1, 2]);
        $data = array(
            'section' => 'Clientes',
            'description' => 'Administre a los clientes que están registrados en su tienda.',
            'view' => 'admin/customers/list',
            'view_data' => ['customers' => $this->catalog->customers_all()],
        );
        $this->load->view('admin/layout', $data);
    }

    function admin_new() {
        $this->control_auth_and_permission([1, 2]);
        $this->load->view('admin/customers/new');
    }

    function admin_insert() {
        $this->control_auth_and_permission([1, 2]);
        try {
            $data = $this->separate_tables_data($this->input->post());
            $insert = $this->catalog->customers_new($this->input->post(), $data['profile'], $data['address']);
            $this->echo_die($insert['error'], $insert['message']);
        } catch (Exception $exc) {
            $this->echo_die(TRUE, $exc->getMessage());
        }
    }

    function admin_view($user_id) {
        $this->control_auth_and_permission([1, 2]);
        try {
            $this->load->view('admin/customers/view', [
                'customer' => $this->catalog->customer_by_user_id($user_id),
                'orders' => $this->catalog->orders_by_user_id($user_id),
                'general_status' => $this->general_status(),
                'payment_status' => $this->payment_status(),
                'shipping_status' => $this->shipping_status()
            ]);
        } catch (Exception $exc) {
            $this->echo_die(TRUE, $exc->getMessage());
        }
    }

    function admin_update($user_id) {
        $this->control_auth_and_permission([1, 2]);
        try {
            $post_data = $this->input->post();
            $data = $this->separate_tables_data($post_data);
            //Actualizo el registro en la BD
            $update = $this->catalog->customers_update($user_id, $data['profile'], $data['address']);

            $this->echo_die($update['error'], $update['message']);
        } catch (Exception $exc) {
            $this->echo_die(TRUE, $exc->getMessage());
        }
    }

    private function separate_tables_data($data) {
        $array = array();
        foreach ($data as $key => $value) {
            if (strpos($key, 'prf_') !== false) {
                $array['profile'][$key] = $value;
            } elseif (strpos($key, 'add_') !== false) {
                $array['address'][$key] = $value;
            }
        }
        return $array;
    }

}

/* End of file customers.php */
/* Location: ./application/controllers/customers.php */

