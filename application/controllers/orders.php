<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Orders extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('catalog');
    }

    //Duplicated in customers.php
    private function general_status() {
        return ['1' => ['PENDIENTE', 'info'], '2' => ['REVISADO', 'info'], '3' => ['TERMINADO', 'success'], '4' => ['ANULADO', 'danger']];
    }

    //Duplicated in customers.php
    private function payment_status() {
        return ['1' => ['PENDIENTE', 'info'], '2' => ['CONFIRMADO', 'success']];
    }

    //Duplicated in customers.php
    private function shipping_status() {
        return ['1' => ['EN PROCESO', 'info'], '2' => ['ENVIADO', 'success']];
    }

    function admin_list() {
        $this->control_auth_and_permission([1, 2]);
        $data = array(
            'section' => 'Pedidos',
            'description' => 'Administración de pedidos de su sitio web.',
            'view' => 'admin/orders/list',
            'view_data' => [
                'orders' => $this->catalog->orders_all(),
                'general_status' => $this->general_status(),
                'payment_status' => $this->payment_status(),
                'shipping_status' => $this->shipping_status()
            ],
        );
        $this->load->view('admin/layout', $data);
    }

    function admin_insert() {

    }

    function admin_view($ord_id) {
        $this->control_auth_and_permission([1, 2]);
        try {
            $order = $this->catalog->orders_by_id($ord_id);
            $order_history = $this->catalog->order_history_by_order($ord_id);
            $order_items = $this->catalog->order_items_by_order($ord_id);

            //Seleccion de colores y talles por producto
            foreach ($order_items ? $order_items : array() as $key => $item) {
                $order_items[$key]->colors = $this->catalog->products_colors_by_id($item->itm_product_id);
                $order_items[$key]->sizes = $this->catalog->products_sizes_by_id($item->itm_product_id);
            }

            $this->load->view('admin/orders/view', [
                'order' => $order,
                'order_items' => $order_items,
                'order_history' => $order_history,
                'general_status' => $this->general_status(),
                'payment_status' => $this->payment_status(),
                'shipping_status' => $this->shipping_status()
            ]);
        } catch (Exception $exc) {
            $this->echo_die(TRUE, $exc->getMessage());
        }
    }

    function admin_update($ord_id) {
        $this->control_auth_and_permission([1, 2]);
        try {
            $post_data = $this->input->post();
            $data = $this->separate_tables_data($post_data);
            //Actualizo el registro en la BD
            $update = $this->catalog->orders_update($ord_id, $data['order'], $data['address'], $data['invoice'], $data['status'], $data['changes']);
            $this->echo_die($update['error'], $update['message']);
        } catch (Exception $exc) {
            $this->echo_die(TRUE, $exc->getMessage());
        }
    }

    function update($ord_id) {
        $this->control_auth_and_permission();
        try {
            $post_data = $this->input->post();
            if ($post_data) {
                $data = $this->separate_tables_data($post_data);
                //Actualizo el registro en la BD
                $update = $this->catalog->orders_update_without_comments(
                        $ord_id, ($data['order'] ? $data['order'] : []), ($data['address'] ? $data['address'] : []), ($data['invoice'] ? $data['invoice'] : [])
                );
                $this->echo_die($update['error'], $update['message']);
            } else {
                $this->echo_die(TRUE, 'No se recibieron datos');
            }
        } catch (Exception $exc) {
            $this->echo_die(TRUE, $exc->getMessage());
        }
    }

    function admin_update_all_items() {
        $items = $this->input->post();
        $history = array();
        $order_id = 0;
        $last_item = 0;
        $error = FALSE;
        foreach ($items['item'] as $itm_id => $data) {
            $previous_data = $this->catalog->order_item_by_id($itm_id);
            $update = $this->catalog->orders_update_item($itm_id, $data, $previous_data);
            if ($update['error']) {
                $error = TRUE;
            } else {
                $history = array_merge($history, $update['history']);
                if ($order_id == 0)
                    $order_id = $update['order_id'];
                if ($last_item == 0)
                    $last_item = $update['last_item'];
            }
        }
        if (!$error) {
            $this->catalog->send_mail_items_update($last_item, $order_id, $history);
            return ['error' => FALSE, 'message' => 'Datos actualizados satisfactoriamente!'];
        } else {
            return ['error' => TRUE, 'message' => 'Problemas al actualizar el pedido!'];
        }
    }

    function admin_update_item($itm_id) {
        $this->control_auth_and_permission([1, 2]);
        try {
            $data = $this->input->post();
            //Actualizo el registro en la BD
            $previous_data = $this->catalog->order_item_by_id($itm_id);
            $update = $this->catalog->orders_update_item($itm_id, $data, $previous_data);
            if ($update['error']) {
                $this->echo_die($update['error'], $update['message']);
            } else {
                $this->catalog->send_mail_items_update($update['last_item'], $update['order_id'], $update['history']);
                $this->echo_die($update['error'], $update['message']);
            }
        } catch (Exception $exc) {
            $this->echo_die(TRUE, $exc->getMessage());
        }
    }

    private
            function separate_tables_data($data = array()) {
        $array = array_keys(['order' => array(), 'address' => array(), 'invoice' => array(), 'status' => array(), 'changes' => array()]);
        foreach ($data as $key => $value) {
            if (strpos($key, 'ord_') !== false) {
                $array['order'][$key] = $value;
            } elseif (strpos($key, 'add_') !== false) {
                $array['address'][$key] = $value;
            } elseif (strpos($key, 'inv_') !== false) {
                $array['invoice'][$key] = $value;
            } elseif ($key == 'general' || $key == 'payment' || $key == 'shipping') {
                $array['status'][$key] = $value;
            } elseif ($key == 'address_change' || $key == 'comment_change' || $key == 'invoice_change' || $key == 'shipping_change' || $key == 'ship_to_change') {
                $array['changes'][$key] = $value;
            }
        }
        return $array;
    }

}

/* End of file orders.php */
    /* Location: ./application/controllers/orders.php */