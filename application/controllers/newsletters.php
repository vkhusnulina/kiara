<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Newsletters extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('newsletters_model', 'newsletters');
    }

    function admin_list() {
        $this->control_auth_and_permission([1, 2]);

        $data = array(
            'section' => 'Suscripciones al newsletter',
            'description' => 'Listado de suscriptos al newsletter.',
            'view' => 'admin/newsletters/list',
            'view_data' => ['newsletters' => $this->newsletters->getAll()],
        );

        $this->load->view('admin/layout', $data);
    }

    function add() {
        $this->newsletters->insert(['news_email' => $this->input->post('news_email')]);

        redirect('/?news=1');
    }

}
