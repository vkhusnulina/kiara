﻿<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Products extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library(['catalog', 'images']);
    }

    function admin_list() {
        $this->control_auth_and_permission([1, 2]);
        $data = array(
            'section' => 'Productos',
            'description' => 'Administración de los productos de su sitio web.',
            'view' => 'admin/products/list',
            'view_data' => ['products' => $this->catalog->products_all()],
        );
        $this->load->view('admin/layout', $data);
    }

    function admin_new() {
        $this->control_auth_and_permission([1, 2]);
        $categories = $this->catalog->categories_all();
        $colors = $this->catalog->colors_all();
        $sizes = $this->catalog->sizes_all();
        $shoes_sizes = $this->catalog->sizes_all(TRUE);
        $this->load->view('admin/products/new', [
            'categories' => $categories,
            'colors' => $colors,
            'sizes' => $sizes,
            'shoes_sizes' => $shoes_sizes
        ]);
    }

    function admin_insert() {
        $this->control_auth_and_permission([1, 2]);
        try {
            $post_data = $this->input->post();
            $post_categories = $post_data['categories'];
            unset($post_data['categories']);
            $post_colors = $post_data['colors'];
            unset($post_data['colors']);
            $post_sizes = $post_data['sizes'];
            unset($post_data['sizes']);

            //Guardo el producto en BD
            $insert = $this->catalog->products_new($post_data);
            if ($insert['error']) {
                $this->echo_die(TRUE, $insert['message']);
            }
            $prod_id = $insert['message'];

            if ($_FILES['prod_img']['name'][0] == !'') {
                $upload = $this->do_upload($_FILES['prod_img']);
                if ($upload['error']) {
                    $this->echo_die(TRUE, $upload['message']);
                }
                $images = $upload['message'];

                foreach ($images as $image) {
                    $saved_media = $this->catalog->media_new($image);
                    if ($saved_media['error']) {
                        $this->echo_die(TRUE, $saved_media['message']);
                    }
                    $media_id = $saved_media['message'];
                    $update = $this->catalog->products_media_save($prod_id, $media_id);
                    if ($update['error']) {
                        $this->echo_die(TRUE, $update['message']);
                    }
                }
            }

            //Actualizo el registro en la BD
            $categories = $this->catalog->products_categories_update($prod_id, $post_categories);
            $colors = $this->catalog->products_colors_update($prod_id, $post_colors);
            $sizes = $this->catalog->products_sizes_update($prod_id, $post_sizes);
            $this->echo_die(($categories['error'] && $colors['error'] && $sizes['error']), $categories['message'] . ' ' . $colors['message'] . ' ' . $sizes['message']);
        } catch (Exception $exc) {
            $this->echo_die(TRUE, $exc->getMessage());
        }
    }

    function admin_view($prod_id) {
        $this->control_auth_and_permission([1, 2]);
        try {
            $product = $this->catalog->products_by_id($prod_id);

            $images = $this->catalog->products_images_by_id($prod_id);

            $product_categories = $this->catalog->products_categories_by_id($prod_id);
            $categories = $this->catalog->categories_all();
            $this->control_added_items($categories, $product_categories, 'categories_cat_id', 'cat_id');

            $product_colors = $this->catalog->products_colors_by_id($prod_id);
            $colors = $this->catalog->colors_all();
            $this->control_added_items($colors, $product_colors, 'colors_clr_id', 'clr_id');

            $product_sizes = $this->catalog->products_sizes_by_id($prod_id);
            $sizes = $this->catalog->sizes_all();
            $this->control_added_items($sizes, $product_sizes, 'sizes_siz_id', 'siz_id');
            $shoes_sizes = $this->catalog->sizes_all(TRUE);
            $this->control_added_items($shoes_sizes, $product_sizes, 'sizes_siz_id', 'siz_id');

            $this->load->view('admin/products/view', [
                'product' => $product,
                'images' => $images,
                'categories' => $categories,
                'colors' => $colors,
                'sizes' => $sizes,
                'shoes_sizes' => $shoes_sizes
            ]);
        } catch (Exception $exc) {
            $this->echo_die(TRUE, $exc->getMessage());
        }
    }

    function admin_update($prod_id) {
        $this->control_auth_and_permission([1, 2]);
        try {
            $post_data = $this->input->post();
            $post_categories = $post_data['categories'];
            unset($post_data['categories']);
            $post_colors = $post_data['colors'];
            unset($post_data['colors']);
            $post_sizes = $post_data['sizes'];
            unset($post_data['sizes']);

            if ($_FILES['prod_img']['name'][0] == !'') {
                $upload = $this->do_upload($_FILES['prod_img']);
                if ($upload['error']) {
                    $this->echo_die(TRUE, $upload['message']);
                }
                $images = $upload['message'];

                foreach ($images as $image) {
                    $saved_media = $this->catalog->media_new($image);
                    if ($saved_media['error']) {
                        $this->echo_die(TRUE, $saved_media['message']);
                    }
                    $media_id = $saved_media['message'];
                    $update = $this->catalog->products_media_save($prod_id, $media_id);
                    if ($update['error']) {
                        $this->echo_die(TRUE, $update['message']);
                    }
                }
            }
            //Actualizo el registro en la BD
            $update = $this->catalog->products_update($prod_id, $post_data);
            if ($update['error']) {
                $this->echo_die($update['error'], $update['message']);
            } else {
                //Actualizo el registro en la BD
                $categories = $this->catalog->products_categories_update($prod_id, $post_categories);
                $colors = $this->catalog->products_colors_update($prod_id, $post_colors);
                $sizes = $this->catalog->products_sizes_update($prod_id, $post_sizes);
                $this->echo_die(($categories['error'] && $colors['error'] && $sizes['error']), $categories['message'] . ' ' . $colors['message'] . ' ' . $sizes['message']);
            }
        } catch (Exception $exc) {
            $this->echo_die(TRUE, $exc->getMessage());
        }
    }

    function admin_change_details($prod_id) {
        $this->control_auth_and_permission([1, 2]);
        try {
            $post_data = $this->input->post();
            //Actualizo el registro en la BD
            $categories = $this->catalog->products_categories_update($prod_id, $post_data['categories']);
            $colors = $this->catalog->products_colors_update($prod_id, $post_data['colors']);
            $sizes = $this->catalog->products_sizes_update($prod_id, $post_data['sizes']);
            $this->echo_die(($categories['error'] && $colors['error'] && $sizes['error']), $categories['message'] . ' ' . $colors['message'] . ' ' . $sizes['message']);
        } catch (Exception $exc) {
            $this->echo_die(TRUE, $exc->getMessage());
        }
    }

    function admin_delete_image($media_id) {
        $this->control_auth_and_permission([1, 2]);
        try {
            //Actualizo el registro en la BD
            $delete = $this->catalog->products_media_delete($media_id);
            if ($delete) {
                echo json_encode(new stdClass);
                die();
            } else {
                echo 'Problemas al intentar eliminar la imagen.';
                die();
            }
        } catch (Exception $exc) {
            echo 'Mi error' . $exc->getMessage();
            die();
        }
    }

    private function control_added_items(&$general_array, $added_array, $added_id, $general_id) {
        $ids = array();
        foreach (($added_array ? $added_array : array()) as $added_item) {
            $ids[] = $added_item->$added_id;
        }
        foreach (($general_array ? $general_array : array()) as $key => $general_item) {
            if (in_array($general_item->$general_id, $ids)) {
                $general_array[$key]->exist = TRUE;
            } else {
                $general_array[$key]->exist = FALSE;
            }
        }
    }

    private function do_upload($files) {
        $config = array(
            'upload_path' => 'assets/admin/images/products/',
            'allowed_types' => 'jpg|gif|png',
            'overwrite' => 0,
            'maintain_ratio' => TRUE,
            'create_thumb' => FALSE,
            'encrypt_name' => TRUE
        );

        $this->load->library('upload', $config);
        $this->load->library('image_lib');
        $images = array();
        $savedImages = array();

        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name'] = $files['name'][$key];
            $_FILES['images[]']['type'] = $files['type'][$key];
            $_FILES['images[]']['tmp_name'] = $files['tmp_name'][$key];
            $_FILES['images[]']['error'] = $files['error'][$key];
            $_FILES['images[]']['size'] = $files['size'][$key];

            $images[] = $image;

            $fileName = md5(uniqid(mt_rand()));
            $config['file_name'] = $fileName;

            $this->upload->initialize($config);

            //Guardo la imagen
            if ($this->upload->do_upload('images[]')) {
                $file_data = $this->upload->data();
                $savedImages[] = $file_data['file_name'];
                //Configuracion para redimensionamiento
                $max_height = 700;
                $max_width = 700;
                $configResize = array(
                    'source_image' => $file_data['full_path'],
                    'width' => $max_width,
                    'height' => $max_height,
                    'maintain_ratio' => TRUE,
                    'create_thumb' => FALSE
                );

                $this->image_lib->clear();
                $this->image_lib->initialize($configResize);
                $this->image_lib->resize();
            } else {
                //En caso de error en el gurdado
                return ['error' => TRUE, 'message' => 'No se pudo guardar las imagenes'];
            }
        }
        return ['error' => FALSE, 'message' => $savedImages];
    }

}

/* End of file products.php */
/* Location: ./application/controllers/products.php */