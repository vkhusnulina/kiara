<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Shoes_sizes extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('catalog');
    }

    function admin_list() {
        $this->control_auth_and_permission([1, 2]);
        $data = array(
            'section' => 'Talles de calzados',
            'description' => 'Administración de atributos de productos de su sitio web.',
            'view' => 'admin/product_attributes/shoes_sizes/list',
            'view_data' => ['shoes_sizes' => $this->catalog->sizes_all(TRUE)],
        );
        $this->load->view('admin/layout', $data);
    }

    function admin_new() {
        $this->control_auth_and_permission([1, 2]);
        $this->load->view('admin/product_attributes/shoes_sizes/new');
    }

    function admin_insert() {
        $this->control_auth_and_permission([1, 2]);
        try {
            //Guardo registro en BD
            $post = $this->input->post();
            $post['siz_shoes'] = 1;
            $insert = $this->catalog->sizes_new($post);
            $this->echo_die($insert['error'], $insert['message']);
        } catch (Exception $exc) {
            $this->echo_die(TRUE, $exc->getMessage());
        }
    }

    function admin_view($siz_id) {
        $this->control_auth_and_permission([1, 2]);
        try {
            $this->load->view('admin/product_attributes/shoes_sizes/view', ['shoes_size' => $this->catalog->sizes_by_id($siz_id)]);
        } catch (Exception $exc) {
            $this->echo_die(TRUE, $exc->getMessage());
        }
    }

    function admin_update($siz_id) {
        $this->control_auth_and_permission([1, 2]);
        try {
            $post_data = $this->input->post();
            //Actualizo el registro en la BD
            $update = $this->catalog->sizes_update($siz_id, $post_data);
            $this->echo_die($update['error'], $update['message']);
        } catch (Exception $exc) {
            $this->echo_die(TRUE, $exc->getMessage());
        }
    }

}

/* End of file shoes_sizes.php */
/* Location: ./application/controllers/shoes_sizes.php */
