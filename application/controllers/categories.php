<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Categories extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('catalog');
        $this->load->library('images');
    }

    function admin_list() {
        $this->control_auth_and_permission([1, 2]);
        $data = array(
            'section' => 'Categorías',
            'description' => 'Administre las categorías que se utilizan para agrupar los productos de su tienda.',
            'view' => 'admin/categories/list',
            'view_data' => ['categories' => $this->catalog->categories_all()],
        );
        $this->load->view('admin/layout', $data);
    }

    function admin_new() {
        $this->control_auth_and_permission([1, 2]);
        $this->load->view('admin/categories/new');
    }

    function admin_insert() {
        $this->control_auth_and_permission([1, 2]);
        try {
            //Control de la imagen a subir
            $image_control = $this->images->control($_FILES['cat_img'], 200);
            if (!$image_control['result']) {
                $this->echo_die(TRUE, $image_control['message']);
            }

            //Guardo registro en BD
            $insert = $this->catalog->categories_new($this->input->post());
            if ($insert['error']) {
                $this->echo_die(TRUE, $insert['message']);
            }

            //Guardo la imagen
            $image_uploaded = $this->images->upload($_FILES['cat_img'], 'ctg' . $insert['message'], 'categories');
            if (!$image_uploaded) {
                $this->echo_die(TRUE, 'Problemas internos al subir la imagen');
            }

            //Guardo nombre de la imagen en la BD
            $update = $this->catalog->categories_update($insert['message'], ['cat_image' => $image_uploaded]);
            $this->echo_die($update['error'], $update['message']);
        } catch (Exception $exc) {
            $this->echo_die(TRUE, $exc->getMessage());
        }
    }

    function admin_view($cat_id) {
        $this->control_auth_and_permission([1, 2]);
        try {
            $this->load->view('admin/categories/view', ['category' => $this->catalog->categories_by_id($cat_id)]);
        } catch (Exception $exc) {
            $this->echo_die(TRUE, $exc->getMessage());
        }
    }

    function admin_update($cat_id) {
        $this->control_auth_and_permission([1, 2]);
        try {
            $post_data = $this->input->post();
            if (is_uploaded_file($_FILES['cat_img']['tmp_name'])) {
                //Control de la imagen a subir
                $image_control = $this->images->control($_FILES['cat_img'], 200);
                if (!$image_control['result']) {
                    $this->echo_die(TRUE, $image_control['message']);
                }

                //Guardo la imagen
                $image_uploaded = $this->images->upload($_FILES['cat_img'], 'ctg' . $cat_id, 'categories');
                if (!$image_uploaded) {
                    $this->echo_die(TRUE, 'Problemas internos al subir la imagen');
                }
                $post_data['cat_image'] = $image_uploaded; //Nombre de la imagen
            }

            //Actualizo el registro en la BD
            $update = $this->catalog->categories_update($cat_id, $post_data);
            $this->echo_die($update['error'], $update['message']);
        } catch (Exception $exc) {
            $this->echo_die(TRUE, $exc->getMessage());
        }
    }

}

/* End of file categories.php */
/* Location: ./application/controllers/categories.php */