<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Slideshow extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library(['catalog', 'images']);
    }

    function manage() {
        $this->control_auth_and_permission([1, 2]);
        $data = array(
            'section' => 'Slideshow',
            'description' => 'Administración de la galería slideshow.',
            'view' => 'admin/slideshow/manage',
            'view_data' => ['images' => $this->catalog->slideshow_all()],
        );
        $this->load->view('admin/layout', $data);
    }

    function upload() {
        if ($_FILES['img']['name'][0] == !'') {
            $upload = $this->do_upload($_FILES['img']);
            if ($upload['error']) {
                $this->echo_die(TRUE, $upload['message']);
            }
            $images = $upload['message'];

            foreach ($images as $image) {
                $saved_media = $this->catalog->media_new($image, 3);
                if ($saved_media['error']) {
                    $this->echo_die(TRUE, $saved_media['message']);
                }
            }
        }
        $this->echo_die(FALSE, 'Galería guardada!');
    }
    
    
    private function do_upload($files) {
        $config = array(
            'upload_path' => 'assets/Resources/slideshow/',
            'allowed_types' => 'jpg|gif|png',
            'overwrite' => 0,
            'maintain_ratio' => TRUE,
            'create_thumb' => FALSE,
            'encrypt_name' => TRUE
        );

        $this->load->library('upload', $config);
        $this->load->library('image_lib');
        $images = array();
        $savedImages = array();

        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name'] = $files['name'][$key];
            $_FILES['images[]']['type'] = $files['type'][$key];
            $_FILES['images[]']['tmp_name'] = $files['tmp_name'][$key];
            $_FILES['images[]']['error'] = $files['error'][$key];
            $_FILES['images[]']['size'] = $files['size'][$key];

            $images[] = $image;

            $fileName = md5(uniqid(mt_rand()));
            $config['file_name'] = $fileName;

            $this->upload->initialize($config);

            //Guardo la imagen
            if ($this->upload->do_upload('images[]')) {
                $file_data = $this->upload->data();
                $savedImages[] = $file_data['file_name'];
            } else {
                //En caso de error en el gurdado
                return ['error' => TRUE, 'message' => 'No se pudo guardar las imagenes'];
            }
        }
        return ['error' => FALSE, 'message' => $savedImages];
    }

}
