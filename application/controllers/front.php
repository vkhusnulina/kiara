<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Front extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('catalog');
        $this->load->library('session');
    }

    function home() {
        /* $slideshow = array();
          foreach (glob('./assets/Resources/slideshow/*.*') as $filename) {
          if (strpos($filename, 'Thumbs.db') === FALSE) {
          $slideshow[] = $filename;
          }
          } */

        $data = array(
            'section' => 'Inicio',
            'view' => 'welcome',
            'view_data' => [
                'categories' => $this->catalog->categories_all(True),
                'slideshow' => $this->catalog->slideshow_all()
            ],
        );
        $this->load->view('layout', $data);
    }

    function size_guide() {
        $data = array(
            'section' => 'Guía de Talles',
            'view' => 'size_guide',
            'view_data' => [],
        );
        $this->load->view('layout', $data);
    }

    function where_we_are() {
        $data = array(
            'section' => 'Donde Encontrarnos',
            'view' => 'where_we_are',
            'view_data' => [],
        );
        $this->load->view('layout', $data);
    }

    function who_are_we() {
        $data = array(
            'section' => 'Quienes Somos',
            'view' => 'welcome',
            'view_data' => [],
        );
        $this->load->view('layout', $data);
    }

    function contact() {
        $data = array(
            'section' => 'Contacto',
            'view' => 'contact',
            'view_data' => [],
        );
        $this->load->view('layout', $data);
    }

    public function contact_submit() {
        $this->form_validation->set_rules('nombre', 'Nombre completo', 'required|trim|max_length[200]|xss_clean');
        $this->form_validation->set_rules('telefono', 'Teléfono', 'required|trim|max_length[200]|xss_clean');
        $this->form_validation->set_rules('provincia', 'Provincia', 'required|trim|max_length[200]|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|max_length[200]|xss_clean');
        $this->form_validation->set_rules('mensaje', 'Mensaje', 'required|trim|xss_clean');

        if (!$this->form_validation->run()) {
            redirect(site_url('/contacto?r=2'));
        } else {
            $this->load->library('parser');
                $this->load->library('email');

            $array_datos_contacto = array(
                'nombre' => $this->input->post('nombre'),
                'telefono' => $this->input->post('telefono'),
                'provincia' => $this->input->post('provincia'),
                'email' => $this->input->post('email'),
                'mensaje' => $this->input->post('mensaje'),
            );

            $mail_body = $this->parser->parse('email/email_view', $array_datos_contacto, true);

            $titulo = 'Consulta enviada desde el sitio web kiaraweb.com.ar.';

            $para = ' info@kiaraweb.com.ar';
            //$para = 'carolina@dolmastudio.com';
            $name = $this->input->post('nombre');
            $email = $this->input->post('email');

            $cabeceras = "MIME-Version: 1.0\r\n" . 'From: ' . $name . '<' . $email . '>' . "\r\n" . 'Reply-To: ' . $email . '<' . $email . '>' . "\r\n" . 'X-Mailer: PHP/' . phpversion();

            if (!mail($para, $titulo, $mail_body, $cabeceras)) {
                redirect(site_url('/contacto?r=0'));
            } else {
                redirect(site_url('/contacto?r=1'));
            }
        }
    }

    function how_buy() {
        $data = array(
            'section' => 'Como Comprar',
            'view' => 'how_buy',
            'view_data' => [],
        );
        $this->load->view('layout', $data);
    }

    function categories() {
        $data = array(
            'section' => 'Productos',
            'view' => 'categories',
            'view_data' => ['categories' => $this->catalog->categories_all(True)],
        );
        $this->load->view('layout', $data);
    }

    function product($slug_c, $slug_p) {
        //Control the slug
        if ($slug_c != strtolower($slug_c) || $slug_p != strtolower($slug_p)) {
            redirect(base_url() . strtolower($slug_c) . '/' . strtolower($slug_p));
            die();
        }
        //Find category by slug
        $category = $this->catalog->categories_by_slug($slug_c);
        if ($category) {
            if ($category->cat_visible == 0) {
                redirect(base_url());
                die();
            }
            //with products array
            $products = $this->catalog->categories_products_by_id($category->cat_id, 1);
            $product = NULL;
            foreach ($products as &$product_item) {
                if ($product_item->prod_slug == $slug_p && $product_item->prod_visible == '1') {
                    $product = $product_item;
                    $product_item->selected = TRUE;
                } else {
                    $product_item->selected = FALSE;
                }
            }
            if (!$product) {
                redirect(base_url() . $slug_c);
                die();
            }
            //with images, colors and sizes
            $colors = $this->catalog->products_colors_by_id($product->prod_id);
            $sizes = $this->catalog->products_sizes_by_id($product->prod_id);
            $images = $this->catalog->products_images_by_id($product->prod_id);

            //also return the selected category
            $data = array(
                'section' => $product->prod_name,
                'view' => 'product',
                'view_data' => [
                    'categories' => $this->catalog->categories_all(True),
                    'products' => $products,
                    'category' => $category,
                    'product' => $product,
                    'images' => $images,
                    'colors' => $colors,
                    'sizes' => $sizes
                ],
            );
            $this->load->view('layout', $data);
        } else {
            redirect(base_url());
        }
    }

    function category($slug) {
        //Control the slug
        if ($slug != strtolower($slug)) {
            redirect(base_url() . strtolower($slug));
            die();
        }

        //Find category by slug
        $category = $this->catalog->categories_by_slug($slug);
        if ($category) {
            if ($category->cat_visible == 0) {
                redirect(base_url());
                die();
            }
            //with products array
            $products = $this->catalog->categories_products_by_id($category->cat_id, 1);
            foreach ($products as &$product) {
                $product->media = $this->catalog->products_images_by_id($product->prod_id);
                $product->colors = $this->catalog->products_colors_by_id($product->prod_id);
            }
            //with colors array
            //also return the slug of category selected
            $data = array(
                'section' => $category->cat_name,
                'view' => 'products',
                'view_data' => [
                    'categories' => $this->catalog->categories_all(True),
                    'products' => $products,
                    'category' => $category
                ],
            );
            $this->load->view('layout', $data);
        } else {
            redirect(base_url());
        }
    }

    function my_account() {
        $this->control_auth_and_permission();
        $orders = $this->catalog->orders_by_user_id($this->session->userdata('user_id'));
        foreach ($orders as &$order) {
            $items = $this->catalog->order_items_by_order($order->ord_id);
            $total = 0;
            $edit = TRUE;
            if (($order->ord_general_status == 3) ||
                    ($order->ord_general_status == 4) ||
                    ($order->ord_payment_status == 2) ||
                    ($order->ord_shipping_status == 2)) {
                $edit = FALSE;
            }
            foreach ($items as &$item) {
                $item->image = $this->catalog->product_image($item->itm_product_id);
                $total += $item->itm_unit_price * $item->itm_quantity;
            }
            $order->items = $items;
            $order->total = $total;
            $order->edit = $edit;
        }

        $data = array(
            'section' => 'Mi Cuenta',
            'view' => 'my_account',
            'view_data' => [
                'user_id' => $this->session->userdata('user_id'),
                'customer' => $this->catalog->customer_by_user_id($this->session->userdata('user_id')),
                'orders' => $orders,
                'general_status' => $this->general_status(),
                'payment_status' => $this->payment_status(),
                'shipping_status' => $this->shipping_status()
            ],
        );
        $this->load->view('layout', $data);
    }

    function add_to_order() {
        $this->control_auth_and_permission();
        $data = $this->input->post();
        $order = $this->catalog->get_unfinished_order();
        if (!$order) {
            $order_id = $this->catalog->orders_new();
        } else {
            $order_id = $order->ord_id;
        }
        $result = $this->catalog->add_item_to_order($order_id, $data['product'], $data['quantity'], $data['description']);
        if (!$result['error']) {
            if ($order_id) {
                $items = $this->catalog->order_items_by_order($order_id);
                $total = 0;
                $items_in_session = 0;
                $items_session = array();
                foreach ($items as &$item) {
                    if ($items_in_session < 4) {
                        $item->image = $this->catalog->product_image($item->itm_product_id);
                        $items_session[] = array(
                            'prod_name' => $item->prod_name,
                            'prod_code' => $item->prod_code,
                            'itm_price' => $item->itm_unit_price * $item->itm_quantity,
                            'itm_quantity' => $item->itm_quantity,
                            'image' => $item->image,
                        );
                        $items_in_session += 1;
                    }
                    $total += $item->itm_unit_price * $item->itm_quantity;
                }
                $this->session->set_userdata('shopping_cart', count($items));
                $this->session->set_userdata('shopping_total', $total);
                $this->session->set_userdata('shopping_items', $items_session);
            } else {
                $this->session->set_userdata('shopping_cart', 0);
                $this->session->set_userdata('shopping_total', 0);
                $this->session->set_userdata('shopping_items', array());
            }
        }
        $this->echo_die($result['error'], $result['message']);
    }

    function actual_order() {
        $this->control_auth_and_permission();
        $order = $this->catalog->get_unfinished_order();
        $items = array();
        if ($order) {
            $items = $this->catalog->order_items_by_order($order->ord_id);
            foreach ($items as $item) {
                $item->image = $this->catalog->product_image($item->itm_product_id);
            }
        }
        $data = array(
            'section' => 'Mi carrito',
            'view' => 'actual_order',
            'view_data' => ['items' => $items, 'order' => $order],
        );
        $this->load->view('layout', $data);
    }

    function update_item($itm_id) {
        $this->control_auth_and_permission();
        if ($this->catalog->unfinished_orders_update_item($itm_id, $this->input->post())) {
            $this->catalog->actualize_shopping_data(NULL, $itm_id);
            $this->echo_die(FALSE, 'Guardado');
        } else {
            $this->echo_die(TRUE, 'Error');
        }
    }

    function delete_item($itm_id) {
        $this->control_auth_and_permission();
        $this->session->set_userdata('shopping_cart', $this->session->userdata("shopping_cart") - 1);
        $order_id = $this->catalog->order_by_item_id($itm_id);
        $this->catalog->orders_delete_item($itm_id);
        $this->catalog->actualize_shopping_data($order_id);
        $this->echo_die(FALSE, "");
    }

    function shipping_invoices() {
        $this->control_auth_and_permission();
        $order = $this->catalog->get_unfinished_order();
        $items = array();
        if ($order) {
            $items = $this->catalog->order_items_by_order($order->ord_id);
            $total = 0;
            foreach ($items as $item) {
                $total = $total + ($item->itm_unit_price * $item->itm_quantity);
            }
            if ($total < 2000) {
                redirect('mi_carrito');
                die();
            }
            $customer = $this->catalog->customer_by_user_id($this->session->userdata('user_id'));
            $data = array(
                'section' => 'Envío y Facturación',
                'view' => 'shipping_invoices',
                'view_data' => [
                    'order' => $order,
                    'customer' => $customer
                ],
            );
            $this->load->view('layout', $data);
        } else {
            redirect('');
            die();
        }
    }

    function shipping_payment() {
        $this->control_auth_and_permission();
        $order = $this->catalog->get_unfinished_order();
        $items = array();
        if ($order) {
            $items = $this->catalog->order_items_by_order($order->ord_id);
            $total = 0;
            foreach ($items as $item) {
                $total = $total + ($item->itm_unit_price * $item->itm_quantity);
            }
            if ($total < 2000) {
                redirect('mi_carrito');
                die();
            }
            $customer = $this->catalog->customer_by_user_id($this->session->userdata('user_id'));
            $data = array(
                'section' => 'Métodos de envío y Pago',
                'view' => 'shipping_payment',
                'view_data' => [
                    'order' => $order,
                    'customer' => $customer
                ],
            );
            $this->load->view('layout', $data);
        } else {
            redirect('');
            die();
        }
    }

    function finish($ord_id) {
        $this->control_auth_and_permission();
        $order = $this->catalog->get_unfinished_order();
        $items = array();
        if ($order) {
            if ($order->ord_id == $ord_id) {
                $items = $this->catalog->order_items_by_order($order->ord_id);
                $total = 0;
                $list_items = array();
                foreach ($items as $item) {
                    $total = $total + ($item->itm_unit_price * $item->itm_quantity);
                    $list_items[] = strtoupper($item->prod_name) . ' (codigo #' . $item->prod_code . ') precio unitario: $' . $item->itm_unit_price . ' cantidad: ' . $item->itm_quantity . ' subtotal: $' . ($item->itm_quantity * $item->itm_unit_price);
                }
                if ($total < 2000) {
                    redirect('mi_carrito');
                    die();
                }
                $data['ord_finished'] = 1;
                $update = $this->catalog->orders_update_without_comments($ord_id, $data, [], []);
                if ($update['error']) {
                    redirect('metodo_de_envio_y_pago');
                    die();
                }
                $this->catalog->actualize_shopping_data();
                $this->catalog->send_finished_order_email($list_items, $ord_id, $total);
                $dataView = array(
                    'section' => 'Finalizar',
                    'view' => 'finish',
                    'view_data' => [
                        'order' => $order
                    ],
                );
                $this->load->view('layout', $dataView);
            } else {
                redirect('');
                die();
            }
        } else {
            redirect('');
            die();
        }
    }

    function update_address($user_id) {
        $this->control_auth_and_permission();
        try {
            $post_data = $this->input->post();
            //Actualizo el registro en la BD
            $update = $this->catalog->customers_update($user_id, NULL, $post_data);

            $this->echo_die($update['error'], $update['message']);
        } catch (Exception $exc) {
            $this->echo_die(TRUE, $exc->getMessage());
        }
    }

    function update_account($user_id) {
        $this->control_auth_and_permission();
        try {
            $post_data = $this->input->post();
            var_dump($post_data);
            //Actualizo el registro en la BD
            $update = $this->catalog->customers_update($user_id, $post_data, NULL);

            $this->echo_die($update['error'], $update['message']);
        } catch (Exception $exc) {
            $this->echo_die(TRUE, $exc->getMessage());
        }
    }

    //Duplicated in customers.php
    private function general_status() {
        return ['1' => ['Pendiente', 'info'], '2' => ['Revisado', 'info'], '3' => ['Terminado', 'success'], '4' => ['Anulado', 'danger']];
    }

    //Duplicated in customers.php
    private function payment_status() {
        return ['1' => ['Pendiente', 'info'], '2' => ['Confirmado', 'success']];
    }

    //Duplicated in customers.php
    private function shipping_status() {
        return ['1' => ['En Proceso', 'info'], '2' => ['Enviado', 'success']];
    }

}
