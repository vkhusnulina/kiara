<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Colors extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('catalog');
        $this->load->library('images');
    }

    function admin_list() {
        $this->control_auth_and_permission([1, 2]);
        $data = array(
            'section' => 'Colores y Texturas',
            'description' => 'Administración de atributos de productos de su sitio web.',
            'view' => 'admin/product_attributes/colors/list',
            'view_data' => ['colors' => $this->catalog->colors_all()],
        );
        $this->load->view('admin/layout', $data);
    }

    function admin_new() {
        $this->control_auth_and_permission([1, 2]);
        $this->load->view('admin/product_attributes/colors/new');
    }

    function admin_insert() {
        $this->control_auth_and_permission([1, 2]);
        try {
            //Control de la imagen a subir
            $image_control = $this->images->control($_FILES['clr_img'], 200);
            if (!$image_control['result']) {
                $this->echo_die(TRUE, $image_control['message']);
            }

            //Guardo registro en BD
            $insert = $this->catalog->colors_new($this->input->post());
            if ($insert['error']) {
                $this->echo_die(TRUE, $insert['message']);
            }

            //Guardo la imagen
            $image_uploaded = $this->images->upload($_FILES['clr_img'], 'clr' . $insert['message'], 'colors');
            if (!$image_uploaded) {
                $this->echo_die(TRUE, 'Problemas internos al subir la imagen');
            }

            //Guardo nombre de la imagen en la BD
            $update = $this->catalog->colors_update($insert['message'], ['clr_image' => $image_uploaded]);
            $this->echo_die($update['error'], $update['message']);
        } catch (Exception $exc) {
            $this->echo_die(TRUE, $exc->getMessage());
        }
    }

    function admin_view($clr_id) {
        $this->control_auth_and_permission([1, 2]);
        try {
            $this->load->view('admin/product_attributes/colors/view', ['color' => $this->catalog->colors_by_id($clr_id)]);
        } catch (Exception $exc) {
            $this->echo_die(TRUE, $exc->getMessage());
        }
    }

    function admin_update($clr_id) {
        $this->control_auth_and_permission([1, 2]);
        $image_uploaded = NULL;
        try {
            $post_data = $this->input->post();

            if (is_uploaded_file($_FILES['clr_img']['tmp_name'])) {
                //Control de la imagen a subir
                $image_control = $this->images->control($_FILES['clr_img'], 200);
                if (!$image_control['result']) {
                    $this->echo_die(TRUE, $image_control['message']);
                }

                //Guardo la imagen
                $image_uploaded = $this->images->upload($_FILES['clr_img'], 'clr' . $clr_id, 'colors');
                if (!$image_uploaded) {
                    $this->echo_die(TRUE, 'Problemas internos al subir la imagen');
                }
                $post_data['clr_image'] = $image_uploaded; //Nombre de la imagen
            }

            //Actualizo el registro en la BD
            $update = $this->catalog->colors_update($clr_id, $post_data);
            $this->echo_die($update['error'], $update['message']);
        } catch (Exception $exc) {
            $this->echo_die(TRUE, $exc->getMessage());
        }
    }

}

/* End of file colors.php */
/* Location: ./application/controllers/colors.php */

