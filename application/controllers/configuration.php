<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Configuration extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('configuration_model', 'configuration');
    }

    function email_list() {
        $this->control_auth_and_permission([1, 2]);

        $data = array(
            'section' => 'Configuración de e-mails',
            'description' => 'Configure los textos de los correos electrónicos',
            'view' => 'admin/configuration/email/list',
            'view_data' => ['correos' => $this->configuration->getCorreos()],
        );

        $this->load->view('admin/layout', $data);
    }

    function email_view($id) {
        $this->control_auth_and_permission([1, 2]);
        try {

            $this->load->view('admin/configuration/email/view', ['correo' => $this->configuration->getCorreo($id)]);
        } catch (Exception $exc) {
            $this->echo_die(TRUE, $exc->getMessage());
        }
    }

    function email_save($id) {
        $this->control_auth_and_permission([1, 2]);
        try {
            if ($this->configuration->save($id, ['config_value' => $this->input->post('config_value')])) {
                echo json_encode(['error' => FALSE, 'message' => 'Mensaje modificado con éxito.']);
            } else {
                echo json_encode(['error' => TRUE, 'message' => 'No se pudo modificar correctamente el mensaje.']);
            }
        } catch (Exception $exc) {
            $this->echo_die(TRUE, $exc->getMessage());
        }
    }

}