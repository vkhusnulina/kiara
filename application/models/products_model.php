<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Products_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Get all products
     *
     * @return	object
     */
    function get_all() {
        $query = $this->db->get('products');
        if ($query->num_rows() > 0)
            return $query->result();
        return NULL;
    }
    
    /**
     * Get product by ID
     *
     * @return	object
     */
    function get_by_id($prod_id) {
        $this->db->where('prod_id', $prod_id);

        $query = $this->db->get('products');
        if ($query->num_rows() == 1)
            return $query->row();
        return NULL;
    }

    function get_images($prod_id) {
        $this->db->select('media.*');
        $this->db->from('media');
        $this->db->join('products_media', 'products_media.media_media_id = media.media_id');
        $this->db->where('products_media.products_prod_id', $prod_id);

        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query->result();
        return array();
    }

    function get_categories($prod_id) {
        $this->db->select('products_categories.categories_cat_id');
        $this->db->from('products_categories');
        $this->db->where('products_categories.products_prod_id', $prod_id);

        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query->result();
        return NULL;
    }

    function get_colors($prod_id) {
        $this->db->select('*');
        $this->db->from('products_colors');
        $this->db->join('colors', 'colors.clr_id = products_colors.colors_clr_id');
        $this->db->where('products_colors.products_prod_id', $prod_id);

        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query->result();
        return array();
    }

    function get_sizes($prod_id) {
        $this->db->select('*');
        $this->db->from('products_sizes');
        $this->db->join('sizes', 'sizes.siz_id = products_sizes.sizes_siz_id');
        $this->db->where('products_sizes.products_prod_id', $prod_id);

        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query->result();
        return array();
    }

    /**
     * Insert product
     *
     * @return	int ID or False
     */
    function insert($data) {
        if ($this->db->insert('products', $data)) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    /**
     * Insert products_media
     *
     * @return	int ID or False
     */
    function insert_media($prod_id, $media_id) {
        if ($this->db->insert('products_media', ['media_media_id' => $media_id, 'products_prod_id' => $prod_id])) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    /**
     * Update product
     *
     * @return	boolean
     */
    function update($prod_id, $data) {
        $this->db->where('prod_id', $prod_id);
        return $this->db->update('products', $data);
    }

    function update_categories($prod_id, $selected_categories, $already_added) {
        return $this->update_many_to_many('products_categories', 'products_prod_id', 'categories_cat_id', 'categoría', $prod_id, $selected_categories, $already_added);
    }

    function update_colors($prod_id, $selected_colors, $already_added) {
        return $this->update_many_to_many('products_colors', 'products_prod_id', 'colors_clr_id', 'color', $prod_id, $selected_colors, $already_added);
    }

    function update_sizes($prod_id, $selected_sizes, $already_added) {
        return $this->update_many_to_many('products_sizes', 'products_prod_id', 'sizes_siz_id', 'tamaño', $prod_id, $selected_sizes, $already_added);
    }

    private function update_many_to_many($table, $column_product, $other_column, $item_name, $prod_id, $selected, $already_added) {
        $results = TRUE;
        $message = "";
        foreach ($selected as $item) {
            if (!in_array($item, $already_added)) {
                if (!$this->db->insert($table, [$column_product => $prod_id, $other_column => $item])) {
                    $results = FALSE;
                    $message = $message . ' Problemas para insertar ' . $item_name . ' ID' . $item . '. ';
                }
            }
        }
        foreach ($already_added as $item) {
            if (!in_array($item, $selected)) {
                if (!$this->db->delete($table, [$column_product => $prod_id, $other_column => $item])) {
                    $results = FALSE;
                    $message = $message . ' Problemas para eliminar ' . $item_name . ' ID' . $item . '. ';
                }
            }
        }
    }

}

/* End of file products_model.php */
/* Location: ./application/models/products_model.php */