<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Newsletters_model extends CI_Model {

    private $table = 'newsletters';

    function __construct() {
        parent::__construct();
    }

    function getAll() {
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0)
            return $query->result();
        return NULL;
    }

    function insert($data) {
        return $this->db->insert($this->table, $data);
    }

}

/* End of file Newsletters_model.php */
/* Location: ./application/models/Newsletters_model.php */