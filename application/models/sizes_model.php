<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sizes_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Get all clothes sizes
     *
     * @return	object
     */
    function get_all_sizes() {
        $query = $this->db->get_where('sizes', array('siz_shoes' => 0));
        if ($query->num_rows() > 0)
            return $query->result();
        return NULL;
    }

    /**
     * Get all sizes
     *
     * @return	object
     */
    function get_all_shoes_sizes() {
        $query = $this->db->get_where('sizes', array('siz_shoes' => 1));
        if ($query->num_rows() > 0)
            return $query->result();
        return NULL;
    }

    /**
     * Get color by ID
     *
     * @return	object
     */
    function get_by_id($siz_id) {
        $this->db->where('siz_id', $siz_id);

        $query = $this->db->get('sizes');
        if ($query->num_rows() == 1)
            return $query->row();
        return NULL;
    }

    /**
     * Insert color
     *
     * @return	int ID or False
     */
    function insert($data) {
        if ($this->db->insert('sizes', $data)) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    /**
     * Update color
     *
     * @return	boolean
     */
    function update($siz_id, $data) {
        $this->db->where('siz_id', $siz_id);
        return $this->db->update('sizes', $data);
    }

}

/* End of file sizes_model.php */
/* Location: ./application/models/sizes_model.php */