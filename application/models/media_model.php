<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Media_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    function get_by_visible($visible = 3) {
        $this->db->from('media');
        $this->db->where('media_visible', $visible);

        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query->result();
        return array();
    }

    /**
     * Insert image
     *
     * @return	int ID or False
     */
    function insert($data) {
        if ($this->db->insert('media', $data)) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    /**
     * Update image
     *
     * @return	boolean
     */
    function update($media_id, $data) {
        $this->db->where('media_id', $media_id);
        return $this->db->update('media', $data);
    }

    /**
     * Delete image
     *
     * @return	boolean
     */
    function delete($media_id) {
        $this->db->where('media_id', $media_id);
        $this->db->delete('media');
        return TRUE;
        /*
        $query = $this->db->get('media');
        if ($query->num_rows() == 1) {
            $media = $query->row();
            $server_path = str_replace("/", DIRECTORY_SEPARATOR, $_SERVER["DOCUMENT_ROOT"]);
            $path = join(DIRECTORY_SEPARATOR, array($server_path,
                'kiara',
                'assets',
                'admin',
                'images',
                'products',
                $media->media_name));
            if (is_file($path)) {
                unlink($path);
                $this->db->where('media_id', $media_id);
                $this->db->delete('media');
                return TRUE;
            }
        }
        return FALSE;*/
    }

}

/* End of file sizes_model.php */
/* Location: ./application/models/sizes_model.php */