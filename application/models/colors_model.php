<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Colors_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Get all colors
     *
     * @return	object
     */
    function get_all() {
        $query = $this->db->get('colors');
        if ($query->num_rows() > 0)
            return $query->result();
        return NULL;
    }

    /**
     * Get color by ID
     *
     * @return	object
     */
    function get_by_id($clr_id) {
        $this->db->where('clr_id', $clr_id);

        $query = $this->db->get('colors');
        if ($query->num_rows() == 1)
            return $query->row();
        return NULL;
    }

    /**
     * Insert color
     *
     * @return	int ID or False
     */
    function insert($data) {
        if ($this->db->insert('colors', $data)) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    /**
     * Update color
     *
     * @return	boolean
     */
    function update($clr_id, $data) {
        $this->db->where('clr_id', $clr_id);
        return $this->db->update('colors', $data);
    }

}

/* End of file colors_model.php */
/* Location: ./application/models/colors_model.php */