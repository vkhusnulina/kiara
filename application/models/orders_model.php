<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Orders_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    //Duplicated in orders.php controller
    public function status($name) {
        $status = ['general', 'payment', 'shipping'];
        $status['general'][] = 'General';
        $status['payment'][] = 'de Pago';
        $status['shipping'][] = 'de Envío';
        $status['general'][] = ['1' => ['PENDIENTE', 'info'], '2' => ['REVISADO', 'info'], '3' => ['TERMINADO', 'success'], '4' => ['ANULADO', 'danger']];
        $status['payment'][] = ['1' => ['PENDIENTE', 'info'], '2' => ['CONFIRMADO', 'success']];
        $status['shipping'][] = ['1' => ['EN PROCESO', 'info'], '2' => ['ENVIADO', 'success']];
        return $status[$name];
    }

    /**
     * Get all orders
     *
     * @return	object
     */
    function get_all() {

        $this->db->select('*');
        $this->db->from('orders');
        $this->db->where('orders.ord_finished', '1');
        $this->db->join('users', 'orders.ord_customer_id = users.id');
        $this->db->join('user_profiles', 'users.id = user_profiles.prf_user_id');

        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query->result();
        return array();
    }

    /**
     * Get order by ID
     *
     * @return	object
     */
    function get_by_id($ord_id) {
        $this->db->select('*');
        $this->db->from('orders');
        $this->db->join('users', 'orders.ord_customer_id = users.id');
        $this->db->join('user_profiles', 'users.id = user_profiles.prf_user_id');
        $this->db->join('addresses', 'addresses.add_id = orders.ord_address_id', 'left');
        $this->db->join('invoices', 'invoices.inv_order_id = orders.ord_id', 'left');
        $this->db->where('orders.ord_id', $ord_id);

        $query = $this->db->get();
        if ($query->num_rows() == 1)
            return $query->row();
        return NULL;
    }

    /**
     * Get orders by user ID
     *
     * @return	array
     */
    function get_by_user_id($user_id) {
        $this->db->where('orders.ord_customer_id', $user_id);
        $this->db->where('orders.ord_finished', '1');
        $this->db->order_by('orders.ord_id', 'desc');

        $query = $this->db->get('orders');
        if ($query->num_rows() > 0)
            return $query->result();
        return array();
    }
    
    function get_unfinished_order_by_user($user_id) {
        $this->db->select('*');
        $this->db->from('orders');
        $this->db->join('addresses', 'addresses.add_id = orders.ord_address_id', 'left');
        $this->db->join('invoices', 'invoices.inv_order_id = orders.ord_id', 'left');
        $this->db->where('orders.ord_customer_id', $user_id);
        $this->db->where('orders.ord_finished', '0');
        $this->db->order_by('orders.ord_id');
        $this->db->limit(1);

        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query->row();
        return NULL;
    }

    /**
     * Get order items by order ID
     *
     * @return	object
     */
    function get_items_by_id($ord_id) {
        $this->db->select('*');
        $this->db->from('order_items');
        $this->db->join('products', 'products.prod_id = order_items.itm_product_id');
        $this->db->join('colors', 'colors.clr_id = order_items.itm_color_id', 'left');
        $this->db->join('sizes', 'sizes.siz_id = order_items.itm_size_id', 'left');
        $this->db->where('itm_order_id', $ord_id);

        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query->result();
        return array();
    }

    /**
     * Get order item by item ID
     *
     * @return	object
     */
    function get_order_id_by_item_id($itm_id) {
        $this->db->select('itm_order_id');
        $this->db->from('order_items');
        $this->db->where('itm_id', $itm_id);

        $query = $this->db->get();
        if ($query->num_rows() == 1)
            return $query->row();
        return NULL;
    }
    
    /**
     * Get order item by item ID
     *
     * @return	object
     */
    function get_item_by_id($itm_id) {
        $this->db->select('*');
        $this->db->from('order_items');
        $this->db->join('products', 'products.prod_id = order_items.itm_product_id');
        $this->db->join('colors', 'colors.clr_id = order_items.itm_color_id', 'left');
        $this->db->join('sizes', 'sizes.siz_id = order_items.itm_size_id', 'left');
        $this->db->where('itm_id', $itm_id);

        $query = $this->db->get();
        if ($query->num_rows() == 1)
            return $query->row();
        return NULL;
    }
    
    function get_user_email_by_item_id($itm_id) {
        $this->db->select('*');
        $this->db->from('order_items');
        $this->db->join('orders', 'orders.ord_id = order_items.itm_order_id');
        $this->db->join('users', 'users.id = orders.ord_customer_id');
        $this->db->where('itm_id', $itm_id);

        $query = $this->db->get();
        if ($query->num_rows() == 1)
            $row = $query->row();
            return $row->email;
        return NULL;
    }
    
    function get_user_email_by_order_id($ord_id) {
        $this->db->select('*');
        $this->db->from('orders');
        $this->db->join('users', 'users.id = orders.ord_customer_id');
        $this->db->where('ord_id', $ord_id);

        $query = $this->db->get();
        if ($query->num_rows() == 1)
            $row = $query->row();
            return $row->email;
        return NULL;
    }

    function get_address_id_by_order_id($order_id) {
        $this->db->where('ord_id', $order_id);
        $query = $this->db->get('orders');
        if ($query->num_rows() == 1) {
            $row = $query->row();
            return $row->ord_address_id;
        }
        return NULL;
    }

    function get_invoice_id_by_order_id($order_id) {
        $this->db->where('inv_order_id', $order_id);
        $query = $this->db->get('invoices');
        if ($query->num_rows() == 1) {
            $row = $query->row();
            return $row->inv_id;
        }
        return NULL;
    }

    function get_history_by_order_id($order_id) {
        $this->db->join('users', 'order_history.hst_user_id = users.id');
        $this->db->where('hst_order_id', $order_id);
        $query = $this->db->get('order_history');
        if ($query->num_rows() > 0)
            return $query->result();
        return array();
    }

    /**
     * Insert order
     *
     * @return	int ID or False
     */
    function insert($data) {
        if ($this->db->insert('orders', $data)) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    /**
     * Insert address
     *
     * @return	int ID or False
     */
    function insert_address($data) {
        if ($this->db->insert('addresses', $data)) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    /**
     * Insert invoice
     *
     * @return	int ID or False
     */
    function insert_invoice($data) {
        if ($this->db->insert('invoices', $data)) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    /**
     * Insert history
     *
     * @return	int True or False
     */
    function insert_history($data) {
        if ($this->db->insert('order_history', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    /**
     * Insert item
     *
     * @return	int ID or False
     */
    function insert_item($data) {
        if ($this->db->insert('order_items', $data)) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    /**
     * Update order
     *
     * @return	boolean
     */
    function update($ord_id, $data) {
        $this->db->where('ord_id', $ord_id);
        return $this->db->update('orders', $data);
    }

    /**
     * Update order item
     *
     * @return	boolean
     */
    function update_item($itm_id, $data) {
        $this->db->where('itm_id', $itm_id);
        return $this->db->update('order_items', $data);
    }
    
    /**
     * Delete order item
     *
     * @return	boolean
     */
    function delete_item($itm_id) {
        $this->db->where('itm_id', $itm_id);
        return $this->db->delete('order_items');
    }

    /**
     * Update address data
     *
     * @return	boolean
     */
    function update_address($address_id, $data) {
        $this->db->where(array('add_id' => $address_id));
        return $this->db->update('addresses', $data);
    }

    /**
     * Update address data
     *
     * @return	boolean
     */
    function update_invoice($invoice_id, $data) {
        $this->db->where(array('inv_id' => $invoice_id));
        return $this->db->update('invoices', $data);
    }

}

/* End of file products_model.php */
/* Location: ./application/models/products_model.php */



