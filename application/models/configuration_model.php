<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Configuration_model extends CI_Model {

    private $table = 'configuration';

    function __construct() {
        parent::__construct();
    }

    function getCorreos() {
        $this->db->where('config_type', 'email');
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0)
            return $query->result();
        return NULL;
    }

    function getCorreo($id) {
        $this->db->where('config_id', $id);
        $this->db->where('config_type', 'email');
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0)
            return $query->row();
        return NULL;
    }

    function save($id, $data) {
        $this->db->where('config_id', $id);
        return $this->db->update($this->table, $data);
    }

}