<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Categories_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Get all categories
     *
     * @return	object
     */
    function get_all($visibleOnly = False) {
        $this->db->order_by('cat_order asc, cat_name');
        if ($visibleOnly) {
            $this->db->where('cat_visible', '1');
        }
        $query = $this->db->get('categories');
        if ($query->num_rows() > 0)
            return $query->result();
        return NULL;
    }

    /**
     * Get category by ID
     *
     * @return	object
     */
    function get_by_id($cat_id) {
        $this->db->where('cat_id', $cat_id);

        $query = $this->db->get('categories');
        if ($query->num_rows() == 1)
            return $query->row();
        return NULL;
    }

    /**
     * Get category by slug
     *
     * @return	object
     */
    function get_by_slug($cat_slug) {
        $this->db->where('cat_slug', strtolower($cat_slug));

        $query = $this->db->get('categories');
        if ($query->num_rows() == 1)
            return $query->row();
        return NULL;
    }

    /**
     * Get all products from specific category
     *
     * @return	object
     */
    function get_products_by_id($cat_id, $visible = 0) {
        $this->db->select('products.*');
        $this->db->from('products_categories');
        $this->db->join('products', 'products.prod_id = products_categories.products_prod_id');
        $this->db->where('products_categories.categories_cat_id', $cat_id);

        if ($visible == 1)
            $this->db->where('products.prod_visible', 1);

        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query->result();
        return array();
    }

    /**
     * Insert category
     *
     * @return	int ID or False
     */
    function insert($data) {
        if ($this->db->insert('categories', $data)) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    /**
     * Update category
     *
     * @return	boolean
     */
    function update($cat_id, $data) {
        $this->db->where('cat_id', $cat_id);
        return $this->db->update('categories', $data);
    }

}

/* End of file categories_model.php */
/* Location: ./application/models/categories_model.php */