<?php

/* * *
 * El CUSTOMER_MODEL es formado por las tablas users, user_profiles, addresses
 */


if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customers_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_by_user_id($user_id) {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('user_profiles', 'users.id = user_profiles.prf_user_id');
        $this->db->join('addresses', 'user_profiles.prf_address_id = addresses.add_id', 'left');
        $this->db->where('users.id', $user_id);

        $query = $this->db->get();
        if ($query->num_rows() == 1)
            return $query->row();
        return NULL;
    }

    function get_address_id_by_user_id($user_id) {
        $this->db->where('prf_user_id', $user_id);
        $query = $this->db->get('user_profiles');
        if ($query->num_rows() == 1) {
            $row = $query->row();
            return $row->prf_address_id;
        }
        return NULL;
    }
    
    /**
     * Insert user_profile
     *
     * @return	int ID or False
     */
    function insert_profile($data) {
        if ($this->db->insert('user_profiles', $data)) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }
    
    /**
     * Insert address
     *
     * @return	int ID or False
     */
    function insert_address($data) {
        if ($this->db->insert('addresses', $data)) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    /**
     * Update user_profile data
     *
     * @return	boolean
     */
    function update_profile($user_id, $data) {
        $this->db->where(array('prf_user_id' => $user_id));
        return $this->db->update('user_profiles', $data);
    }

    /**
     * Update address data
     *
     * @return	boolean
     */
    function update_address($address_id, $data) {
        $this->db->where(array('add_id' => $address_id));
        return $this->db->update('addresses', $data);
    }

}

/* End of file products_model.php */
/* Location: ./application/models/products_model.php */

