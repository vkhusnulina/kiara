# KIARA Client #

E-Commerce based web system with products, orders and customers administration.

### Details ###

* Developed on **PHP** language under **CodeIgniter** framework
* **MySQL** database
* **HTML5/CSS3/JQuery/AJAX**

### Collaborators ###

* Front-end: Gastón Perez
* Back-end + Admin CRM Panel: Valeria Khusnulina

http://www.kiaraweb.com.ar

![navbar.JPG](https://bitbucket.org/repo/bK8Ed4/images/197674494-navbar.JPG)
![newproduct.JPG](https://bitbucket.org/repo/bK8Ed4/images/3361139593-newproduct.JPG)